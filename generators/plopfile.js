// eslint-disable-next-line no-undef
module.exports = function (plop) {
	// controller generator

	plop.setGenerator('command', {
		description: 'Create a command',

		prompts: [
			{
				type: 'input',
				name: 'category',
				message: 'component category: '
			},
			{
				type: 'input',
				name: 'name',
				message: 'component name: '
			},
			{
				type: 'input',
				name: 'description',
				message: 'component description: '
			}
		],
		actions: [
			{
				type: 'add',
				path: '../src/commands/{{pascalCase category}}/{{pascalCase name}}.ts',
				templateFile: 'templates/command.ts.hbs'
			}
		]
	});

	plop.setGenerator('subcommand', {
		description: 'Create a subcommand',

		prompts: [
			{
				type: 'input',
				name: 'category',
				message: 'component category: '
			},
			{
				type: 'input',
				name: 'name',
				message: 'component name: '
			},
			{
				type: 'input',
				name: 'description',
				message: 'component description: '
			}
		],
		actions: [
			{
				type: 'add',
				path: '../src/commands/{{pascalCase category}}/{{pascalCase name}}.ts',
				templateFile: 'templates/subcommand.ts.hbs'
			}
		]
	});

	plop.setGenerator('button-handler', {
		description: 'Create a button handler',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: 'component name: '
			}
		],
		actions: [
			{
				type: 'add',
				path: '../src/interaction-handlers/buttons/{{pascalCase name}}.ts',
				templateFile: 'templates/interaction-handler-button.ts.hbs'
			}
		]
	});

	plop.setGenerator('select-handler', {
		description: 'Create a select handler',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: 'component name: '
			}
		],
		actions: [
			{
				type: 'add',
				path: '../src/interaction-handlers/selects/{{pascalCase name}}.ts',
				templateFile: 'templates/interaction-handler-select.ts.hbs'
			}
		]
	});

	plop.setGenerator('listener', {
		description: 'Create an event listener',
		prompts: [
			{
				type: 'input',
				name: 'event',
				message: 'event: '
			}
		],
		actions: [
			{
				type: 'add',
				path: '../src/listeners/{{pascalCase event}}.ts',
				templateFile: 'templates/listener.ts.hbs'
			}
		]
	});

	plop.setGenerator('scheduled-task', {
		description: 'Create a scheduled task',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: 'task name: '
			}
		],
		actions: [
			{
				type: 'add',
				path: '../src/scheduled-tasks/{{pascalCase name}}.ts',
				templateFile: 'templates/scheduled-task.ts.hbs'
			}
		]
	});
};
