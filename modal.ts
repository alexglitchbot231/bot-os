import { confirm, createDefaultEmbed, generateButtonCommandId } from '#lib/utils';
import { Listener, Events, type PieceContext, type ListenerOptions, UserError, ChatInputCommandErrorPayload } from '@sapphire/framework';
import { codeBlock } from '@sapphire/utilities';
import { GuildMember, Interaction, MessageActionRow, MessageButton, ModalSubmitInteraction, TextChannel } from 'discord.js';
import { inspect } from 'util';
import { SudoWarning } from '../lib/constants';

export class UserEvent extends Listener<typeof Events.InteractionCreate> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: Events.InteractionCreate
		});
	}

	public async run(interaction: Interaction) {
		if (!interaction.isModalSubmit()) return;

		const [, name] = interaction.customId.split('.');

		Reflect.get(this, name).call(this, interaction);
	}

	private async eval(interaction: ModalSubmitInteraction) {
		// @ts-expect-error - This is a hack to get the interaction data
		const code = i.fields.components[0].components[0].value;

		const embed = createDefaultEmbed(interaction.user).setDescription([SudoWarning, codeBlock('ts', code)].join('\n'));

		// @ ts-expect-error - TypeScript is wrong here
		// interaction.reply({ embeds: [embed] });

		const conf = confirm({
			interaction: <ModalSubmitInteraction>interaction,
			message: [SudoWarning, codeBlock('ts', code)].join('\n'),
			valueIfConfirmed: 'Done'
		});

		if (!conf) {
			return;
		}
	}

	private async introduce(interaction: ModalSubmitInteraction) {
		const name = interaction.fields.getTextInputValue('name');
		const pronouns = interaction.fields.getTextInputValue('pronouns') || null;
		const description = interaction.fields.getTextInputValue('description');

		const embed = createDefaultEmbed(interaction.user)
			.setAuthor({
				name: pronouns ? `${interaction.user.tag} [${pronouns}]` : interaction.user.tag,
				iconURL: (interaction.member as GuildMember).displayAvatarURL({ dynamic: true })
			})
			.addField('Name', name)
			.addField('Description', description)
			.setColor((interaction.member as GuildMember).displayColor);

		const c = (await interaction.guild!.channels.fetch('986565450735452180')) as TextChannel;

		await c.send({ embeds: [embed] });

		interaction.reply({ content: 'Sent', ephemeral: true });
	}

	private async feedback(interaction: ModalSubmitInteraction) {
		const feedback = interaction.fields.getTextInputValue('feedback');

		const embed = createDefaultEmbed(interaction.member as GuildMember)
			.setTitle('Feedback')
			.setDescription(feedback);

		const c = (await interaction.guild!.channels.fetch('986577213916864582')) as TextChannel;

		await c.send({ embeds: [embed] });

		interaction.reply({ content: 'Sent', ephemeral: true });
	}

	private async bug(interaction: ModalSubmitInteraction) {
		const description = interaction.fields.getTextInputValue('description');
		const expected = interaction.fields.getTextInputValue('expected');
		const actual = interaction.fields.getTextInputValue('actual');
		const repro = interaction.fields.getTextInputValue('repro');

		const embed = createDefaultEmbed(interaction.member as GuildMember)
			.addField('Description', description)
			.addField('Expected Result', expected)
			.addField('Actual Result', actual)
			.addField('Reproduction Steps', repro);

		const c = (await interaction.guild!.channels.fetch('960713433463013396')) as TextChannel;

		await c.send({ embeds: [embed] });
	}

	private async suggest(interaction: ModalSubmitInteraction) {
		const suggestion = interaction.fields.getTextInputValue('suggestion');

		const embed = createDefaultEmbed(interaction.member as GuildMember)
			.setTitle('Suggestion')
			.setDescription(suggestion);

		const { Id } = await this.container.prisma.suggestion.create({
			data: {
				Category: 'Server',
				Content: suggestion,
				Downvotes: 0,
				Upvotes: 0,
				UserId: interaction.user.id
			},
			select: { Id: true }
		});

		const c = (await interaction.guild!.channels.fetch('987387468212998204')) as TextChannel;

		const resolveButtons = [
			new MessageButton()
				.setCustomId(generateButtonCommandId('suggestion', 'resolve', 'accept', Id.toString()))
				.setLabel('Accept')
				.setStyle('SUCCESS'),
			new MessageButton()
				.setCustomId(generateButtonCommandId('suggestion', 'resolve', 'consider', Id.toString()))
				.setLabel('Consider')
				.setStyle('SECONDARY'),
			new MessageButton()
				.setCustomId(generateButtonCommandId('suggestion', 'resolve', 'deny', Id.toString()))
				.setLabel('Deny')
				.setStyle('DANGER')
		];
		const reply = await c.send({ embeds: [embed], components: [new MessageActionRow().setComponents(...resolveButtons)] });

		interaction.reply({ content: 'Sent', ephemeral: true });

		await (reply.channel as TextChannel).threads.create({
			startMessage: reply,
			name: `Suggestion #${Id}`
		});
	}
}
