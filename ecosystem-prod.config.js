module.exports = {
	apps: [
		{
			name: 'agb',
			script: './dist/index.js',
			env: {
				NODE_ENV: 'production'
			}
		}
	]
};
