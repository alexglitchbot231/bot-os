FROM node:16

RUN ["corepack", "enable"] # Enable corepack (enables yarn)

COPY . /opt/agb

WORKDIR /opt/agb

RUN ["yarn", "install"] # Install dependencies
RUN ["yarn", "build"] # Build the app
ENV DATABASE_URL=postgresql://postgres:mysecretpassword@postgres:5432/alexglitchbot?schema=public

ENTRYPOINT ["yarn", "start"] # Run the app
