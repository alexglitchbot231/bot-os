import { execSync, spawnSync } from 'child_process';
import { readFileSync } from 'fs';
import inquirer from 'inquirer';
import ora from 'ora';
import { join } from 'path';
import YAML from 'yaml';

async function manage() {
	const { how } = await inquirer.prompt([
		{
			type: 'list',
			message: 'How do you want to manage the bot',
			choices: [
				'Stop All Services',
				'Stop A Service',
				'Start All Services',
				'Start A Service',
				'Restart All Services',
				'Restart A Service',
				'Update the bot'
			],
			name: 'how'
		}
	]);

	switch (how) {
		case 'Stop All Services':
			await stopAll();
			break;
		case 'Stop A Service':
			await stopSome();
			break;
		case 'Start All Services':
			await startAll();
			break;
		case 'Start A Service':
			await startSome();
			break;
		case 'Restart a service':
			break;
		case 'Restart all services':
			break;
		case 'Update the bot':
			await update();
			break;
	}
}

async function stopAll() {
	const spinner = ora('Stopping services').start();
	execSync('docker-compose -f .\\docker-compose.development.yml stop', { shell: true, stdio: 'ignore' });
	spinner.succeed('Stopped all services');
}

async function stopSome() {
	const raw = readFileSync(join('.', 'docker-compose.development.yml'), 'utf-8');

	const { services } = await inquirer.prompt([
		{
			name: 'services',
			type: 'checkbox',
			choices: Object.keys(YAML.parse(raw).services),
			validate(answer) {
				if (answer.length < 1) {
					return 'You must choose at least one service.';
				}

				return true;
			}
		}
	]);

	const spinner = ora('Stopping services').start();
	execSync(`docker-compose -f .\\docker-compose.development.yml stop ${services.join(' ')}`, { stdio: 'ignore' });
	spinner.succeed(`Stopped ${services.length} services`);
}

async function startAll() {
	const spinner = ora('Starting services').start();
	execSync('docker-compose -f .\\docker-compose.development.yml start', { shell: true, stdio: 'ignore' });
	spinner.succeed('Started all services');
}

async function startSome() {
	const raw = readFileSync(join('.', 'docker-compose.development.yml'), 'utf-8');

	const { services } = await inquirer.prompt([
		{
			name: 'services',
			type: 'checkbox',
			choices: Object.keys(YAML.parse(raw).services),
			validate(answer) {
				if (answer.length < 1) {
					return 'You must choose at least one service.';
				}

				return true;
			}
		}
	]);

	const spinner = ora('Start services').start();
	execSync(`docker-compose -f .\\docker-compose.development.yml start ${services.join(' ')}`, { stdio: 'ignore' });
	spinner.succeed(`Started ${services.length} services`);
}

async function restartAll() {
	const spinner = ora('Starting services').start();
	execSync('docker-compose -f .\\docker-compose.development.yml restart', { shell: true, stdio: 'ignore' });
	spinner.succeed('Started all services');
}

async function restartSome() {
	const raw = readFileSync(join('.', 'docker-compose.development.yml'), 'utf-8');

	const { services } = await inquirer.prompt([
		{
			name: 'services',
			type: 'checkbox',
			choices: Object.keys(YAML.parse(raw).services),
			validate(answer) {
				if (answer.length < 1) {
					return 'You must choose at least one service.';
				}

				return true;
			}
		}
	]);

	const spinner = ora('Start services').start();
	execSync(`docker-compose -f .\\docker-compose.development.yml start ${services.join(' ')}`, { stdio: 'ignore' });
	spinner.succeed(`Started ${services.length} services`);
}

async function update() {
	const spinner1 = ora('Pulling latest update').start();
	execSync('docker-compose pull', { stdio: 'ignore' });
	spinner1.succeed('Pulled latest update');
	const spinner2 = ora('Applying update').start();
	execSync('docker-compose up -d --remove-orphans');
	spinner2.succeed(`Applied update. Now on v${JSON.parse(readFileSync(join('.', 'package.json'), 'utf-8')).version}`);
}

void manage();
