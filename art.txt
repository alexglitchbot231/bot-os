{{green}}             _          |>  {{command_count}} Commands Loaded
{{green}}  __ _  __ _| |__       |>  {{listener_count}} Listeners Loaded
{{green}} / _` |/ _` | '_ \      |>  {{precondition_count}} Preconditions Loaded
{{green}}| (_| | (_| | |_) |     |>  {{interaction_handler_count}} Interaction Handler Count
{{green}} \__,_|\__, |_.__/      |>  {{task_count}} Scheduled Tasks Loaded
{{green}}       |___/            |>  {{scam_domain_count}} Scam Domains Loaded
                        |>  {{disabled_module_count}} Modules Disabled