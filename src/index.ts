import './lib/setup';
import { container, LogLevel, SapphireClient } from '@sapphire/framework';
import type { PrismaClient } from '@prisma/client';
import { EconomyManager } from '#lib/EconomyManager';
import { Queue } from '#lib/Queue';
import { Connectors, Player, Shoukaku } from 'shoukaku';
import type { Nullish } from '@sapphire/utilities';
import type { QueueEntry } from '#lib/interfaces/QueueEntry';
import type { Dayjs } from 'dayjs';
import dayjs from 'dayjs';
import type { AGBUserFlagsType } from '#lib/constants';
import type { TextChannel } from 'discord.js';
import { ScheduledTaskRedisStrategy } from '@sapphire/plugin-scheduled-tasks/register-redis';
import type { EconomyConfig } from '#lib/interfaces/EconomyConfig';
import type { BotConfig } from '#lib/interfaces/BotConfig';
import EventEmitter from 'events';
import type { ChallengeType } from '#lib/structures';
import { MathChallengeType, ScrambleChallengeType, WordChallengeType } from '#src/challenge-types';

const client = new SapphireClient({
	caseInsensitiveCommands: true,
	logger: {
		level: container.botConfig.logger.level,
		// level: process.env.NODE_ENV === 'development' ? LogLevel.Debug : LogLevel.Info,
		depth: container.botConfig.logger.depth
	},
	shards: 'auto',
	intents: ['GUILD_MEMBERS', 'GUILDS', 'GUILD_BANS', 'GUILD_VOICE_STATES', 'GUILD_MESSAGES'],
	// env: {
	// 	enabled: true
	// },
	tasks: {
		strategy: new ScheduledTaskRedisStrategy({
			bull: {
				connection: {
					port: container.botConfig.secrets.redis.port,
					host: container.botConfig.secrets.redis.host,
					db: container.botConfig.secrets.redis.db ?? 2
				}
			}
		})
	}
});

const main = async () => {
	container.econManager = new EconomyManager();
	container.emitter = new EventEmitter();

	try {
		client.logger.info('Logging in');
		// await client.login(container.env.string('DISCORD_TOKEN'));
		await client.login(container.botConfig.secrets.token);
		client.logger.info('logged in');
		container.startTime = dayjs();
	} catch (error) {
		client.logger.fatal(error);
		client.destroy();
		process.exit(1);
	}

	try {
		// const useLL = container.env.boolean('USE_LAVALINK', true);
		const useLL = container.botConfig.secrets.lavalink.enabled;
		container.musicNode = useLL
			? new Shoukaku(new Connectors.DiscordJS(client), [
					{
						name: 'agb',
						auth: container.botConfig.secrets.lavalink.password,
						// auth: container.env.string('LAVALINK_PASS')!,
						url: container.botConfig.secrets.lavalink.host!
						// url: container.env.string('LAVALINK_HOST')!
					}
			  ])
			: null;

		container.lavalinkConnected = true;
		container.lavalinkEnabled = useLL;
		container.musicQueue = new Queue<QueueEntry>();

		container.challengeTypes = [new MathChallengeType(), new WordChallengeType(), new ScrambleChallengeType()];

		// new Timers().startAll();
	} catch (error) {
		container.lavalinkConnected = false;
		container.logger.warn('Could not connect to Lavalink.', error);
	}
};

declare module '@sapphire/pieces' {
	interface Container {
		prisma: PrismaClient;
		dbConnected: boolean;
		econManager: EconomyManager;
		musicNode: Shoukaku | null;
		lavalinkConnected: boolean;
		lavalinkEnabled: boolean;
		musicPlayer: Player | Nullish;
		musicQueue: Queue<QueueEntry>;
		musicBoundTextChannel: TextChannel | Nullish;
		startTime: Dayjs;
		econConfig: EconomyConfig;
		botConfig: BotConfig;
		scamDomains: string[];
		scamDomainWsConnected: boolean;
		allowedWords: string[];
		emitter: EventEmitter;
		challengeTypes: ChallengeType[];
		disabledModules: string[];
	}
}

declare module '@sapphire/framework' {
	interface Preconditions {
		CheckLavaLink: never;
		CheckHugsEnabled: never;
		Blacklist: never;
		ModuleDisabled: never;
		RequireFlags: {
			flags: AGBUserFlagsType[];
		};
		RequireMissingFlags: {
			flags: AGBUserFlagsType[];
		};
		Cost: {
			Cost: number;
		};
	}
}

// declare module '@kaname-png/plugin-env' {
// 	interface EnvKeys {
// 		DATABASE_URL: never;
// 		DISCORD_TOKEN: never;
// 		OWNERS: never;
// 		TENOR_API_KEY: never;
// 		LAVALINK_PASS: never;
// 		LAVALINK_HOST: never;
// 		USE_LAVALINK: never;
// 		INTERNAL_LOG_COMMAND: never;
// 		INTERNAL_LOG_ERROR: never;
// 		INTERNAL_LOG_STATUS: never;
// 		REDIS_HOST: never;
// 		NODE_ENV: never;
// 		WOLFRAM_KEY: never;
// 		OPENAI_API_KEY: never;
// 	}
// }
main();
