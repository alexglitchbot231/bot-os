import { ChallengeType } from '#lib/structures';
import { UserFlagManager } from '#lib/UserFlagManager';
import { saveEconomyConfig, loadEconomyConfig } from '#lib/utils';
import type { ChallengeCompletedPayloadReturnType } from '#lib/interfaces';
import { container } from '@sapphire/framework';
import { Time, Duration } from '@sapphire/time-utilities';
import { pickRandom, sleep } from '@sapphire/utilities';
import { randomInt } from 'crypto';
import { Message, MessageEmbed, TextChannel } from 'discord.js';

export class MathChallengeType extends ChallengeType {
	public readonly name = 'math';
	public async run(channel: TextChannel): ChallengeCompletedPayloadReturnType {
		const { min, max } = container.econConfig.ranges.challenges;
		const a = randomInt(min, max).toLocaleString();
		const b = randomInt(min, max).toLocaleString();
		// const reward = randomInt(1, 1000);
		const sign = pickRandom(['+', '-', '*']).toLocaleString();

		const ans = parseInt(eval(`${a} ${sign} ${b}`));

		const embed = new MessageEmbed()
			.setColor('GREEN')
			.setTitle('Challenge')
			.setDescription(`First person to solve the following equation: \`${a} ${sign} ${b}\` wins a challenge point`);

		const msg = await channel.send({ embeds: [embed] });

		const awaited = await channel.awaitMessages({
			filter: async (m) => {
				return (
					m.content === ans.toString() && //
					!(await new UserFlagManager(m.author).hasFlag('EconBanned')) &&
					!isNaN(parseInt(m.content))
				);
			},
			max: 1,
			time: Time.Second * 30
		});

		if (!awaited.size) {
			await msg.edit({
				embeds: [
					new MessageEmbed()
						.setColor('RED')
						.setTitle('Challenge Failed')
						.setDescription('No one solved the challenge')
						.setFooter({ text: `${a} ${sign} ${b} = ${ans}` })
				]
			});
			await sleep(Time.Second * 5);
			await msg.delete();
			return { completed: false };
		}

		const winnerMsg = awaited.first()!;

		const winner = winnerMsg.author;

		await winnerMsg.delete();

		await msg.edit({
			embeds: [
				new MessageEmbed()

					.setColor('GREEN')
					.setTitle('Challenge Won')
					.setDescription(`${winner} won the challenge!`)
					.setFooter({ text: `${a} ${sign} ${b} = ${ans}` })
			]
		});

		await container.prisma.users.upsert({
			where: { Id: winner.id },
			update: {
				ChallengePoints: { increment: 1 }
			},
			create: {
				Id: winner.id,
				ChallengePoints: 1
			}
		});

		const { ChallengePoints: newPoints } = await container.prisma.users.upsert({
			where: { Id: winner.id },
			select: { ChallengePoints: true },
			update: {
				ChallengePoints: { increment: 1 }
			},
			create: {
				Id: winner.id,
				ChallengePoints: 1
			}
		});

		return {
			completed: true,
			type: this,
			user: winner,
			response: winnerMsg.content,
			newPoints
		};
	}
}
