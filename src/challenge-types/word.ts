import { ChallengeType } from '#lib/structures';
import { UserFlagManager } from '#lib/UserFlagManager';
import { saveEconomyConfig, loadEconomyConfig } from '#lib/utils';
import type { ChallengeCompletedPayloadReturnType } from '#lib/interfaces';
import { container } from '@sapphire/framework';
import { Time, Duration } from '@sapphire/time-utilities';
import { pickRandom, sleep } from '@sapphire/utilities';
import { Message, MessageEmbed, TextChannel } from 'discord.js';

export class WordChallengeType extends ChallengeType {
	public readonly name = 'word';

	public async run(channel: TextChannel): ChallengeCompletedPayloadReturnType {
		const word = pickRandom(container.allowedWords);

		const embed = new MessageEmbed()
			.setColor('GREEN')
			.setTitle('Challenge')
			.setDescription(`First person to type: \`${word}\` wins a challenge point`);

		const msg = await channel.send({ embeds: [embed] });

		const awaited = await channel.awaitMessages({
			filter: async (m) => m.content.toLowerCase() === word.toLowerCase() && !(await new UserFlagManager(m.author).hasFlag('EconBanned')),
			max: 1,
			time: Time.Second * 30
		});

		if (!awaited.size) {
			await msg.edit({
				embeds: [new MessageEmbed().setColor('RED').setTitle('Challenge Failed').setDescription('No one solved the challenge')]
			});
			await sleep(Time.Second * 5);
			await msg.delete();
			return { completed: false };
		}

		const winnerMsg = awaited.first()!;

		const winner = winnerMsg.author;

		await winnerMsg.delete();

		await msg.edit({
			embeds: [new MessageEmbed().setColor('GREEN').setTitle('Challenge Won').setDescription(`${winner} won the challenge!`)]
		});

		const { ChallengePoints: newPoints } = await container.prisma.users.upsert({
			where: { Id: winner.id },
			select: { ChallengePoints: true },
			update: {
				ChallengePoints: { increment: 1 }
			},
			create: {
				Id: winner.id,
				ChallengePoints: 1
			}
		});

		return {
			completed: true,
			type: this,
			user: winner,
			response: winnerMsg.content,
			msg,
			newPoints
		};
	}
}
