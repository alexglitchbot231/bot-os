import { ChallengeType } from '#lib/structures';
import { UserFlagManager } from '#lib/UserFlagManager';
import { saveEconomyConfig, loadEconomyConfig } from '#lib/utils';
import type { ChallengeCompletedPayloadReturnType } from '#lib/interfaces';
import { container } from '@sapphire/framework';
import { Time, Duration } from '@sapphire/time-utilities';
import { pickRandom, sleep } from '@sapphire/utilities';
import { Message, MessageEmbed, TextChannel } from 'discord.js';

export class ScrambleChallengeType extends ChallengeType {
	public readonly name = 'scramble';

	public async run(channel: TextChannel): ChallengeCompletedPayloadReturnType {
		const word = pickRandom(container.allowedWords);
		const scrambled = word
			.split('')
			.sort(() => Math.random() - 0.5)
			.join('');

		const embed = new MessageEmbed()
			.setColor('GREEN')
			.setTitle('Challenge')
			.setDescription(`First person to unscramble: \`${scrambled}\` wins a challenge point`);

		const msg = await channel.send({ embeds: [embed] });

		const awaited = await channel.awaitMessages({
			filter: async (m) => m.content.toLowerCase() === word.toLowerCase() && !(await new UserFlagManager(m.author).hasFlag('EconBanned')),
			max: 1,
			time: Time.Second * 30
		});

		if (!awaited.size) {
			await msg.edit({
				embeds: [
					new MessageEmbed()
						.setColor('RED')
						.setTitle('Challenge Failed')
						.setDescription('No one solved the challenge')
						.setFooter({ text: `The word was ${word}` })
				]
			});
			await sleep(Time.Second * 5);
			await msg.delete();
			return { completed: false };
		}

		const winnerMsg = awaited.first()!;

		const winner = winnerMsg.author;

		await container.prisma.users.upsert({
			where: { Id: winner.id },
			update: {
				ChallengePoints: { increment: 1 }
			},
			create: {
				Id: winner.id,
				ChallengePoints: 1
			}
		});

		await msg.edit({
			embeds: [new MessageEmbed().setColor('GREEN').setTitle('Challenge Won').setDescription(`${winner} won the challenge!`)]
		});
		await sleep(Time.Second * 5);

		// await msg.delete();
		container.econConfig.challenges.cooldown_end = new Duration(container.econConfig.challenges.cooldown).fromNow.getTime();
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		const { ChallengePoints: newPoints } = await container.prisma.users.upsert({
			where: { Id: winner.id },
			select: { ChallengePoints: true },
			update: {
				ChallengePoints: { increment: 1 }
			},
			create: {
				Id: winner.id,
				ChallengePoints: 1
			}
		});

		return {
			completed: true,
			type: this,
			user: winner,
			response: winnerMsg.content,
			newPoints,
			msg
		};
	}
}
