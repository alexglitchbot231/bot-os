import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { ButtonInteraction, MessageActionRow, MessageButton, MessageEmbed, MessageSelectMenu, Modal, TextInputComponent } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { Emojis, zws } from '#lib/constants';
import { createDefaultEmbed, generateButtonCommandId } from '#lib/utils';
import { hyperlink, roleMention } from '@discordjs/builders';
import { stripIndents } from 'common-tags';
import { underline } from 'ansi-colors';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Onboarding extends InteractionHandler {
	async run(interaction: ButtonInteraction, { type }: InteractionHandler.ParseResult<this>) {
		Reflect.get(this, type).call(this, interaction);
	}

	parse(_interaction: ButtonInteraction) {
		const [name, type] = _interaction.customId.split(':') as [`agb.${string}`, 'rules' | 'introduce' | 'feedback' | 'bug' | 'roleinfo' | 'tone'];

		if (name !== 'agb.onboarding') return this.none();

		return this.some({ type });
	}

	async introduce(interaction: ButtonInteraction) {
		const row1 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent().setCustomId('name').setLabel('What name would you like to use?').setStyle('SHORT').setRequired(true)
		);
		const row2 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high

			new TextInputComponent().setCustomId('pronouns').setLabel('What are your pronouns (optional)').setStyle('SHORT').setCustomId('pronouns')
		);
		const row3 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high

			new TextInputComponent()
				.setCustomId('description')
				.setLabel('Descrption about yourself')
				.setStyle('PARAGRAPH')
				.setRequired(true)
				.setCustomId('description')
		);

		// @ts-expect-error TS is high
		const modal = new Modal().setCustomId('agb.introduce').setTitle('Introduction').addComponents(row1, row2, row3);

		await interaction.showModal(modal);
	}

	async feedback(interaction: ButtonInteraction) {
		const row1 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent().setCustomId('feedback').setLabel('Your feedback').setStyle('PARAGRAPH').setRequired(true)
		);

		// @ts-expect-error TS is high
		const modal = new Modal().setCustomId('agb.feedback').setTitle('Feedback').addComponents(row1);

		await interaction.showModal(modal);
	}

	async roles(interaction: ButtonInteraction) {
		const selfRoles = this.container.botConfig.self_role;

		const pronouns = selfRoles
			.filter((r) => r.type === 'pronoun')
			.map((r) => {
				return {
					label: r.name,
					value: r.id,
					description: `${r.name.replaceAll(/pronouns/gi, '')} Pronouns`,
					emoji: r.emoji || undefined
				};
			});

		const ping = selfRoles
			.filter((r) => r.type === 'ping')
			.map((r) => {
				return {
					label: r.name,
					value: r.id,
					description: r.description,
					emoji: r.emoji || undefined
				};
			});

		const misc = selfRoles
			.filter((r) => r.type === 'misc')
			.map((r) => {
				return {
					label: r.name,
					value: r.id,
					description: r.description,
					emoji: r.emoji || undefined
				};
			});

		const buttons = [
			new MessageButton().setCustomId(generateButtonCommandId('roles', 'pronouns')),
			new MessageButton().setCustomId(generateButtonCommandId('roles', 'ping')),
			new MessageButton().setCustomId(generateButtonCommandId('roles', 'misc'))
			// new MessageButton().setCustomId(generateButtonCommandId('roles', 'colour'))
		];

		const row1 = new MessageActionRow().addComponents(buttons);

		await interaction.reply({
			embeds: [
				new MessageEmbed()
					.setTitle('Roles')
					.setDescription('Use the dropdowns to select your roles.')
					.setColor('GREEN')
					.setFooter({ text: 'You can select multiple roles' })
			],
			ephemeral: true,
			components: [row1]
		});
	}

	async bug(interaction: ButtonInteraction) {
		const row1 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent()
				.setCustomId('description')
				.setLabel('descriptiorn')
				.setPlaceholder('Enter a short description of the bug')
				.setStyle('SHORT')
				.setRequired(true)
		);
		const row2 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high

			new TextInputComponent()
				.setCustomId('expected')
				.setLabel('Expected Result')
				.setPlaceholder('What should happen?')
				.setStyle('SHORT')
				.setRequired(true)
		);
		const row3 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent()
				.setCustomId('actual')
				.setLabel('Actual Result')
				.setPlaceholder('What actually happened?')
				.setStyle('SHORT')
				.setRequired(true)
		);
		const row4 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent()
				.setCustomId('repro')
				.setLabel('Reproduction Steps')
				.setPlaceholder('How to reproduce the bug?')
				.setStyle('PARAGRAPH')
				.setRequired(true)
		);

		// @ts-expect-error TS is high
		const modal = new Modal().setCustomId('agb.bug').setTitle('Introduction').addComponents(row1, row2, row3, row4);

		await interaction.showModal(modal);
	}

	async roleinfo(interaction: ButtonInteraction) {
		const roles: Record<string, string> = {
			/* staff */
			'690285905176428604': '<@690281795618734330> himself.', // leader
			'924496283891294238': 'Responsible for developing, maintaining and hosting our custom bot <@526709237162967041>', // developer
			'693134233572737054': 'Responsible for the moderation of the server, deciding new staff members and rules.', // admin
			'738482067427295282': 'Responsible for the moderation of the server and rules.', // sr mod
			'734509461422735390': "Responsible for the moderation of Alex's streams, not the server.", // stream mod
			/* bots */
			'731442251695390743': 'Bots that are here for fun.', // fun bots
			'690293027838623816': 'Bots that help with the overall functionality of the server.', // util bots
			/* other */
			'700025683212107788': 'Given to people who use their Nitro Boost on the server', // booster
			'766301246544936970': 'Given to the winner of the Discord 2020 Halloween Event.', // booster
			/* notif */
			'709452739024781444': 'Get notified when <@690281795618734330> goes live', // stream ping
			'707649202779324416': 'Get notified of server announcements' // stream ping
		};

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Role Info')
			.setDescription(
				Object.keys(roles)
					.map((key) => `${roleMention(key)}: ${roles[key]}`)
					.join('\n')
			);

		await interaction.reply({
			embeds: [embed],
			ephemeral: true
		});
	}

	async tone(interaction: ButtonInteraction) {
		const tags: Record<string, string> = {
			j: 'joking',
			hj: 'half joking',
			's or /sarc': 'sarcastic / sarcasm',
			srs: 'serious',
			nsrs: 'not serious',
			lh: 'light hearted',
			'g or /gen': 'genuine / genuine question',
			ij: 'inside joke',
			ref: 'reference',
			t: 'teasing',
			nm: 'not mad',
			lu: 'a little upset',
			nf: 'not forced',
			nbh: 'nobody here',
			nsb: 'not subtweeting',
			nay: 'not at you',
			ay: 'at you',
			nbr: 'not being rude',
			ot: 'off topic',
			th: 'threat',
			cb: 'clickbait',
			f: 'fake',
			q: 'quote',
			l: 'lyrics',
			c: 'copypasta',
			m: 'metaphor / metaphorically',
			li: 'literal / literally',
			'rt or /rh ': 'rhetorical question',
			hyp: 'hyperbole',
			ex: 'exaggeration',
			p: 'platonic',
			r: 'romantic',
			a: 'alterous',
			'sx or /x': 'sexual intent',
			'nsx or /ns': 'non-sexual intent',
			'pc or /pos ': 'positive connotation',
			'nc or /neg ': 'negative connotation',
			neu: 'neutral / neutral connotation'
		};

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Tone Indicators')
			.setDescription(
				Object.keys(tags)
					.map((key) => `/${key} = ${tags[key]}`)
					.join('\n')
			);

		await interaction.reply({
			embeds: [embed],
			ephemeral: true,
			components: [
				new MessageActionRow().addComponents([new MessageButton().setURL('https://tonetags.carrd.co/#').setStyle('LINK').setLabel('Carrd')])
			]
		});
	}
}
