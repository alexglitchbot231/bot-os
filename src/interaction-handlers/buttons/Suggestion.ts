import { InteractionHandler, InteractionHandlerTypes, container } from '@sapphire/framework';
import { ButtonInteraction, MessageActionRow, MessageButton, MessageEmbed, Modal, TextInputComponent } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { UserFlagManager } from '#lib/UserFlagManager';
import { generateButtonCommandId } from '#lib/utils';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Suggestion extends InteractionHandler {
	async run(interaction: ButtonInteraction, { type, id }: InteractionHandler.ParseResult<this>) {
		await this[type](interaction, id);
	}

	private async up(i: ButtonInteraction, id: number) {
		const { Upvotes: upvotes, Downvotes: downvotes } = await container.prisma.suggestion.update({
			where: { Id: id },
			data: {
				Upvotes: { increment: 1 },
				Voters: {
					push: i.user.id
				}
			},
			select: { Upvotes: true, Downvotes: true }
		});

		const newEmbed = new MessageEmbed(i.message.embeds[0])
			.spliceFields(0, 1, {
				name: 'Upvotes',
				value: `${upvotes} (${Math.round((upvotes / (upvotes + downvotes)) * 100).toFixed(0)}%)`,
				inline: true
			})
			.spliceFields(1, 1, {
				name: 'Downvotes',
				value: `${downvotes} (${Math.round((downvotes / (upvotes + downvotes)) * 100).toFixed(0)}%)`,
				inline: true
			});

		await i.update({
			embeds: [newEmbed],
			components: [
				new MessageActionRow().addComponents(
					new MessageButton()
						.setCustomId(generateButtonCommandId('suggestionVote', 'up', id.toString()))
						.setEmoji('<:icons_upvote:909715386843430933>')
						.setStyle('PRIMARY'),
					// .setLabel(upvotes.toLocaleString()),
					new MessageButton()
						.setCustomId(generateButtonCommandId('suggestionVote', 'down', id.toString()))
						.setEmoji('<_:icons_downvote:911135418420953138>')
						.setStyle('DANGER')
					// .setLabel(downvotes.toLocaleString())
				)
			]
		});
	}

	private async down(i: ButtonInteraction, id: number) {
		const { Upvotes: upvotes, Downvotes: downvotes } = await container.prisma.suggestion.update({
			where: { Id: id },
			data: {
				Downvotes: { increment: 1 }
				// Voters: {
				// 	push: i.user.id
				// }
			},
			select: { Upvotes: true, Downvotes: true }
		});

		const newEmbed = new MessageEmbed(i.message.embeds[0])
			.spliceFields(0, 1, {
				name: 'Upvotes',
				value: `${upvotes} (${Math.round((upvotes / (upvotes + downvotes)) * 100).toFixed(0)}%)`,
				inline: true
			})
			.spliceFields(1, 1, {
				name: 'Downvotes',
				value: `${downvotes} (${Math.round((downvotes / (upvotes + downvotes)) * 100).toFixed(0)}%)`,
				inline: true
			});

		await i.update({
			embeds: [newEmbed],
			components: [
				new MessageActionRow().addComponents(
					new MessageButton()
						.setCustomId(generateButtonCommandId('suggestionVote', 'up', id.toString()))
						.setEmoji('<:icons_upvote:909715386843430933>')
						.setStyle('PRIMARY'),
					// .setLabel(upvotes.toLocaleString()),
					new MessageButton()
						.setCustomId(generateButtonCommandId('suggestionVote', 'down', id.toString()))
						.setEmoji('<_:icons_downvote:911135418420953138>')
						.setStyle('DANGER')
					// .setLabel(downvotes.toLocaleString())
				)
			]
		});
	}

	async parse(_interaction: ButtonInteraction) {
		const [name, type, id] = _interaction.customId.split(':') as [`agb.${string}`, 'up' | 'down', string];

		this.container.logger.debug({ name, type, id });
		if (name !== 'agb.suggestionVote') return this.none();

		const s = await container.prisma.suggestion.findUnique({ where: { Id: parseInt(id) } });

		if (s?.Voters.includes(_interaction.user.id)) {
			await _interaction.reply({ content: 'You have already voted on this suggestion', ephemeral: true });
			return this.none();
		}

		return this.some({ type, id: parseInt(id) });
	}
}
