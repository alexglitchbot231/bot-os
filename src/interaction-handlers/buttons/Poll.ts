import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { ButtonInteraction, MessageActionRow, MessageButton, TextChannel } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { generateButtonCommandId } from '#lib/utils';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Poll extends InteractionHandler {
	async run(interaction: ButtonInteraction, { type: _, pollId, vote }: InteractionHandler.ParseResult<this>) {
		const dbRes = await this.container.prisma.poll.update({
			where: {
				Id: parseInt(pollId)
			},
			data: {
				[vote === 'up' ? 'Upvotes' : 'Downvotes']: { increment: 1 },
				Voters: { push: interaction.user.id }
			},
			select: {
				Upvotes: true,
				Downvotes: true,
				MessageId: true
			}
		});

		const { Downvotes: downvotes, MessageId: messageId, Upvotes: upvotes } = dbRes;

		const g = await this.container.client.guilds.fetch('690282410675404854');
		const c = (await g.channels.fetch('697518288950788126')) as TextChannel;
		const m = await c.messages.fetch(messageId!);

		await m.edit({
			components: [
				new MessageActionRow().setComponents([
					new MessageButton()
						.setEmoji('⬆️')
						.setLabel(`${upvotes.toLocaleString()}`)
						.setCustomId(generateButtonCommandId('poll', 'vote', pollId.toString(), 'up'))
						.setStyle('PRIMARY'),
					new MessageButton()
						.setEmoji('⬇️')
						.setLabel(`${downvotes.toLocaleString()}`)
						.setCustomId(generateButtonCommandId('poll', 'vote', pollId.toString(), 'down'))
						.setStyle('DANGER')
				])
			]
		});

		await interaction.reply({
			content: 'Thanks for voting!',
			ephemeral: true
		});
	}

	async parse(_interaction: ButtonInteraction) {
		const [name, type, pollId, vote] = _interaction.customId.split(':') as [`agb.${string}`, 'vote', string, 'up' | 'down'];

		if (name !== 'agb.poll') return this.none();

		const { Voters: voters } = (await this.container.prisma.poll.findFirst({
			where: {
				Id: parseInt(pollId)
			},
			select: {
				Voters: true
			}
		}))!;

		if (voters.includes(_interaction.user.id)) {
			await _interaction.reply({
				content: 'You already voted!',
				ephemeral: true
			});
			return this.none();
		}

		if (name !== 'agb.poll') return this.none();

		return this.some({ type, pollId, vote });
	}
}
