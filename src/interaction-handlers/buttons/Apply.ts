import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import type { ButtonInteraction, User } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { UserFlagManager } from '#lib/UserFlagManager';
import { userMention } from '@discordjs/builders';
import { createDefaultEmbed, safeDM } from '#lib/utils';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Apply extends InteractionHandler {
	async run(interaction: ButtonInteraction, { type, action, id }: InteractionHandler.ParseResult<this>) {
		const u = await this.container.client.users.fetch(id);

		if (type === 'irl_friend') {
			await this.irlFriend(interaction, action, u);
		} else if (type === 'staff') {
			await this.staff(interaction, action, id);
		}
	}

	async irlFriend(i: ButtonInteraction, action: 'approve' | 'deny', user: User) {
		if (action === 'approve') {
			await new UserFlagManager(user).setFlag('IrlFriend');
			await i.reply({
				content: `Approved ${user.tag}`,
				ephemeral: true
			});

			await safeDM(user, {
				embeds: [
					createDefaultEmbed(user)
						.setTitle(`Your IRL Friend application has been approved!`)
						.setDescription(`You can now use </vip_server:998959694506889276> to join the VIP server!`)
				]
			});
		} else if (action === 'deny') {
			await safeDM(user, {
				embeds: [createDefaultEmbed(user).setTitle(`Your IRL Friend application has been denied!`)]
			});

			await i.reply({
				content: `Denied ${user.tag}`,
				ephemeral: true
			});
		}
	}

	async staff(i: ButtonInteraction, action: 'approve' | 'deny', id: string) {}

	async parse(_interaction: ButtonInteraction) {
		const [name, type, action, id] = _interaction.customId.split(':') as [`agb.${string}`, 'irl_friend' | 'staff', 'approve' | 'deny', string];

		if (name !== 'agb.apply') return this.none();

		if (['approve', 'deny'].includes(action) && _interaction.id !== '690281795618734330') {
			await _interaction.reply({
				content: 'The maze was not meant for you',
				ephemeral: true
			});
			return this.none();
		}

		return this.some({ type, action, id });
	}
}
