import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { ButtonInteraction, MessageActionRow, MessageButton } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { generateButtonCommandId } from '#lib/utils';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Giveaway extends InteractionHandler {
	async run(interaction: ButtonInteraction, { type }: InteractionHandler.ParseResult<this>) {
		Reflect.get(this, type).call(this, interaction);
	}

	parse(_interaction: ButtonInteraction) {
		const [name, type] = _interaction.customId.split(':') as [`agb.${string}`, 'enter' | 'leave'];

		if (name !== 'agb.giveaway') return this.none();

		return this.some({ type });
	}

	private async enter(interaction: ButtonInteraction) {
		const id = interaction.customId.split(':')[2];

		const giveaway = await this.container.prisma.giveaways.findFirst({
			where: {
				Id: parseInt(id)
			},
			select: {
				EnteredUsers: true
			}
		});

		if (giveaway?.EnteredUsers.includes(interaction.user.id)) {
			await interaction.reply({
				content: 'You have already entered this giveaway',
				ephemeral: true,
				components: [
					new MessageActionRow().setComponents([
						new MessageButton()
							.setLabel('Leave')
							.setCustomId(generateButtonCommandId('giveaway', 'leave', id.toString()))
							.setStyle('DANGER')
					])
				]
			});
			return;
		}

		await this.container.prisma.giveaways.update({
			where: {
				Id: parseInt(id)
			},
			data: {
				EnteredUsers: {
					push: interaction.user.id
				}
			}
		});

		await interaction.reply({
			content: 'You have entered the giveaway',
			ephemeral: true
		});
	}

	private async leave(interaction: ButtonInteraction) {
		const id = interaction.customId.split(':')[2];

		const giveaway = await this.container.prisma.giveaways.findFirst({
			where: {
				Id: parseInt(id)
			},
			select: {
				EnteredUsers: true
			}
		});

		const old = giveaway!.EnteredUsers;

		delete old[old.indexOf(interaction.user.id)];

		await this.container.prisma.giveaways.update({
			where: {
				Id: parseInt(id)
			},
			data: {
				EnteredUsers: old
			}
		});

		await interaction.reply({
			content: 'You left the giveaway',
			ephemeral: true
		});
	}
}
