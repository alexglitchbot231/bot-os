import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { ButtonInteraction, Message, MessageActionRow, MessageButton, TextChannel } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { UserFlagManager } from '#lib/UserFlagManager';
import { createDefaultEmbed, generateButtonCommandId } from '#lib/utils';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Unlock extends InteractionHandler {
	async run(interaction: ButtonInteraction, { channel }: InteractionHandler.ParseResult<this>) {
		await interaction.reply({
			embeds: [
				createDefaultEmbed(interaction.user)
					.setTitle('Channel unlocked')
					.setDescription(`🔓 ${channel} has been unlocked by ${interaction.user}`)
			]
		});

		await channel.permissionOverwrites.edit(interaction.guildId!, {
			SEND_MESSAGES: null,
			ADD_REACTIONS: null
		});

		const msg = interaction.message as Message;
		await msg.edit({
			embeds: msg.embeds,
			components: [
				new MessageActionRow().addComponents([
					new MessageButton()
						.setCustomId(generateButtonCommandId('unlock', channel.id))
						.setLabel('Unlock')
						.setStyle('DANGER')
						.setEmoji('🔓')
						.setDisabled(true)
				])
			]
		});
	}

	async parse(_interaction: ButtonInteraction) {
		const [name, id] = _interaction.customId.split(':') as [`agb.${string}`, string];
		const fMgr = new UserFlagManager(_interaction.user);
		if (name !== 'agb.unlock') return this.none();

		if (!(await fMgr.hasFlag('Staff'))) {
			await _interaction.reply({ content: 'You do not have permission to use this button', ephemeral: true });
			return this.none();
		}

		return this.some({
			channel: (await this.container.client.channels.fetch(id)) as TextChannel
		});
	}
}
