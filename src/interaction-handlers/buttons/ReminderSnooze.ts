import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { ButtonInteraction, MessageActionRow, MessageSelectMenu } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class ReminderSnooze extends InteractionHandler {
	async run(interaction: ButtonInteraction, { id }: InteractionHandler.ParseResult<this>) {
		const select = new MessageSelectMenu().setCustomId(`agb.reminder-snooze-select:${id}`).setOptions([
			{ label: '5 minutes', value: '5' },
			{ label: '10 minutes', value: '10' },
			{ label: '15 minutes', value: '15' },
			{ label: '30 minutes', value: '30' },
			{ label: '1 hour', value: '60' },
			{ label: '2 hours', value: '120' },
			{ label: '4 hours', value: '240' },
			{ label: '8 hours', value: '480' },
			{ label: '12 hours', value: '720' },
			{ label: '1 day', value: '1440' }
		]);

		await interaction.reply({
			content: 'How long would you like to snooze this reminder for?',
			components: [new MessageActionRow().addComponents(select)],
			ephemeral: true
		});
	}

	parse(_interaction: ButtonInteraction) {
		const [name, id] = _interaction.customId.split(':') as [`agb.${string}`, string];

		if (name !== 'agb.reminder-snooze') return this.none();

		return this.some({ id: parseInt(id) });
	}
}
