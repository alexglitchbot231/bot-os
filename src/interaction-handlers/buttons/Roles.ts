import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { ButtonInteraction, MessageActionRow, MessageButton, MessageEmbed, MessageSelectMenu } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Roles extends InteractionHandler {
	async run(interaction: ButtonInteraction, { type }: InteractionHandler.ParseResult<this>) {
		if (type === 'main') return this.main(interaction);
		else if (type === 'pronouns') return this.pronouns(interaction);
		else if (type === 'ping') return this.ping(interaction);
		else if (type === 'misc') return this.misc(interaction);
		else if (type === 'colour') return this.colour(interaction);
		else if (type === 'back') return this.main(interaction, true);
	}

	async main(interaction: ButtonInteraction, update = false) {
		const embed = new MessageEmbed().setColor('RANDOM').setTitle('Customize your roles').setDescription('Choose a category to choose roles.');

		if (update) {
			await interaction.update({
				embeds: [embed],
				components: [
					new MessageActionRow().addComponents(
						new MessageButton().setCustomId('agb.roles:pronouns').setLabel('Pronoun Roles').setStyle('PRIMARY'),
						new MessageButton().setCustomId('agb.roles:ping').setLabel('Announcement Roles').setStyle('PRIMARY'),
						new MessageButton().setCustomId('agb.roles:misc').setLabel('Miscellaneous Roles').setStyle('PRIMARY'),
						new MessageButton().setCustomId('agb.roles:colour').setLabel('Colour Roles').setStyle('PRIMARY')
					)
				]
			});
		} else {
			await interaction.reply({
				embeds: [embed],
				components: [
					new MessageActionRow().addComponents(
						new MessageButton().setCustomId('agb.roles:pronouns').setLabel('Pronoun Roles').setStyle('PRIMARY'),
						new MessageButton().setCustomId('agb.roles:ping').setLabel('Announcement Roles').setStyle('PRIMARY'),
						new MessageButton().setCustomId('agb.roles:misc').setLabel('Miscellaneous Roles').setStyle('PRIMARY'),
						new MessageButton().setCustomId('agb.roles:colour').setLabel('Colour Roles').setStyle('PRIMARY')
					)
				],
				ephemeral: true
			});
		}
	}

	async pronouns(interaction: ButtonInteraction) {
		const pronouns = this.container.botConfig.self_role
			.filter((r) => r.type === 'pronoun')
			.map((r) => {
				return {
					label: r.name,
					value: r.id,
					description: `${r.name.replaceAll(/pronouns/gi, '')} Pronouns`,
					emoji: r.emoji || undefined
				};
			});

		const embed = new MessageEmbed().setColor('RANDOM').setTitle('Pronoun Roles').setDescription('Choose your pronouns.');

		await interaction.update({
			embeds: [embed],
			components: [
				new MessageActionRow().addComponents(
					new MessageSelectMenu()
						.setCustomId('agb.roleSelect:pronouns')
						.setPlaceholder('Select your roles')
						.addOptions(pronouns)
						.setMaxValues(pronouns.length)
				),
				new MessageActionRow().addComponents(new MessageButton().setCustomId('agb.roles:back').setLabel('Back').setStyle('SECONDARY'))
			]
		});
	}

	async ping(interaction: ButtonInteraction) {
		const pronouns = this.container.botConfig.self_role
			.filter((r) => r.type === 'ping')
			.map((r) => {
				return {
					label: r.name,
					value: r.id,
					description: r.description,
					emoji: r.emoji || undefined
				};
			});

		const embed = new MessageEmbed()
			.setColor('RANDOM')
			.setTitle('Announcement Roles')
			.setDescription('Choose what announcements you want to receive.');

		await interaction.update({
			embeds: [embed],
			components: [
				new MessageActionRow().addComponents(
					new MessageSelectMenu()
						.setCustomId('agb.roleSelect:pronouns')
						.setPlaceholder('Select your roles')
						.addOptions(pronouns)
						.setMaxValues(pronouns.length)
				),
				new MessageActionRow().addComponents(new MessageButton().setCustomId('agb.roles:back').setLabel('Back').setStyle('SECONDARY'))
			]
		});
	}

	async misc(interaction: ButtonInteraction) {
		const pronouns = this.container.botConfig.self_role
			.filter((r) => r.type === 'misc')
			.map((r) => {
				return {
					label: r.name,
					value: r.id,
					description: r.description,
					emoji: r.emoji || undefined
				};
			});

		const embed = new MessageEmbed().setColor('RANDOM').setTitle('Misc Roles').setDescription('Choose what miscellaneous roles you want.');

		await interaction.update({
			embeds: [embed],
			components: [
				new MessageActionRow().addComponents(
					new MessageSelectMenu()
						.setCustomId('agb.roleSelect:pronouns')
						.setPlaceholder('Select your roles')
						.addOptions(pronouns)
						.setMaxValues(pronouns.length)
				),
				new MessageActionRow().addComponents(new MessageButton().setCustomId('agb.roles:back').setLabel('Back').setStyle('SECONDARY'))
			]
		});
	}

	async colour(interaction: ButtonInteraction) {
		const colours = this.container.botConfig.self_role
			.filter((r) => r.type === 'colour')
			.map((r) => {
				return {
					label: r.name,
					value: r.id,
					description: r.description,
					emoji: r.emoji || undefined
				};
			});

		const embed = new MessageEmbed().setColor('RANDOM').setTitle('Colour Roles').setDescription('Choose your colour.');

		await interaction.update({
			embeds: [embed],
			components: [
				new MessageActionRow().addComponents(
					new MessageSelectMenu().setCustomId('agb.colourSelect').setPlaceholder('Select your roles').addOptions(colours).setMaxValues(1)
				),
				new MessageActionRow().addComponents(new MessageButton().setCustomId('agb.roles:back').setLabel('Back').setStyle('SECONDARY'))
			]
		});
	}

	parse(_interaction: ButtonInteraction) {
		const [name, type] = _interaction.customId.split(':') as [`agb.${string}`, 'main' | 'pronouns' | 'ping' | 'misc' | 'colour' | 'back'];

		if (name !== 'agb.roles') return this.none();

		return this.some({ type });
	}
}
