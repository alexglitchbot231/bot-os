import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { ButtonInteraction, Message, MessageActionRow, MessageButton } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { createDefaultEmbed, generateButtonCommandId } from '#lib/utils';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.Button })
export class Avatar extends InteractionHandler {
	async run(interaction: ButtonInteraction, { type, id }: InteractionHandler.ParseResult<this>) {
		if (type === 'global') await this.globalPfp(interaction, id);
		else await this.guildPfp(interaction, id);

		// await interaction.reply({
		// 	content: `Now showing ${type} avatar`,
		// 	ephemeral: true
		// });
	}

	private async globalPfp(interaction: ButtonInteraction, id: string) {
		const user = await this.container.client.users.fetch(id);

		await interaction.update({
			embeds: [
				createDefaultEmbed(user)
					.setTitle(`${user.tag}'s Global avatar`)
					.setImage(
						user.avatarURL({ dynamic: true, size: 2048 }) ??
							`https://cdn.discordapp.com/embed/avatars/${parseInt(user.discriminator) % 5}.png`
					)
			],
			components: [
				new MessageActionRow().addComponents([
					new MessageButton().setCustomId(generateButtonCommandId('avatar', 'guild', id)).setLabel('Show Guild').setStyle('PRIMARY')
				])
			]
		});
	}
	private async guildPfp(interaction: ButtonInteraction, id: string) {
		const member = await this.container.client.guilds.cache.get(interaction.guildId!)!.members.fetch(id);

		await interaction.update({
			embeds: [
				createDefaultEmbed(member)
					.setTitle(`${member.user.tag}'s Server avatar`)
					.setImage(member.avatarURL({ dynamic: true, size: 2048 }) ?? '')
			],
			components: [
				new MessageActionRow().addComponents([
					new MessageButton().setCustomId(generateButtonCommandId('avatar', 'global', id)).setLabel('Show Global').setStyle('PRIMARY')
				])
			]
		});
	}

	parse(_interaction: ButtonInteraction) {
		const [name, type, id] = _interaction.customId.split(':') as [`agb.${string}`, 'global' | 'guild', string];

		if (name !== 'agb.avatar') return this.none();

		return this.some({ type, id });
	}
}
