import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { Formatters, SelectMenuInteraction } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import dayjs from 'dayjs';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.SelectMenu })
export class ReminderSnoozeSelect extends InteractionHandler {
	async run(interaction: SelectMenuInteraction, { id, minutes }: InteractionHandler.ParseResult<this>) {
		const date = dayjs().add(minutes, 'minute').toDate();
		await this.container.prisma.reminders.update({
			where: {
				Id: id
			},
			data: {
				EndAt: date,
				Active: true
			}
		});

		await interaction.update({
			content: `Snoozed until ${Formatters.time(date, 'R')}`,
			components: []
		});
	}

	parse(_interaction: SelectMenuInteraction) {
		const [name, id] = _interaction.customId.split(':') as [`agb.${string}`, string];

		const minutes = parseInt(_interaction.values[0]);

		this.container.logger.debug({ name, id, minutes });

		if (name !== 'agb.reminder-snooze-select') return this.none();

		return this.some({ id: parseInt(id), minutes });
	}
}
