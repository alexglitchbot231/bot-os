import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import type { GuildMember, SelectMenuInteraction } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { roleMention } from '@discordjs/builders';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.SelectMenu })
export class ColourSelect extends InteractionHandler {
	async run(interaction: SelectMenuInteraction, _: InteractionHandler.ParseResult<this>) {
		const colours = this.container.botConfig.self_role.filter((r) => r.type === 'colour');
		const ids = colours.map((r) => r.id);

		const selectedColour = interaction.values[0];

		await (interaction.member as GuildMember).roles.remove(ids);

		await (interaction.member as GuildMember).roles.add(selectedColour);

		await interaction.reply({ content: `You are now ${roleMention(selectedColour)}`, ephemeral: true, allowedMentions: { parse: [] } });
	}

	parse(_interaction: SelectMenuInteraction) {
		if (_interaction.customId !== 'agb.colourSelect') return this.none();

		return this.some({});
	}
}
