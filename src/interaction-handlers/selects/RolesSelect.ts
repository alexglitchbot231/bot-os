import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import type { GuildMember, SelectMenuInteraction } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { addOrRemoveRoles, createDefaultEmbed } from '#lib/utils';
import { UserFlagManager } from '#lib/UserFlagManager';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.SelectMenu })
export class RolesSelect extends InteractionHandler {
	async run(interaction: SelectMenuInteraction, { roles }: InteractionHandler.ParseResult<this>) {
		if (roles.includes('949328093900853258') && (await new UserFlagManager(interaction.user).hasFlag('SeriousBanned'))) {
			const embed = createDefaultEmbed(interaction.user)
				.setTitle('Failed to add roles')
				.setDescription("You can't add the role **Serious Access** because you are **Serious Banned**.");

			await interaction.reply({ embeds: [embed], ephemeral: true });
			return;
		}

		const { added, removed } = await addOrRemoveRoles(<GuildMember>interaction.member, ...roles);

		await interaction.reply({ content: `Added ${added} roles and removed ${removed} roles`, ephemeral: true });
	}

	parse(_interaction: SelectMenuInteraction) {
		const [name] = _interaction.customId.split(':');

		if (name !== 'agb.roleSelect') return this.none();

		return this.some({ roles: _interaction.values });
	}
}
