import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import type { GuildMember, SelectMenuInteraction, TextChannel } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { UserFlagManager } from '#lib/UserFlagManager';
import type { AGBUserFlagsType } from '#lib/constants';
import { createDefaultEmbed } from '#lib/utils';
import { stripIndents } from 'common-tags';
import dayjs from 'dayjs';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.SelectMenu })
export class AgbShopBuy extends InteractionHandler {
	async run(interaction: SelectMenuInteraction, _: InteractionHandler.ParseResult<this>) {
		const itemId = interaction.values[0];

		const item = await this.container.prisma.shopItem.findFirst({ where: { Id: parseInt(itemId) } })!;

		const econMgr = this.container.econManager;

		if ((await econMgr.getBalance(interaction.user)) <= item!.Price) {
			return interaction.reply({
				content: `You don't have enough money to buy this item!`,
				ephemeral: true
			});
			return;
		}

		switch (item!.Type) {
			case 'Role':
				const role = await interaction.guild!.roles.fetch(item!.Role!);
				await econMgr.removeBalance(interaction.user, item!.Price, `Brought ${role?.name}`);
				await (interaction.member! as GuildMember).roles.add(role!);
				break;
			case 'Flag':
				await econMgr.removeBalance(interaction.user, item!.Price, `Brought ${item!.Flag}`);
				// const flag = item!.Flag!; // as any as typeof BuyableFlags;
				const flag = item!.Flag! as AGBUserFlagsType;

				this.container.logger.debug(`Brought ${flag}`);
				// @ts-ignor
				// await new UserFlagManager(interaction.user).setFlag(flag.filter((f) => f === item!.Name)[0]);
				await new UserFlagManager(interaction.user).setFlag(flag);
				break;
			case 'Item':
				await econMgr.removeBalance(interaction.user, item!.Price, `Brought ${item!.Name}`);
				await this.container.prisma.inventory.upsert({
					where: { usersId: interaction.user.id },
					update: {
						Items: {
							connect: {
								Id: item!.Id
							}
						}
					},
					create: {
						usersId: interaction.user.id,
						Items: {
							connect: {
								Id: item!.Id
							}
						}
					}
				});
				break;
			case 'Multiplier':
				await econMgr.removeBalance(interaction.user, item!.Price, `Brought ${item!.Name}`);
				await this.container.prisma.users.upsert({
					where: { Id: interaction.user.id },
					update: {
						Mulitplier: item!.Multiplier!,
						MultiplierExpire: dayjs().add(item?.MultiplierLength!, 'hour').toDate()
					},
					create: {
						Id: interaction.user.id,
						Mulitplier: item!.Multiplier!,
						MultiplierExpire: dayjs().add(item?.MultiplierLength!, 'hour').toDate()
					}
				});
		}

		await interaction.reply({
			content: `You bought ${item!.Name} for ${item!.Price}`,
			ephemeral: true
		});

		const c = (await interaction.guild!.channels.fetch('996885473249198130')) as TextChannel;
		await c.send({
			embeds: [
				createDefaultEmbed(interaction.user).setDescription(stripIndents`
					**Price**: $${item?.Price.toLocaleString()}
					**Item**: ${item?.Name}
					`)
			]
		});
	}

	parse(_interaction: SelectMenuInteraction) {
		if (_interaction.customId !== 'agb.shopBuy') return this.none();

		return this.some({});
	}
}
