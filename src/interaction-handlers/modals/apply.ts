import { createDefaultEmbed } from '#lib/utils';
import { ApplyOptions } from '@sapphire/decorators';
import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { MessageActionRow, ModalSubmitInteraction, TextChannel, MessageButton } from 'discord.js';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.ModalSubmit })
export class Apply extends InteractionHandler {
	async run(interaction: ModalSubmitInteraction, { type }: InteractionHandler.ParseResult<this>) {
		if (type === 'irl_friend') {
			await this.irlFriend(interaction);
		} else if (type === 'staff') {
			// do something else
		}
	}

	async irlFriend(i: ModalSubmitInteraction) {
		const name = i.fields.getTextInputValue('name');
		const how = i.fields.getTextInputValue('how');

		const embed = createDefaultEmbed(i.user)
			.setTitle('New IRL Friend Application')
			.addField('What is your IRL name?', name)
			.addField('How do you know Alex', how);

		const c = (await this.container.client.channels.fetch('954422103925465091')) as TextChannel;

		await c.send({
			embeds: [embed],
			components: [
				new MessageActionRow().addComponents(
					new MessageButton().setCustomId(`agb.apply:irl_friend:approve:${i.user.id}`).setLabel('Approve').setStyle('SUCCESS'),
					new MessageButton().setCustomId(`agb.apply:irl_friend:deny:${i.user.id}`).setLabel('Deny').setStyle('DANGER')
				)
			]
		});

		await i.reply({
			content: 'You have applied for the IRL Friend role',
			ephemeral: true
		});
	}

	async staff(i: ModalSubmitInteraction) {}

	async parse(_interaction: ModalSubmitInteraction) {
		const [name, type] = _interaction.customId.split(':') as [`agb.${string}`, 'irl_friend' | 'staff'];

		if (name !== 'agb.apply') return this.none();

		return this.some({ type });
	}
}
