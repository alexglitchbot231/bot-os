import { zws } from '#lib/constants';
import { createDefaultEmbed, generateButtonCommandId } from '#lib/utils';
import { ApplyOptions } from '@sapphire/decorators';
import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { GuildMember, MessageActionRow, MessageButton, ModalSubmitInteraction, TextChannel } from 'discord.js';

@ApplyOptions<InteractionHandler.Options>({
	interactionHandlerType: InteractionHandlerTypes.ModalSubmit
})
export class Suggest extends InteractionHandler {
	parse(_interaction: ModalSubmitInteraction) {
		const [name] = _interaction.customId.split(':') as [`agb.${string}`];

		if (name !== 'agb.suggest') return this.none();

		return this.some({});
	}

	async run(interaction: ModalSubmitInteraction, _: InteractionHandler.ParseResult<this>) {
		const suggestion = interaction.fields.getTextInputValue('suggestion');

		const embed = createDefaultEmbed(interaction.member as GuildMember)
			.setTitle('Suggestion')
			.setDescription(suggestion)
			.addFields([
				{
					name: 'Upvotes',
					value: '0 (50%)',
					inline: true
				},
				{
					name: 'Downvotes',
					value: '0 (50%)',
					inline: true
				}
			]);

		const { Id } = await this.container.prisma.suggestion.create({
			data: {
				Category: 'Server',
				Content: suggestion,
				Downvotes: 0,
				Upvotes: 0,
				UserId: interaction.user.id
			},
			select: { Id: true }
		});

		const c = (await interaction.guild!.channels.fetch('987387468212998204')) as TextChannel;

		const votingButtons = [
			new MessageButton()
				.setCustomId(generateButtonCommandId('suggestionVote', 'up', Id.toString()))
				// .setEmoji('⬆️')
				.setEmoji('<:icons_upvote:909715386843430933>')
				.setStyle('PRIMARY'),
			// .setLabel('0'),
			new MessageButton()
				.setCustomId(generateButtonCommandId('suggestionVote', 'down', Id.toString()))
				.setEmoji('<_:icons_downvote:911135418420953138>')
				// .setEmoji('⬇️')
				.setStyle('DANGER')
			// .setLabel('0')
		];
		const reply = await c.send({
			embeds: [embed],
			components: [new MessageActionRow().setComponents(...votingButtons)]
		});

		interaction.reply({ content: 'Sent', ephemeral: true });

		await (reply.channel as TextChannel).threads.create({
			startMessage: reply,
			name: `Suggestion #${Id}`
		});

		await this.container.prisma.suggestion.update({
			where: { Id },
			data: { MessageID: reply.id }
		});
	}
}
