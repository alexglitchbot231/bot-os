import { pickRandom } from '#lib/utils';
import { userMention } from '@discordjs/builders';
import { ApplyOptions } from '@sapphire/decorators';
import { InteractionHandler, InteractionHandlerTypes } from '@sapphire/framework';
import { randomInt } from 'crypto';
import { MessageActionRow, MessageButton, MessageEmbed, ModalSubmitInteraction, TextChannel } from 'discord.js';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.ModalSubmit })
export class Confess extends InteractionHandler {
	async run(interaction: ModalSubmitInteraction, _: InteractionHandler.ParseResult<this>) {
		const confession = interaction.fields.getTextInputValue('confession');

		const lowerCase = 'abcdefghijklmnopqrstuvwxyz'.split('');
		const upperCase = lowerCase.map((l) => l.toUpperCase());

		const characters = [...lowerCase, ...upperCase];

		const length = 10;

		const name = Array.from({ length }, () => pickRandom(characters)).join('');
		const discrim = randomInt(1, 9999).toString().padStart(4, '0');

		const userEmbed = new MessageEmbed()
			.setAuthor({ name: `${name}#${discrim}` })
			.setTitle('Confession')
			.setDescription(confession);

		const c = (await this.container.client.channels.fetch('1048192569873530910')) as TextChannel;

		const m = await c.send({ embeds: [userEmbed] });

		const staffEmbed = new MessageEmbed(userEmbed).addField('Author', `${userMention(interaction.user.id)} (${interaction.user.tag})`);

		const sc = (await this.container.client.channels.fetch('1048195546864685176')) as TextChannel;

		await sc.send({ embeds: [staffEmbed] });
		await interaction.reply({
			content: 'Your confession has been sent',
			ephemeral: true,
			components: [new MessageActionRow().addComponents(new MessageButton().setURL(m.url).setLabel('Jump').setStyle('LINK'))]
		});
	}

	async parse(_interaction: ModalSubmitInteraction) {
		const [name] = _interaction.customId.split(':') as [`agb.${string}`];

		if (name !== 'agb.confess') return this.none();

		return this.some({});
	}
}
