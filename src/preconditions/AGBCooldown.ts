import { fullCommandName } from '#lib/utils';
import { Precondition } from '@sapphire/framework';
import dayjs from 'dayjs';
import { CommandInteraction, Formatters } from 'discord.js';

export class UserPrecondition extends Precondition {
	public async chatInputRun(interaction: CommandInteraction) {
		const cd = await this.container.prisma.commandCooldown.findFirst({
			where: {
				UserId: interaction.user.id,
				CommandName: await fullCommandName(interaction)
			}
		});

		if (!cd) return this.ok();

		if (cd.CooldownEnd.getTime() < Date.now()) {
			await this.container.prisma.commandCooldown.delete({
				where: {
					Id: cd.Id
				}
			});
			return this.ok();
		}
		return this.error({
			identifier: 'agbCooldown',
			message: `You are on cooldown for this command. Please try again in ${Formatters.time(dayjs().diff(dayjs(cd.CooldownEnd)))}.`,
			context: {
				// end: dayjs(cd.CooldownEnd).diff(dayjs())
				end: dayjs(cd.CooldownEnd).diff(dayjs())
			}
		});
	}
}

declare module '@sapphire/framework' {
	interface Preconditions {
		AGBCooldown: {
			end: number;
		};
	}
}
