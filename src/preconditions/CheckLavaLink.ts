import { Precondition } from '@sapphire/framework';
import type { CommandInteraction } from 'discord.js';

export class UserPrecondition extends Precondition {
	public async chatInputRun(_interaction: CommandInteraction) {
		if (!this.container.lavalinkEnabled) return this.error({ message: 'LavaLink Disabled.', identifier: 'lavalinkDisabled' });

		return this.container.lavalinkConnected
			? this.ok()
			: this.error({ message: 'LavaLink is not connected.', identifier: 'lavalinkDisconnected' });
	}
}
