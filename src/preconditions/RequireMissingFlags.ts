import { Command, Precondition, PreconditionContext } from '@sapphire/framework';
import { AGBUserFlagsType, AGBUserReadableNames } from '#lib/constants';
import type { CommandInteraction } from 'discord.js';
import { UserFlagManager } from '#lib/UserFlagManager';

export interface RequireFlagPreconditionContext extends PreconditionContext {
	flags?: AGBUserFlagsType;
}

export class UserPrecondition extends Precondition {
	public async chatInputRun(interaction: CommandInteraction, _: Command, context: RequireFlagPreconditionContext) {
		if (!context.flags) return this.ok();

		const flagNames = AGBUserReadableNames[context.flags];

		const flagMgr = new UserFlagManager(interaction.user);

		if (await flagMgr.hasFlag(context.flags)) {
			return this.error({
				identifier: 'preconditionFlag',
				context: {
					flags: flagNames
				}
			});
		}
		return this.ok();
	}
}
