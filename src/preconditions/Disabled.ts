import type AgbCommandOptions from '#lib/interfaces/AgbCommandOptions';
import type AgbCommand from '#lib/structures/AgbCommand';
import { container, Precondition } from '@sapphire/framework';
import type { CommandInteraction } from 'discord.js';

export class UserPrecondition extends Precondition {
	public async chatInputRun(interaction: CommandInteraction, cmd: AgbCommand) {
		const { disabledReason } = cmd.options as AgbCommandOptions;
		return disabledReason ? this.error({ message: `This command has been disabled because: ${disabledReason}` }) : this.ok();
	}
}

declare module '@sapphire/framework' {
	interface Preconditions {
		Disabled: never;
	}
}
