import { userMention } from '@discordjs/builders';
import { Precondition } from '@sapphire/framework';
import type { CommandInteraction } from 'discord.js';

export class UserPrecondition extends Precondition {
	public async chatInputRun(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user', true);

		const res = await this.container.prisma.users.findFirst({
			where: {
				Id: user.id
			},
			select: {
				HugsEnabled: true
			}
		});

		this.container.logger.debug(res);
		if (!res?.HugsEnabled) {
			return this.error({ message: `${userMention(user.id)} has disabled hugs.`, identifier: 'hugsDisabled' });
		}

		return this.ok();
	}
}
