import { UserFlagManager } from '#lib/UserFlagManager';
import { Precondition } from '@sapphire/framework';
import type { CommandInteraction, ContextMenuInteraction } from 'discord.js';

export class UserPrecondition extends Precondition {
	private async _check(i: CommandInteraction | ContextMenuInteraction) {
		const user = i.user;
		const { BlackistReason } = (await this.container.prisma.users.findFirst({ where: { Id: user.id }, select: { BlackistReason: true } }))!;

		return (await new UserFlagManager(user).hasFlag('Blacklisted'))
			? this.error({ message: 'You have been blacklisted from the bot.', identifier: 'blacklisted', context: { BlackistReason } })
			: this.ok();
	}

	public async chatInputRun(interaction: CommandInteraction) {
		return this._check(interaction);
	}

	public async contextMenuRun(interaction: ContextMenuInteraction) {
		return this._check(interaction);
	}
}
