import type { PrismaClient } from '@prisma/client';
import { container } from '@sapphire/framework';
import type { Message, TextChannel, User } from 'discord.js';
import EventEmitter from 'events';
import type { BalanceChangePayload } from './interfaces/BalanceChangePayload';
import type { ChallengeLeaderboardEntry } from './interfaces/ChallengeLeaderboardEntry';
import type { LeaderboardEntry } from './interfaces/LeaderboardEntry';
import { pickRandom } from './utils';
import type { ChallengeType } from './structures/ChallengeType';
import { MathChallengeType, ScrambleChallengeType, WordChallengeType } from '../challenge-types';

export class EconomyManager extends EventEmitter {
	private prisma: PrismaClient;
	private _emitter: EventEmitter = new EventEmitter();

	constructor() {
		super();
		this.prisma = container.prisma;
		// this._emitter = new EventEmitter();
	}

	get emitter() {
		return this._emitter;
	}

	public static toCurrencyString(amount: number): string {
		return `$${amount.toLocaleString()}`;
	}

	public async getBalance({ id }: User): Promise<number> {
		const user = await container.prisma.users.findFirst({
			where: {
				Id: id
			},
			select: {
				Money: true
			}
		});

		if (!user) {
			await this.prisma.users.create({
				data: {
					Id: id,
					Money: 0,
					JobHistory: ''
				}
			});
			return 0;
		}

		return user.Money;
	}

	public async addBalance({ id }: User, amount: number, reason: string): Promise<number> {
		const { Money: money } = await container.prisma.users.upsert({
			create: {
				Id: id,
				Money: amount,
				JobHistory: ''
			},
			update: {
				Money: {
					increment: amount
				}
			},
			where: {
				Id: id
			},
			select: {
				Money: true
			}
		});

		this._emitter.emit('balanceChange', {
			target: id,
			actionBy: id,
			amount,
			reason,
			newBalance: money
		} as BalanceChangePayload);

		return money;
	}

	public async removeBalance({ id }: User, amount: number, reason: string): Promise<number> {
		const { Money: money } = await container.prisma.users.upsert({
			create: {
				Id: id,
				Money: amount,
				JobHistory: ''
			},
			update: {
				Money: {
					decrement: amount
				}
			},
			where: {
				Id: id
			},
			select: {
				Money: true
			}
		});

		this._emitter.emit('balanceChange', {
			target: id,
			actionBy: id,
			amount,
			reason,
			newBalance: money
		} as BalanceChangePayload);

		return money;
	}

	public async getLeaderboard(): Promise<LeaderboardEntry> {
		const users = await container.prisma.users.findMany({
			take: 10,
			orderBy: {
				Money: 'desc'
			},
			select: {
				Id: true,
				Money: true
			},
			where: {
				Money: { gt: 0 }
			}
		});

		return {
			members: users.map((user, index) => ({
				placement: index + 1,
				userId: user.Id,
				money: user.Money
			})),
			total: users.reduce((acc, user) => acc + user.Money, 0)
		};
	}

	public async getChallengeLeaderboard(): Promise<ChallengeLeaderboardEntry> {
		const users = await container.prisma.users.findMany({
			take: 10,
			orderBy: {
				WonChallenges: 'desc'
			},
			select: {
				Id: true,
				WonChallenges: true
			},
			where: {
				WonChallenges: { gt: 0 }
			}
		});

		return {
			members: users.map((user, index) => ({
				placement: index + 1,
				userId: user.Id,
				wonChallenges: user.WonChallenges
			}))
		};
	}

	public async createChallenge(omsg?: Message) {
		// const types: ChallengeType[] = [new MathChallengeType(), new WordChallengeType(), new ScrambleChallengeType()];
		const types = container.challengeTypes;

		const res = await pickRandom(types).run(omsg!.channel as TextChannel);

		if (!res.completed) return;

		const { user, newPoints, response, type, msg } = res;

		container.emitter.emit('challengeCompleted', {
			type: type!,
			user: user!,
			response: response!,
			newPoints: newPoints!,
			msg: msg!
		});
	}

	public async getMultiplierForUser(user: User): Promise<number> {
		const res = await container.prisma.users.findFirst({
			where: { Id: user.id },
			select: { Mulitplier: true }
		});

		return res?.Mulitplier ?? 1;
	}

	public async setMultiplierForUser(user: User, multiplier: number): Promise<void> {
		await container.prisma.users.upsert({
			where: { Id: user.id },
			update: { Mulitplier: multiplier },
			create: {
				Id: user.id,
				Mulitplier: multiplier
			}
		});
	}
}
