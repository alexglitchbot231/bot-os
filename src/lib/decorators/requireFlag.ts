import { createFunctionPrecondition } from '@sapphire/decorators';
import { container } from '@sapphire/framework';
import type { CommandInteraction } from 'discord.js';
import type { AGBUserFlagsType } from '#lib/constants';
import { UserFlagManager } from '#lib/UserFlagManager';

export function RequireFlag(flags: AGBUserFlagsType): MethodDecorator {
	return createFunctionPrecondition(
		async (i: CommandInteraction) => {
			return await new UserFlagManager(i.user).hasFlag(flags);
		},
		(i: CommandInteraction) => {
			// @ts-ignore
			container.client.emit(
				'chatInputCommandDenied',
				{
					context: { flags },
					identifier: 'preconditionNoFlag'
				},
				{ command: i.command, interaction: i, context: { flags } }
			);
		}
	);
}
