import { createFunctionPrecondition } from '@sapphire/decorators';
import { container } from '@sapphire/framework';
import type { CommandInteraction } from 'discord.js';
import { fullCommandName } from '#lib/utils';

/**
 * A god damn awful decorator to check if a user has enough money to run a command.
 * @param cost The cost of the command
 * @returns
 */
export function CommandCost(cost: number): MethodDecorator {
	return createFunctionPrecondition(async (i: CommandInteraction) => {
		const { Money } = (await container.prisma.users.findFirst({
			where: { Id: i.user.id },
			select: { Money: true }
		}))!;

		if (Money < cost) {
			container.client.emit(
				// @ts-ignore what the fuck lol
				'chatInputCommandDenied',
				{
					context: { cost },
					identifier: 'notEnoughMoney',
					message: `You need ${cost} to use this command.`,
					name: 'Cost'
				},
				{ command: i.command, interaction: i, context: { cost } }
			);
			return false;
		}

		await container.econManager.removeBalance(i.user, cost, `${await fullCommandName(i)} command`);
		return true;
	});
}
