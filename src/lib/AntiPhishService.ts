import { fetch } from '@sapphire/fetch';
import { container } from '@sapphire/framework';
import { green } from 'ansi-colors';
import { WebSocket } from 'ws';

export class AntiPhishService {
	private httpUrl = 'https://phish.sinking.yachts/v2/all';
	private wsUrl = 'wss://phish.sinking.yachts/feed';
	private xIdentity = 'AlexGlitchBot By Badstagram (discord.gg/F3dMf7DqFY)';
	private urls = new Set<string>();

	public async fetchUrls(): Promise<number> {
		const response = await fetch<string[]>(this.httpUrl, {
			headers: {
				'X-Identity': this.xIdentity
			}
		});
		this.urls = new Set(response);
		container.scamDomains = Array.from(this.urls);
		container.logger.info(`Loaded ${green(this.urls.size.toLocaleString())} url${this.urls.size === 1 ? '' : 's'}`);
		return this.urls.size;
	}

	public async startWS(): Promise<void> {
		const ws = new WebSocket(this.wsUrl, {
			headers: {
				'X-Identity': this.xIdentity
			}
		});
		ws.onmessage = (message) => {
			const parsed = JSON.parse(message.data.toString()) as { type: 'add' | 'delete'; domains: string[] };
			const type = parsed.type;
			switch (type) {
				case 'add':
					this.addUrl(parsed.domains);
					break;
				case 'delete':
					this.removeUrl(parsed.domains);
					break;
			}
		};

		container.scamDomainWsConnected = true;
	}

	private addUrl(urls: string[]) {
		urls.push(...urls);
		container.logger.info(`Added ${green(urls.length.toLocaleString())} url${urls.length === 1 ? '' : 's'}`);
	}

	private removeUrl(urls: string[]) {
		urls.forEach((url) => this.urls.delete(url));
		container.logger.info(`Removed ${green(urls.length.toLocaleString())} url${urls.length === 1 ? '' : 's'}`);
	}

	public getUrls(): string[] {
		return Array.from(this.urls);
	}
}
