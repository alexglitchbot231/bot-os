import type { APIApplicationCommandOptionChoice } from 'discord-api-types/v10';
import { ApplicationCommandRegistry, RegisterBehavior } from '@sapphire/framework';
import { join } from 'path';

export const rootDir = join(__dirname, '..', '..');
export const srcDir = join(rootDir, 'src');
export const confDir = join(rootDir, 'config');

export const RandomLoadingMessage = ['Computing...', 'Thinking...', 'Cooking some food', 'Give me a moment', 'Loading...'];

export const DefaultChatInputOptions: ApplicationCommandRegistry.RegisterOptions = {
	behaviorWhenNotIdentical: RegisterBehavior.Overwrite,
	registerCommandIfMissing: true,
	guildIds: ['690282410675404854', '739115569311383634']
};

export const zws = '\u200b';

const pad = ' '.repeat(7);

export const SudoWarning = [
	'We trust you have received the usual lecture from the local System',
	'Administrator. It usually boils down to these three things:',
	pad,
	`${pad}#1) Respect the privacy of others.`,
	`${pad}#2) Think before you type.`,
	`${pad}#3) With great power comes great responsibility.`
].join('\n');

export const AGBUserFlags = {
	Developer: 1 << 0,
	Blacklisted: 1 << 1,
	EconBanned: 1 << 2,
	MusicBanned: 1 << 3,
	PatreonSupporter: 1 << 4,
	Furry: 1 << 5,
	EconomyManager: 1 << 6,
	StaffApplicationBlacklisted: 1 << 7,
	IRLFriendApplicationBlacklisted: 1 << 8,
	StaffApplicationCooldown: 1 << 9,
	DJ: 1 << 10,
	SuggestionManager: 1 << 11,
	SeriousBanned: 1 << 12,
	IrlFriend: 1 << 13,
	Staff: 1 << 14,
	ConfessionBanned: 1 << 15
} as const;

export type AGBUserFlagsType = keyof typeof AGBUserFlags;

export const InternalFlags: AGBUserFlagsType[] = ['StaffApplicationBlacklisted', 'IRLFriendApplicationBlacklisted', 'StaffApplicationCooldown'];

export const AGBUserReadableNames = {
	Developer: 'Developer',
	Blacklisted: 'Blacklisted',
	EconBanned: 'Econ Banned',
	MusicBanned: 'Music Banned',
	PatreonSupporter: 'Patreon Supporter',
	Furry: 'Furry',
	EconomyManager: 'Economy Manager',
	StaffApplicationBlacklisted: 'Staff Application Blacklisted',
	IRLFriendApplicationBlacklisted: 'IRL Friend Application Blacklisted',
	StaffApplicationCooldown: 'Staff Application Cooldown',
	DJ: 'DJ',
	SuggestionManager: 'Suggestion Manager',
	SeriousBanned: 'Serious Banned',
	IrlFriend: 'IRL Friend',
	Staff: 'Staff',
	ConfessionBanned: 'Confession Banned'
} as const;

export type AGBUserFlagsNames = keyof typeof AGBUserFlags;

export const AGBUserFlagsOptions: APIApplicationCommandOptionChoice<string>[] = Object.keys(AGBUserFlags)
	.map((key) => key as AGBUserFlagsType)
	.map((f) => {
		return {
			name: AGBUserReadableNames[f],
			value: f
		};
	});

export const BuyableFlags = ['DJ'] as const;

export const BuyableFlagsOptions: APIApplicationCommandOptionChoice<string>[] = BuyableFlags.map((key) => key as AGBUserFlagsType).map((f) => {
	return {
		name: AGBUserReadableNames[f],
		value: f
	};
});
export const Jobs = [
	'Doctor',
	'Police Officer',
	'Postman',
	'Mcdonalds Worker',
	'KFC Manager',
	'Google CEO',
	'Pizza Delivery Driver',
	'Youtuber',
	'Actor',
	'Tesco Worker',
	'Software Engineer'
];

export const Emojis = {
	id: '<:_:860133546102620190>',
	bot: '<:_:865488228789649429>',
	connector: '<:_:1033770464063000606>',
	endConnector: '<:_:1033770462846664735>',
	join: '<:_:875754472834469948>',
	leave: '<:_:875754473023229972>',
	tick: '<:_:1033771529164574801>',
	cross: '<:_:1033771530427043931>',
	blank: '<:_:1033773467746709524>',
	channels: {
		category: '<:_:860123643659681802>',
		text: '<:_:859424401950113822>',
		voice: '<:_:860133545544908802>',
		stage: '<:_:979965205436055582>',
		thread: '<:_:866694823972438056>'
	},
	emojis: {
		emoji: '<:_:874587985218244649>',
		sticker: '<:_:861124851435831317>'
	},
	features: {
		partner: '<:_:861852633027182612>',
		commerce: '<:_:988409333782020216>',
		discovery: '<:_:859429432535023666>',
		news: '<:_:859424400456679445>',
		store: '<:_:859424401950113822>',
		vipRegions: '<:_:860133545884123136>',
		community: '<:_:964425853930995783>',
		gating: '<:_:949635040252428318>',
		moreRoles: '<:_:866605189029298197>',
		verified: '<:_:859424400939286549>',
		tickedEvents: '<:_:860123644520562698>'
	},
	pronouns: {
		hh: '886586234883932181',
		sh: '886586235567624243',
		tt: '886586234800078889'
	} as const
};

export const CommandModules = [
	'admin',
	'animal',
	'apply',
	'birthday',
	'challenge',
	'config',
	'economy',
	'fun',
	'giveaway',
	'info',
	'mod',
	'music',
	'poll',
	'reminder',
	'shop',
	'staff',
	'suggest',
	'tag'
] as const;

export type CommandModule = typeof CommandModules[number];

export const CommandModuleOptions: APIApplicationCommandOptionChoice<string>[] = CommandModules.map((m) => {
	return {
		name: m,
		value: m
	};
});
