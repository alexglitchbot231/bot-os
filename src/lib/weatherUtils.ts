import { FetchResultTypes } from '@sapphire/fetch';
import { CurrentCondition, Icons, ResolvedConditions, Theme, Weather, WeatherCode, WeatherName, WeatherTheme } from './interfaces/Weather';
import { fetch } from '@sapphire/fetch';
import { loadImage, Image } from 'canvas-constructor/napi-rs';
import { join } from 'path';
import { rootDir } from './constants';

export async function getData(location: string): Promise<Weather> {
	const url = new URL(`https://wttr.in/${location}`);

	url.searchParams.set('format', 'j1');
	url.searchParams.set('lang', 'en');

	const result = await fetch(url, FetchResultTypes.Text).catch(() => null);

	if (result === null) throw 'Failed to fetch weather data';

	if (result.startsWith('{')) {
		const data = JSON.parse(result) as Weather;
		return data;
	}

	if (result.startsWith('Unknown location')) {
		throw 'Unknown location';
	}

	throw 'Unknown error while fetching weather data';
}

const getWeatherNameMap = new Map<WeatherCode, WeatherName>([
	[WeatherCode.ClearOrSunny, 'Sunny'],
	[WeatherCode.PartlyCloudy, 'PartlyCloudy'],
	[WeatherCode.Cloudy, 'Cloudy'],
	[WeatherCode.Overcast, 'VeryCloudy'],
	[WeatherCode.Mist, 'Fog'],
	[WeatherCode.PatchyRainNearby, 'LightShowers'],
	[WeatherCode.PatchySnowNearby, 'LightSleetShowers'],
	[WeatherCode.PatchySleetNearby, 'LightSleet'],
	[WeatherCode.PatchyFreezingDrizzleNearby, 'LightSleet'],
	[WeatherCode.ThunderyOutbreaksInNearby, 'ThunderyShowers'],
	[WeatherCode.BlowingSnow, 'LightSnow'],
	[WeatherCode.Blizzard, 'HeavySnow'],
	[WeatherCode.Fog, 'Fog'],
	[WeatherCode.FreezingFog, 'Fog'],
	[WeatherCode.PatchyLightDrizzle, 'LightShowers'],
	[WeatherCode.LightDrizzle, 'LightRain'],
	[WeatherCode.FreezingDrizzle, 'LightSleet'],
	[WeatherCode.HeavyFreezingDrizzle, 'LightSleet'],
	[WeatherCode.PatchyLightRain, 'LightRain'],
	[WeatherCode.LightRain, 'LightRain'],
	[WeatherCode.ModerateRainAtTimes, 'HeavyShowers'],
	[WeatherCode.ModerateRain, 'HeavyRain'],
	[WeatherCode.HeavyRainAtTimes, 'HeavyShowers'],
	[WeatherCode.HeavyRain, 'HeavyRain'],
	[WeatherCode.LightFreezingRain, 'LightSleet'],
	[WeatherCode.ModerateOrHeavyFreezingRain, 'LightSleet'],
	[WeatherCode.LightSleet, 'LightSleet'],
	[WeatherCode.ModerateOrHeavySleet, 'LightSnow'],
	[WeatherCode.PatchyLightSnow, 'LightSnowShowers'],
	[WeatherCode.LightSnow, 'LightSnowShowers'],
	[WeatherCode.PatchyModerateSnow, 'HeavySnow'],
	[WeatherCode.ModerateSnow, 'HeavySnow'],
	[WeatherCode.PatchyHeavySnow, 'HeavySnowShowers'],
	[WeatherCode.HeavySnow, 'HeavySnow'],
	[WeatherCode.IcePellets, 'LightSleet'],
	[WeatherCode.LightRainShower, 'LightShowers'],
	[WeatherCode.ModerateOrHeavyRainShower, 'HeavyShowers'],
	[WeatherCode.TorrentialRainShower, 'HeavyShowers'],
	[WeatherCode.LightSleetShowers, 'LightSleetShowers'],
	[WeatherCode.ModerateOrHeavySleetShowers, 'LightSleetShowers'],
	[WeatherCode.LightSnowShowers, 'LightSnowShowers'],
	[WeatherCode.ModerateOrHeavySnowShowers, 'LightSnowShowers'],
	[WeatherCode.LightShowersOfIcePellets, 'LightSleetShowers'],
	[WeatherCode.ModerateOrHeavyShowersOfIcePellets, 'LightSleet'],
	[WeatherCode.PatchyLightRainInAreaWithThunder, 'ThunderyShowers'],
	[WeatherCode.ModerateOrHeavyRainInAreaWithThunder, 'ThunderyHeavyRain'],
	[WeatherCode.PatchyLightSnowInAreaWithThunder, 'ThunderySnowShowers'],
	[WeatherCode.ModerateOrHeavySnowInAreaWithThunder, 'ThunderySnowShowers']
]);

export function getWeatherName(code: WeatherCode): WeatherName {
	const name = getWeatherNameMap.get(code);
	if (code === undefined) throw 'Unknown weather code';
	return name!;
}

export function resolveData(conditions: CurrentCondition): ResolvedConditions {
	return {
		precipitation: `${conditions.precipMM} mm`,
		pressure: `${conditions.pressure} Pa`,
		temperature: `${conditions.temp_C}°C`,
		visibility: `${conditions.visibility} km`,
		windSpeed: `${conditions.windspeedKmph} km/h`
	};
}

export function getColors(name: WeatherName): WeatherTheme {
	switch (name) {
		case 'LightShowers':
		case 'LightSleetShowers':
		case 'LightSnowShowers':
		case 'LightRain':
		case 'LightSleet':
		case 'LightSnow':
		case 'HeavySnow':
		case 'HeavySnowShowers':
		case 'Cloudy':
		case 'Fog':
			return { background: '#2E2E2E', text: '#FAFAFA', theme: 'light' };
		case 'HeavyRain':
		case 'HeavyShowers':
		case 'VeryCloudy':
			return { background: '#EAEAEA', text: '#1F1F1F', theme: 'dark' };
		case 'PartlyCloudy':
		case 'Sunny':
			return { background: '#0096D6', text: '#FAFAFA', theme: 'light' };
		case 'ThunderyHeavyRain':
		case 'ThunderyShowers':
		case 'ThunderySnowShowers':
			return { background: '#99446B', text: '#FAFAFA', theme: 'light' };
		default:
			throw new Error(`Could not find weather name '${name}'.`);
	}
}

const weatherFolder = join(rootDir, 'assets', 'weather', 'icons');
const getFileCache = new Map<WeatherName, Image>();
export async function getFile(name: WeatherName): Promise<Image> {
	const existing = getFileCache.get(name);
	if (existing !== undefined) return existing;

	const image = await loadImage(join(weatherFolder, `${name}.png`));
	getFileCache.set(name, image);
	return image;
}

const getIconsCache = new Map<Theme, Icons>();
export async function getIcons(theme: Theme): Promise<Icons> {
	const existing = getIconsCache.get(theme);
	if (existing !== undefined) return existing;

	const [pointer, precipitation, temperature, visibility] = await Promise.all([
		loadImage(join(weatherFolder, theme, 'pointer.png')),
		loadImage(join(weatherFolder, theme, 'precipitation.png')),
		loadImage(join(weatherFolder, theme, 'temperature.png')),
		loadImage(join(weatherFolder, theme, 'visibility.png'))
	]);

	const icons: Icons = { pointer, precipitation, temperature, visibility };
	getIconsCache.set(theme, icons);
	return icons;
}
