export interface BalanceChangePayload {
	target: string;
	actionBy: string;
	amount: number;
	reason: string;
	newBalance: number;
}
