import type { Message, User } from 'discord.js';
import type { ChallengeType } from '#lib/structures';

export interface ChallengeCompletedPayload {
	completed: boolean;
	user?: User;
	type?: ChallengeType;
	response?: string;
	newPoints?: number;
	msg?: Message;
}

export type ChallengeCompletedPayloadReturnType = Promise<ChallengeCompletedPayload>;
