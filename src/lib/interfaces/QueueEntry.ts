import type { User } from 'discord.js';
import type { Track } from 'shoukaku';

export interface QueueEntry {
	track: Track;
	requester: User;
}
