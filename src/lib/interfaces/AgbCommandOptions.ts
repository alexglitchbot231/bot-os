import type { Command } from '@sapphire/framework';
import type { AGBUserFlagsType, CommandModule } from '../constants';

export interface AgbCommandOptions extends Command.Options {
	requireLavaLink?: boolean;
	requireFlags?: AGBUserFlagsType[];
	requireMissingFlags?: AGBUserFlagsType[];
	disabledReason?: string;
	module: CommandModule;
}
