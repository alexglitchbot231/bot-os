export interface ChallengeLeaderboardEntry {
	members: { placement: number; userId: string; wonChallenges: number }[];
}
