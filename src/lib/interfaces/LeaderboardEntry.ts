export interface LeaderboardEntry {
	members: { placement: number; userId: string; money: number }[];
	total: number;
}
