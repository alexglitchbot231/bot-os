import type { Subcommand } from '@sapphire/plugin-subcommands';
import type { AgbCommandOptions } from '#lib/interfaces';

export interface AgbSubCommandOptions extends Subcommand.Options, AgbCommandOptions {
	// requireLavaLink?: boolean;
	// requireFlags?: AGBUserFlagsType[];
	// requireMissingFlags?: AGBUserFlagsType[];
}
