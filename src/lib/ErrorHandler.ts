import * as Sentry from '@sentry/node';
import type { CommandInteraction, TextChannel } from 'discord.js';
import { fullCommandName } from '#lib/utils';

export class ErrorHandler {
	public async handleCommandError(err: Error, i: CommandInteraction): Promise<void> {
		const opts = i.options.data
			.filter((o) => o.value !== null)
			.filter((o) => !['SUB_COMMAND', 'SUB_COMMAND_GROUP'].includes(o.type))
			.map((o) => `${o.name}: ${o.value}`);

		const commandCtx: Record<string, string> = {
			name: await fullCommandName(i),
			id: i.commandId
		};

		if (opts.length) {
			commandCtx.options = opts.join(' ');
		}

		Sentry.setContext('Command', commandCtx);

		Sentry.setContext('User', {
			id: i.user.id,
			tag: i.user.tag
		});

		Sentry.setContext('Channel', {
			id: i.channel!.id,
			name: (i.channel! as TextChannel).name
		});

		Sentry.captureException(err);
	}
}
