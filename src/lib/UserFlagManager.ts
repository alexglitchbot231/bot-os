import { container } from '@sapphire/framework';
import type { User } from 'discord.js';
import { AGBUserFlags, AGBUserFlagsType, AGBUserReadableNames } from './constants';

export class UserFlagManager {
	private readonly _user: User;

	constructor(user: User) {
		this._user = user;
	}

	public async hasFlag(flags: AGBUserFlagsType): Promise<boolean> {
		const user = await container.prisma.users.findFirst({
			where: {
				Id: this._user.id
			},
			select: {
				Flags: true
			}
		});

		if (!user) {
			await container.prisma.users.create({
				data: {
					Id: this._user.id,
					Money: 0,
					JobHistory: '',
					Flags: 0
				}
			});
			return false;
		}

		const dbFlags = user.Flags;

		return (dbFlags & AGBUserFlags[flags]) != 0;
	}

	public async setFlag(flag: AGBUserFlagsType): Promise<void> {
		const user = await container.prisma.users.findFirst({
			where: {
				Id: this._user.id
			},
			select: {
				Flags: true
			}
		});

		if (!user) {
			await container.prisma.users.create({
				data: {
					Id: this._user.id,
					Money: 0,
					JobHistory: '',
					Flags: AGBUserFlags[flag] //.map((f) => AGBUserFlags[f]).reduce((a, b) => a | b, 0)
				}
			});
			return;
		}

		const dbFlags = user.Flags;
		const newFlags = dbFlags | AGBUserFlags[flag]; //.map((f) => AGBUserFlags[f]).reduce((a, b) => a | b, 0);
		await container.prisma.users.update({
			where: {
				Id: this._user.id
			},
			data: {
				Flags: newFlags
			}
		});
	}

	public async unsetFlag(flag: AGBUserFlagsType): Promise<void> {
		const user = await container.prisma.users.findFirst({
			where: {
				Id: this._user.id
			},
			select: {
				Flags: true
			}
		});

		if (!user) {
			await container.prisma.users.create({
				data: {
					Id: this._user.id,
					Money: 0,
					JobHistory: '',
					Flags: 0
				}
			});
			return;
		}

		let dbFlags = user.Flags;
		const newFlags = (dbFlags &= ~AGBUserFlags[flag]);

		container.logger.debug({ newFlags, dbFlags });

		await container.prisma.users.update({
			where: {
				Id: this._user.id
			},
			data: {
				Flags: newFlags
			}
		});
	}

	public async getFlags(): Promise<string[]> {
		const user = await container.prisma.users.findFirst({
			where: {
				Id: this._user.id
			},
			select: {
				Flags: true
			}
		});

		if (!user) {
			await container.prisma.users.create({
				data: {
					Id: this._user.id,
					Money: 0,
					JobHistory: '',
					Flags: 0
				}
			});
			return [];
		}

		// const vals = getAllEnumKeys(AGBUserFlags)
		// 	.map((key) => AGBUserFlags[key])
		// 	.filter((f) => this.hasFlag(f))
		// 	.map((f) => AGBUserReadableNames[f]);

		const keys = Object.keys(AGBUserFlags).map((key) => key as AGBUserFlagsType);

		const vals: string[] = [];
		for (const key of keys) {
			if (await this.hasFlag(key)) {
				vals.push(AGBUserReadableNames[key]);
			}
		}

		return vals;
	}
}
