// Unless explicitly defined, set NODE_ENV as development:
console.log(process.env.NODE_ENV);
process.env.NODE_ENV ??= 'development';

import 'reflect-metadata';
import '@sapphire/plugin-logger/register';
// import '@kaname-png/plugin-env/register';

import 'source-map-support/register';

import * as colorette from 'colorette';
import { inspect } from 'util';

import RelativeTime from 'dayjs/plugin/relativeTime';
import dayjs from 'dayjs';
import { container } from '@sapphire/framework';

import { readFileSync } from 'fs';
import { confDir } from './constants';
import { join } from 'path';
import type { BotConfig } from '#lib/interfaces/BotConfig';
import * as TOML from '@iarna/toml';

import * as Sentry from '@sentry/node';

dayjs.extend(RelativeTime);

// Read env var
// config({ debug: process.env.NODE_ENV === 'development' });

// Set default inspection depth
inspect.defaultOptions.depth = 1;

// Enable colorette
colorette.createColors({ useColor: true });

container.scamDomainWsConnected = false;

const raw = readFileSync(join(confDir, 'config.toml'), 'utf-8');
container.botConfig = TOML.parse(raw) as any as BotConfig;

Sentry.init({
	dsn: container.botConfig.secrets.sentry
});
