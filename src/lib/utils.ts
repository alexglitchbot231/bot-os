import { fetch } from '@sapphire/fetch';
import { ChatInputCommand, container } from '@sapphire/framework';
import { isNullish } from '@sapphire/utilities';
import {
	ButtonInteraction,
	ColorResolvable,
	CommandInteraction,
	EmbedFooterData,
	Guild,
	GuildMember,
	InteractionReplyOptions,
	Message,
	MessageActionRow,
	MessageButton,
	MessageEmbed,
	MessageOptions,
	MessagePayload,
	ModalSubmitInteraction,
	SelectMenuInteraction,
	TextChannel,
	User
} from 'discord.js';
import { readFile, writeFile } from 'fs/promises';
import { join } from 'path';
import { confDir } from '#lib/constants';
import type { EconomyConfig } from '#lib/interfaces/EconomyConfig';
import type { Joke } from '#lib/interfaces/Joke';
import type { Result, Tenor } from '#lib/interfaces/Tenor';
import dayjs from 'dayjs';
import { userMention } from '@discordjs/builders';

/**
 * Picks a random item from an array
 * @param array The array to pick a random item from
 * @example
 * const randomEntry = pickRandom([1, 2, 3, 4]) // 1
 */
export function pickRandom<T>(array: readonly T[]): T {
	const { length } = array;
	return array[Math.floor(Math.random() * length)];
}

export function takeFromArray<T>(arr: readonly T[], amount = 1, random = false): T[] {
	const array: T[] = [];

	for (let i = 0; i < amount; i++) {
		array.push(random ? pickRandom(arr) : arr[i]);
	}

	return array;
}

/**
 * Create a default embed
 * @param user The user
 */
export function createDefaultEmbed(user: User | GuildMember): MessageEmbed {
	const tag = user instanceof GuildMember ? user.user.tag : user.tag;
	const colours = [
		0xff0000, // red,
		0x00ff00, // green
		0x0000ff, // blue
		0x000000, // white
		0xca5cdd, // purple
		0xf0e68c // gold
	] as const;
	return new MessageEmbed().setColor(pickRandom(colours)).setAuthor({ name: tag, iconURL: user.displayAvatarURL({ dynamic: true }) });
}

export async function respondError(opts: {
	interaction: CommandInteraction;
	error: Error;
	footer?: EmbedFooterData;
	opts?: InteractionReplyOptions;
}) {
	const { interaction, error } = opts;
	const embed = createDefaultEmbed(interaction.user)
		.setTitle('Something went wrong')
		.setColor('RED')
		.setDescription(error.message)
		.setFooter(opts.footer ?? null);

	if (interaction.replied || interaction.deferred) await interaction.editReply({ ...opts.opts, embeds: [embed] });
	else await interaction.reply({ ...opts.opts, embeds: [embed], ephemeral: true });
}

export function generateButtonCommandId(name: string, ...args: string[]) {
	return `agb.${name}:${args.join(':')}`;
}

export async function addOrRemoveRoles(member: GuildMember, ...roles: string[]): Promise<{ added: number; removed: number }> {
	let added = 0;
	let removed = 0;

	roles.forEach((r) => {
		if (member.roles.cache.has(r)) {
			member.roles.remove(r);
			removed++;
		} else {
			member.roles.add(r);
			added++;
		}
	});

	return { added, removed };
}

type AllTypesInteraction = CommandInteraction | SelectMenuInteraction | ButtonInteraction | ModalSubmitInteraction;

interface ConfirmOptions {
	interaction: AllTypesInteraction;
	message: string;
	valueIfConfirmed?: string;
	valueIfCancelled?: string;
	firstReply?: boolean;
	ephemeral?: boolean;
}
export async function confirm(opts: ConfirmOptions) {
	let { interaction, message, ephemeral } = opts;
	ephemeral ??= false;

	const embed = createDefaultEmbed(interaction.user).setTitle(message).setColor('GREEN').setDescription(message);

	const row = new MessageActionRow().setComponents([
		new MessageButton().setLabel('Confirm').setStyle('SUCCESS').setCustomId('confirm'),
		new MessageButton().setLabel('Cancel').setStyle('DANGER').setCustomId('cancel')
	]);

	const reply = interaction.reply;

	container.logger.debug({ deferred: interaction.deferred, replied: interaction.replied });

	await reply({ embeds: [embed], allowedMentions: { users: [interaction.user.id], roles: [] }, components: [row], ephemeral });

	const awaitedButton = await interaction.channel?.awaitMessageComponent({
		componentType: 'BUTTON',
		filter: (msg) => msg.user.id === interaction.user.id && ['confirm', 'cancel'].includes(msg.customId)
	});

	if (!awaitedButton) {
		// await reply({ embeds: [embed.setDescription(valueIfCancelled)], allowedMentions: { users: [interaction.user.id], roles: [] } });
		return false;
	}

	if (awaitedButton?.customId === 'confirm') {
		// await reply({ embeds: [embed.setDescription(valueIfConfirmed)], allowedMentions: { users: [interaction.user.id], roles: [] } });
		return true;
	}

	if (awaitedButton?.customId === 'cancel') {
		// await reply({ embeds: [embed.setDescription(valueIfCancelled)], allowedMentions: { users: [interaction.user.id], roles: [] } });
		return false;
	}

	// await reply({ embeds: [embed.setDescription(valueIfCancelled)], allowedMentions: { users: [interaction.user.id], roles: [] } });
	return false;
}

export async function safeDM(user: User, opts: string | MessagePayload | MessageOptions): Promise<{ success: boolean; message?: Message }> {
	try {
		const message = await user.send(opts);
		return { success: true, message };
	} catch {
		return { success: false };
	}
}

export async function randomGif(search: string, count = 1): Promise<Result[]> {
	const url = new URL('https://tenor.googleapis.com/v2/search');
	url.searchParams.append('key', container.botConfig.secrets.tenor);
	url.searchParams.append('q', search);
	url.searchParams.append('limit', count.toString());
	url.searchParams.append('client_key', 'alexglitchbot231');

	const res = await fetch<Tenor>(url);

	return res.results;
}

export async function fullCommandName(i: CommandInteraction): Promise<string> {
	const group = i.options.getSubcommandGroup(false);
	const sc = i.options.getSubcommand(false);
	const name = i.command!.name;

	return `${[name, group, sc]
		.filter((x) => !isNullish(x))
		.map((x) => x!.trim())
		.join(' ')
		.trim()}`;
}

export function probably(percentage: number) {
	if (percentage < 0 || percentage > 100) throw new Error('Percentage must be between 0..100');

	const chance = Math.random() * 100;

	return chance < percentage;
}

export function capatilise(s: string) {
	return s
		.split(' ')
		.map((word) => word[0].toUpperCase() + word.substring(1))
		.join(' ');
}

export async function sleep(ms: number) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

export function youtubeThumbnail(vid: string): `https://i.ytimg.com/vi/${string}/hqdefault.jpg` {
	return `https://i.ytimg.com/vi/${vid}/hqdefault.jpg`;
}

type JokeType = 'programming' | 'misc' | 'dark' | 'pun' | 'spooky' | 'christmas';
type JokeBlacklist = 'nsfw' | 'religious' | 'political' | 'racist' | 'sexist' | 'explicit';

export async function getJoke(
	type: JokeType[] | 'any',
	blacklist: JokeBlacklist[] = ['nsfw', 'religious', 'political', 'racist', 'sexist', 'explicit']
) {
	const url = new URL(`https://sv443.net/jokeapi/v2/joke/${type === 'any' ? `any` : `${type.join(',')}`}`);
	url.searchParams.append('type', 'twopart');
	url.searchParams.append('blacklistFlags', blacklist.join(','));

	return fetch<Joke>(url);
}

export async function loadEconomyConfig(reload = false) {
	container.logger.debug(`${reload ? 'Reloading' : 'Loading'} economy config`);
	const now = dayjs();

	const fName = process.env.NODE_ENV === 'production' ? 'econ_config.json' : 'econ_config.development.json';

	const file = await readFile(join(confDir, fName), 'utf-8');

	const config = JSON.parse(file) as EconomyConfig;

	container.econConfig = config;

	container.logger.debug(`${reload ? 'Reloaded' : 'Loaded'} economy config in ${dayjs().diff(now, 'ms')}ms`);
}

export async function saveEconomyConfig() {
	container.logger.debug(`Saving economy config`);
	const now = dayjs();

	const fName = process.env.NODE_ENV === 'production' ? 'econ_config.json' : 'econ_config.development.json';

	await writeFile(join(confDir, fName), JSON.stringify(container.econConfig, null, 4));

	container.logger.debug(`Saved economy config in ${dayjs().diff(now, 'ms')}ms`);
}

export async function userToMember(user: User, guild: Guild): Promise<GuildMember | null> {
	try {
		return await guild.members.fetch(user.id);
	} catch (e) {
		return null;
	}
}

interface PunishEmbedOpts {
	user: User | GuildMember;
	moderator: GuildMember;
	reason: string;
	id: number;
	type: 'ban' | 'kick' | 'timeout' | 'warn';
	guild: Guild;
	expire?: Date;
}
export async function sendPunishEmbed({ user, moderator, reason, id, type, expire, guild }: PunishEmbedOpts) {
	const colourMapping: Record<typeof type, ColorResolvable> = {
		ban: 'RED',
		kick: 'YELLOW',
		timeout: 'ORANGE',
		warn: 'YELLOW'
	} as const;

	const hallOfShame = (await guild.channels.fetch('690292726583132263')) as TextChannel;

	const embed = new MessageEmbed()
		.setColor(colourMapping[type])
		.setAuthor({
			name: `Punishment #${id} - ${type}`,
			iconURL: user.displayAvatarURL({ dynamic: true })
		})
		.addFields([
			{
				name: 'User',
				value: `${user} (${userMention(user.id)})`,
				inline: false
			},
			{
				name: 'Moderator',
				value: `${moderator}`,
				inline: true
			},
			{
				name: 'Expires',
				value: expire ? `${dayjs(expire).format('DD/MM/YYYY HH:mm')}` : 'Never',
				inline: true
			},
			{
				name: 'Reason',
				value: reason,
				inline: true
			}
		]);

	await hallOfShame.send({ embeds: [embed] });
}

export function pluralise(count: number, ifOne: string, ifMany?: string) {
	ifMany ??= `${ifOne}s`;
	return count === 1 ? ifOne : ifMany;
}

export async function loadDisabledModules() {
	const res = await container.prisma.botConfig.findFirst({ select: { DisabledModules: true } });

	container.disabledModules = res?.DisabledModules ?? [];
}
