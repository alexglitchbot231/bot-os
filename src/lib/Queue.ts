export class Queue<T> {
	private _items: T[];

	constructor(...params: T[]) {
		console.log(params);
		this._items = [...params];
	}

	enqueue(item: T) {
		this._items.push(item);
	}

	dequeue(): T | undefined {
		return this._items.shift();
	}

	clear() {
		this._items = [];
	}

	shuffle() {
		this._items = this._items.sort(() => Math.random() - 0.5);
	}

	remove(index: number): T {
		const item = this._items[index];
		delete this._items[index];
		return item;
	}

	get items(): T[] {
		return this._items;
	}
}
