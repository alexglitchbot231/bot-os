import type { Message, TextChannel } from 'discord.js';
import type { ChallengeCompletedPayloadReturnType } from '#lib/interfaces';

export abstract class ChallengeType {
	public abstract readonly name: string;
	public abstract run(channel: TextChannel): ChallengeCompletedPayloadReturnType;
}
