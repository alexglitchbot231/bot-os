import { Command, PieceContext } from '@sapphire/framework';
import type { CommandInteraction } from 'discord.js';
import type { AgbCommandOptions } from '#lib/interfaces/AgbCommandOptions';
import dayjs from 'dayjs';

export abstract class AgbCommand extends Command {
	public constructor(ctx: PieceContext, opts: AgbCommandOptions) {
		super(ctx, opts);
	}

	public abstract chatInputRun(interaction: CommandInteraction): Promise<void>;

	protected parseConstructorPreConditions(options: AgbCommandOptions): void {
		this.parseConstructorPreConditionsRunIn(options);
		this.parseConstructorPreConditionsNsfw(options);
		this.parseConstructorPreConditionsRequiredClientPermissions(options);
		this.parseConstructorPreConditionsRequiredUserPermissions(options);
		this.parseConstructorPreConditionsOverwrittenCooldown(options);
		this.parseConstructorPreConditionsLavalink(options);
		this.parseConstructorPreConditionsRequireFlag(options);
		this.parseConstructorPreConditionsRequireMissingFlag(options);
		this.parseConstructorPreConditionsBlacklist();
		this.parseConstructorPreConditionsDisabled(options);
	}

	protected parseConstructorPreConditionsDisabled(options: AgbCommandOptions) {
		if (options.disabledReason) this.preconditions.append('Disabled');
	}

	protected parseConstructorPreConditionsLavalink(options: AgbCommandOptions) {
		if (options.requireLavaLink) this.preconditions.append('CheckLavaLink');
	}

	protected parseConstructorPreConditionsRequireFlag(options: AgbCommandOptions) {
		if (options.requireFlags)
			this.preconditions.append({
				name: 'RequireFlags',
				context: {
					flags: options.requireFlags
				}
			});
	}

	protected parseConstructorPreConditionsRequireMissingFlag(options: AgbCommandOptions) {
		if (options.requireMissingFlags)
			this.preconditions.append({
				name: 'RequireMissingFlags',
				context: {
					flags: options.requireMissingFlags
				}
			});
	}

	protected parseConstructorPreConditionsBlacklist() {
		this.preconditions.append('Blacklist');
	}

	protected parseConstructorPreConditionsOverwrittenCooldown(options: AgbCommandOptions) {
		if (options.cooldownDelay && options.cooldownDelay > 0) {
			this.preconditions.append({
				name: 'AGBCooldown',
				context: {
					end: dayjs().diff(options.cooldownDelay)
				}
			});
		}
	}
}
