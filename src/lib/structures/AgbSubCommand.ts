import type { PieceContext } from '@sapphire/framework';
import type { CommandInteraction } from 'discord.js';
import type AgbSubCommandOptions from '#lib/interfaces/AgbSubCommandOptions';
import { Subcommand } from '@sapphire/plugin-subcommands';

export abstract class AgbSubCommand extends Subcommand {
	public constructor(ctx: PieceContext, opts: AgbSubCommandOptions) {
		super(ctx, opts);
	}

	// public abstract chatInputRun(interaction: CommandInteraction): Promise<void>;

	protected parseConstructorPreConditions(options: AgbSubCommandOptions): void {
		this.parseConstructorPreConditionsRunIn(options);
		this.parseConstructorPreConditionsNsfw(options);
		this.parseConstructorPreConditionsRequiredClientPermissions(options);
		this.parseConstructorPreConditionsRequiredUserPermissions(options);
		this.parseConstructorPreConditionsCooldown(options);
		this.parseConstructorPreConditionsLavalink(options);
		this.parseConstructorPreConditionsRequireFlag(options);
		this.parseConstructorPreConditionsRequireMissingFlag(options);
		this.parseConstructorPreConditionsBlacklist();
	}

	protected parseConstructorPreConditionsLavalink(options: AgbSubCommandOptions) {
		if (options.requireLavaLink) this.preconditions.append('CheckLavaLink');
	}

	protected parseConstructorPreConditionsRequireFlag(options: AgbSubCommandOptions) {
		if (options.requireFlags)
			this.preconditions.append({
				name: 'RequireFlags',
				context: {
					flags: options.requireFlags
				}
			});
	}

	protected parseConstructorPreConditionsRequireMissingFlag(options: AgbSubCommandOptions) {
		if (options.requireMissingFlags)
			this.preconditions.append({
				name: 'RequireMissingFlags',
				context: {
					flags: options.requireMissingFlags
				}
			});
	}

	protected parseConstructorPreConditionsBlacklist() {
		this.preconditions.append('Blacklist');
	}
}
