import { time, TimestampStyles, userMention } from '@discordjs/builders';
import { container } from '@sapphire/framework';
import { Time } from '@sapphire/time-utilities';
import type { TextChannel } from 'discord.js';
import { TaskTimer } from 'tasktimer';
import { createDefaultEmbed, safeDM } from './utils';

export class Timers {
	private _reminderTimer: TaskTimer = new TaskTimer({ interval: Time.Minute });

	public startAll() {
		this._reminderTimer.add(Timers._reminder);
		this._reminderTimer.start();
	}

	private static async _reminder() {
		const { prisma, client, logger } = container;

		const reminders = await prisma.reminders.findMany({
			where: {
				EndAt: {
					lte: new Date()
				}
			}
		});

		for (const reminder of reminders) {
			const { ChannelId, Text, StartedAt, Id, UserId } = reminder;

			await prisma.reminders.delete({
				where: {
					Id
				}
			});

			const user = await client.users.fetch(UserId);
			const embed = createDefaultEmbed(user)
				.setTitle('Reminder')
				.setColor('GREEN')
				.setDescription(`Hey ${user.username}, ${time(StartedAt!, TimestampStyles.RelativeTime)} ago, you wanted to be reminded ${Text}`);

			const msg = await safeDM(user, { embeds: [embed] });

			if (!msg.success) {
				logger.info(`Unable to DM ${user.tag} (${user.id})... Failing back to channel`);
				const c = (await client.channels.fetch(ChannelId)) as TextChannel;
				c.send({ content: userMention(UserId), embeds: [embed] });
			}

			logger.info(`Reminder ${Text} sent to ${user.tag} (${user.id})`);
		}
	}
}
