import type { Guild, GuildFeatures } from 'discord.js';
import { Emojis } from './constants';
import { capatilise } from './utils';

export function parseGuildFeatures(guild: Guild): string[] {
	const { features } = guild;

	const mapping: Record<GuildFeatures, string> = {
		ANIMATED_ICON: ``,
		BANNER: ``,
		COMMERCE: `${Emojis.features.commerce} `,
		DISCOVERABLE: `${Emojis.features.discovery} `,
		FEATURABLE: `${Emojis.features.discovery} `,
		INVITE_SPLASH: ``,
		NEWS: `${Emojis.features.news} `,
		PARTNERED: `${Emojis.features.partner} `,
		VANITY_URL: ``,
		VIP_REGIONS: `${Emojis.features.vipRegions} `,
		WELCOME_SCREEN_ENABLED: ``,
		COMMUNITY: `${Emojis.features.community} `,
		MEMBER_VERIFICATION_GATE_ENABLED: `${Emojis.features.gating} `,
		MONETIZATION_ENABLED: `${Emojis.features.commerce} `,
		MORE_STICKERS: `${Emojis.emojis.sticker} `,
		PREVIEW_ENABLED: ``,
		PRIVATE_THREADS: ``,
		ROLE_ICONS: `${Emojis.features.moreRoles} `,
		SEVEN_DAY_THREAD_ARCHIVE: `${Emojis.channels.thread} `,
		THREE_DAY_THREAD_ARCHIVE: `${Emojis.channels.thread} `,
		VERIFIED: `${Emojis.features.verified} `,
		TICKETED_EVENTS_ENABLED: `${Emojis.features.tickedEvents} `
	};

	return features.map((f) => `${mapping[f] ?? ''} ${capatilise(f.replaceAll(/_/g, ' ').toLocaleLowerCase())}` ?? f.replaceAll('_', '_'));
}
