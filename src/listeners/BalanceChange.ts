import { container, Listener, ListenerOptions, PieceContext } from '@sapphire/framework';
import type { TextChannel } from 'discord.js';
import { EconomyManager } from '#lib/EconomyManager';
import type { BalanceChangePayload } from '#lib/interfaces/BalanceChangePayload';
import { createDefaultEmbed } from '#lib/utils';

export class UserEvent extends Listener<'balanceChange'> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: 'balanceChange',
			emitter: container.econManager.emitter
		});
	}
	public async run({ target, actionBy, amount, reason, newBalance }: BalanceChangePayload) {
		const fetchedTarget = await this.container.client.users.fetch(target);
		const fetchedBy = await this.container.client.users.fetch(actionBy);
		const embed = createDefaultEmbed(fetchedTarget).setTitle('Balance Change').setDescription(`
		**User**: ${fetchedTarget.tag} (${fetchedTarget.id})
		**Updated By**: ${fetchedBy.tag} (${fetchedBy.id})
		**Amount**: ${EconomyManager.toCurrencyString(amount)}
		**Reason**: ${reason}
		**New Balance**: ${EconomyManager.toCurrencyString(newBalance)}
		`);

		((await this.container.client.channels.fetch('692539063957454868')) as TextChannel).send({ embeds: [embed] });
	}
}
