import { InteractionHandler, InteractionHandlerTypes, Listener, ListenerOptions, PieceContext } from '@sapphire/framework';
import type { GuildMember, TextChannel } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { createDefaultEmbed } from '#lib/utils';
import { userMention } from '@discordjs/builders';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.SelectMenu })
export class MemberJoin extends Listener {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: 'guildMemberRemove'
		});
	}
	public async run(member: GuildMember) {
		const embed = createDefaultEmbed(member)
			.setColor('RED')
			.setAuthor({ name: 'Member Left', iconURL: 'https://cdn.discordapp.com/emojis/875754473023229972.png' })
			.setDescription(`${userMention(member.id)} has left the server :(`)
			.setFooter({ text: `We now have ${member.guild.memberCount} members!` });

		const c = (await member.guild!.channels.fetch('697877224505999410')) as TextChannel;
		await c.send({ embeds: [embed] });
	}
}
