import { Listener, type PieceContext, type ListenerOptions } from '@sapphire/framework';

export class UserEvent extends Listener<'unhandledRejection'> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			emitter: process,
			event: 'uncaughtException'
		});
	}

	public async run(err: Error, _: string) {
		this.container.logger.error(`uncaught exception: ${err}`);
		this.container.logger.error(err.stack);
	}
}
