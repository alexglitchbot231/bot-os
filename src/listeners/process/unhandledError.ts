import { Listener, type PieceContext, type ListenerOptions } from '@sapphire/framework';

export class UserEvent extends Listener<'unhandledRejection'> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			emitter: process,
			event: 'unhandledRejection'
		});
	}

	public async run(reason: Error, _promise: Promise<any>) {
		this.container.logger.error(`Unhandled promise rejection: ${reason}`);
		this.container.logger.error(reason.stack);
	}
}
