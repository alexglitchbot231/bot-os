import { fullCommandName } from '#lib/utils';
import type AgbCommandOptions from '#lib/interfaces/AgbCommandOptions';
import type { ChatInputCommand, ChatInputCommandAcceptedPayload, ListenerOptions, PieceContext } from '@sapphire/framework';
import { Events, Listener, LogLevel } from '@sapphire/framework';
import type { Logger } from '@sapphire/plugin-logger';
import { cyan } from 'colorette';
import dayjs from 'dayjs';
import type { CommandInteraction, Guild, User } from 'discord.js';

export class UserEvent extends Listener<typeof Events.ChatInputCommandAccepted> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: Events.ChatInputCommandAccepted
		});
	}

	public async run({ interaction, command }: ChatInputCommandAcceptedPayload) {
		const { guild, user } = interaction;
		const commandName = await this.command(interaction);
		const author = this.author(user);

		await this.cooldown(interaction, command);

		this.container.logger.info(`[${cyan('slash')}] - command ${commandName} executed by ${author}`);
	}

	public onLoad() {
		this.enabled = (this.container.logger as Logger).level <= LogLevel.Info;
		return super.onLoad();
	}

	private async command(command: CommandInteraction) {
		return cyan(await fullCommandName(command));
	}

	private author(author: User) {
		return `${author.username}[${cyan(author.id)}]`;
	}

	private async cooldown(i: CommandInteraction, command: ChatInputCommand) {
		const { cooldownDelay } = command.options;
		if (!cooldownDelay) return;

		if (cooldownDelay <= 0) return;

		await this.container.prisma.commandCooldown.create({
			data: {
				CommandName: await fullCommandName(i),
				CooldownEnd: dayjs().add(cooldownDelay, 'ms').toDate(),
				CommandId: i.commandId,
				UserId: i.user.id
			}
		});
	}
}
