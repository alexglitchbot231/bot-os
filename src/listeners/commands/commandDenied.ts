import { respondError } from '#lib/utils';
import { ChatInputCommandDeniedPayload, Events, ListenerOptions, PieceContext } from '@sapphire/framework';
import { Listener, UserError } from '@sapphire/framework';
import { DurationFormatter } from '@sapphire/time-utilities';
import { CommandInteraction, MessageActionRow, MessageButton } from 'discord.js';

export class UserEvent extends Listener<typeof Events.ChatInputCommandDenied> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: Events.ChatInputCommandDenied
		});
	}

	public async run({ context, identifier, message }: UserError, { interaction }: ChatInputCommandDeniedPayload) {
		// `context: { silent: true }` should make UserError silent:
		// Use cases for this are for example permissions error when running the `eval` command.
		// if (Reflect.get(Object(context), 'silent')) return;

		this.container.logger.debug(``);

		const ctx = Object(context);

		switch (identifier) {
			case 'agbCooldown':
				await this._cooldown(ctx.end, interaction);
				break;
			case 'blacklisted':
				await this._blacklist(ctx.BlackistReason, interaction);
				break;
			case 'preconditionOwnerOnly':
				await respondError({ interaction, error: new Error(`You need to be the bot owner to use this command.`) });
				break;
			case 'lavalinkDisconnected':
				await respondError({ interaction, error: new Error(`LavaLink is disconnected.`) });
				break;
			case 'preconditionNoFlag':
				await respondError({ interaction, error: new Error(`You need the following flags to use this command: ${ctx.flags}`) });
				break;
			case 'notEnoughMoney':
				await respondError({ interaction, error: new Error(`You need $${ctx.cost} for this command `) });
				break;
			default:
				await respondError({ interaction, error: new Error(message) });
		}
	}

	private async _cooldown(remaining: number, i: CommandInteraction) {
		await respondError({ interaction: i, error: new Error(`You can use this command again in ${new DurationFormatter().format(remaining)}`) });
	}

	private async _blacklist(reason: string, i: CommandInteraction) {
		await respondError({
			interaction: i,
			error: new Error(`You have been blacklisted from the bot.`),
			footer: { text: `Reason: ${reason}` },
			opts: {
				components: [
					new MessageActionRow().addComponents(
						new MessageButton().setLabel('Appeal').setStyle('LINK').setURL('https://airtable.com/shr7mSqE7hghSUfgc')
					)
				]
			}
		});
	}
}
