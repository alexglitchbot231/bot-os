import { fullCommandName } from '#lib/utils';
import type {
	ChatInputCommand,
	ChatInputCommandAcceptedPayload,
	ContextMenuCommandAcceptedPayload,
	ListenerOptions,
	MessageCommandAcceptedPayload,
	PieceContext
} from '@sapphire/framework';
import { Events, Listener, LogLevel } from '@sapphire/framework';
import type { Logger } from '@sapphire/plugin-logger';
import { cyan } from 'colorette';
import dayjs from 'dayjs';
import type { CommandInteraction, Guild, User } from 'discord.js';

export class UserEvent extends Listener<typeof Events.ContextMenuCommandAccepted> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: Events.ContextMenuCommandAccepted
		});
	}

	public async run({ interaction, command }: ContextMenuCommandAcceptedPayload) {
		const { user, targetType } = interaction;
		const commandName = cyan(command.name);
		const author = this.author(user);

		this.container.logger.info(`[${cyan(targetType === 'USER' ? 'user' : 'msg')}] - command ${commandName} executed by ${author}`);
	}

	public onLoad() {
		this.enabled = (this.container.logger as Logger).level <= LogLevel.Info;
		return super.onLoad();
	}

	private async command(command: CommandInteraction) {
		return cyan(await fullCommandName(command));
	}

	private author(author: User) {
		return `${author.username}[${cyan(author.id)}]`;
	}
}
