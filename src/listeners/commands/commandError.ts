import { createDefaultEmbed } from '#lib/utils';
import { ErrorHandler } from '#lib/ErrorHandler';
import { Listener, Events, type PieceContext, type ListenerOptions, UserError, ChatInputCommandErrorPayload } from '@sapphire/framework';

export class UserEvent extends Listener<typeof Events.ChatInputCommandError> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: Events.ChatInputCommandError
		});
	}

	public async run(err: UserError, { interaction }: ChatInputCommandErrorPayload) {
		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Something went wrong running that command')
			.setDescription(err.message)
			.setColor('RED');

		if (interaction.replied || interaction.deferred) await interaction.editReply({ embeds: [embed] });
		else await interaction.reply({ embeds: [embed], allowedMentions: { users: [interaction.user.id], roles: [] }, ephemeral: true });

		await new ErrorHandler().handleCommandError(err, interaction);
	}
}
