import { ApplyOptions } from '@sapphire/decorators';
import { Events, Listener, ListenerOptions, PieceContext } from '@sapphire/framework';
import { Message, MessageEmbed, TextChannel } from 'discord.js';
import { pickRandom, probably } from '#lib/utils';
import { Duration } from '@sapphire/time-utilities';

@ApplyOptions<Listener.Options>({
	event: Events.MessageCreate
})
export class UserEvent extends Listener<typeof Events.MessageCreate> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: Events.MessageCreate
		});
	}

	public async run(message: Message) {
		const methods = Reflect.ownKeys(Object.getPrototypeOf(this))
			.map((x) => <string>x)
			.filter((x) => !['run', 'constructor'].includes(x));

		methods.forEach((method) => {
			Reflect.get(this, method).call(this, message);
		});
	}

	// @ts-ignore reflection my beloved
	private static async _autoQuote(message: Message) {
		const re =
			/^(?:https:\/\/)?(?:ptb\.|canary\.)?discord(?:app)?\.com\/channels\/(?<guildId>\d{17,19}|@me)\/(?<channelId>\d{17,19})\/(?<messageId>\d{17,19})$/;

		if (!re.test(message.content)) return;

		const { guildId, channelId, messageId } = re.exec(message.content)!.groups!;

		if (guildId !== message.guild?.id) return;

		const channelObj = (await message.guild.channels.fetch(channelId)) as TextChannel;

		if (channelObj.permissionsFor(message.member!).has('VIEW_CHANNEL')) return;

		const msg = await channelObj.messages.fetch(messageId).catch(() => null);

		if (!msg) return;

		const wh = await (message.channel as TextChannel).createWebhook(msg.member?.displayName!, {
			avatar: msg.member?.displayAvatarURL({ dynamic: true })
		});

		await wh.send({
			content: msg.content,
			embeds: msg.embeds
		});

		await wh.delete();
	}

	// @ts-ignore reflection my beloved
	private async _tubeStrike(message: Message) {
		if (message.author.bot) return;

		const res = [/tube('?s)?( on)? strike/i, /train strike/i];

		if (!res.some((re) => re.test(message.content))) return;

		const lyrics = `Some people might like to get a train to work
        Or drive in in a Beemer or a Merc,
        Some guys like to travel in by bus
        But I can't be bothered with the fuss
        Today I'm gonna take my bike
        'Cause once again the Tube's on strike,
        The greedy bastards want extra pay
        For sitting on their arse all day,
        Even though they earn 30k
        So I'm standing here in the pouring rain,
        Where the fuck's my fucking train?
        London Underground (London Underground)
        They're all lazy fucking useless cunts
        London Underground (London Underground)
        They're all greedy cunts
        I want to shoot them all with a rifle
        All they say is, "Please mind the doors"
        And they learn that on their two-day course,
        This job could be done by a four-year-old,
        They just leave us freezing in the cold.
        What you smell is what you get:
        Burger King and piss and sweat,
        You roast to death in the boiling heat
        With tourists treading on your feet
        Nail bombs on every seat,
        So don't tell me to "Mind the Gap"
        I want my fucking money back!
        London Underground (London Underground)
        They're all lazy fucking useless cunts
        London Underground (London Underground)
        They're all greedy cunts
        I want to shoot them all with a rifle
        The floors are sticky and the seats are damp,
        Every platform has a fucking tramp,
        But the drivers get the day off when
        We're all late for work again
        London Underground (London Underground)
        Wa-wa-wankers, they're all wankers
        London Underground (London Underground)
        Take your Oyster card and shove it up your arsehole.`
			.split('\n')
			.map((x) => x.replace(/,$/, ''));

		await message.channel.send(`${pickRandom(lyrics)} /ly`);
	}

	// @ts-ignore reflection my beloved
	private async _amogus(message: Message) {
		if (![/amogus/i, /among us/i].some((x) => x.test(message.content))) return;
		if (!probably(25)) return;

		await message.channel.send(
			'```ansi\n\u001b[31m                      %%%%%%%%%@\u001b[0m\n\u001b[31m                  %%%%%%%%%%%%%%%%%@\u001b[0m\n\u001b[31m                @%%%%%%%%%%%%%%%%%%%%@\u001b[0m\n\u001b[31m                %%%%%%%%%\u001b[0m\n\u001b[31m               @%%%%%%%      \u001b[0m\u001b[34m+:::::::----+@\u001b[0m\n\u001b[31m               @%%%%%%   \u001b[0m\u001b[34m@=:::::::::\u001b[0m\u001b[34m...\u001b[0m\u001b[34m...\u001b[0m\u001b[34m...\u001b[0m\u001b[34m:\u001b[0m\n\u001b[31m              @@%%%%%    \u001b[0m\u001b[34m==::::::::::::::\u001b[0m\u001b[34m...\u001b[0m\u001b[34m.:%\u001b[0m\n\u001b[31m              @@%%%%%   \u001b[0m\u001b[34m@===::::::::::::::::::=\u001b[0m\n\u001b[31m     %%%%%    @@%%%%%@   \u001b[0m\u001b[34m======+::::::::::::*==\u001b[0m\n\u001b[31m    %%%%%%    @@%%%%%%   \u001b[0m\u001b[34m%====================\u001b[0m\n\u001b[31m    @@@@@@   @@@%%%%%%     \u001b[0m\u001b[34m%===============@\u001b[0m\n\u001b[31m    @@@@@@   @@@%%%%%%%%\u001b[0m\n\u001b[31m    @@@@@@   @@@%%%%%%%%%%%@              %%\u001b[0m\n\u001b[31m    @@@@@@   @@@%%%%%%%%%%%%%%%%%%%%%%%%%%%%\u001b[0m\n\u001b[31m    @@@@@@   @@@@%%%%%%%%%%%%%%%%%%%%%%%%%%%\u001b[0m\n\u001b[31m    @@@@@@   @@@@%%%%%%%%%%%%%%%%%%%%%%%%%%%\u001b[0m\n\u001b[31m    @@@@@@    @@@@%%%%%%%%%%%%%%%%%%%%%%%%%@\u001b[0m\n\u001b[31m    @@@@@@    @@@@@%%%%%%%%%%%%%%%%%%%%%%%@@\u001b[0m\n\u001b[31m    @@@@@@    @@@@@@@%%%%%%%%%%%%%%%%%%%%@@@\u001b[0m\n\u001b[31m    @@@@@@    @@@@@@@@@@%%%%%%%%%%%%%%@@@@@@\u001b[0m\n\u001b[31m     @@@@@    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@\u001b[0m\n\u001b[31m      @@@@    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@\u001b[0m\n\u001b[31m              @@@@@@@@@@@              @@@@\u001b[0m\n\u001b[31m              @@@@@@@@@@            @@@@@@\u001b[0m\n\u001b[31m              @@@@@@@@@@         @@@@@@@@@\u001b[0m\n\u001b[31m              @@@@@@@@@@         @@@@@@@@@\u001b[0m\n\u001b[31m              @@@@@@@@@@          @@@@@@@\u001b[0m\n\u001b[31m               @@@@@@@@@\u001b[0m\n\u001b[31m               @@@@@@@\u001b[0m\n```'
		);
	}

	// @ts-ignore reflection my beloved
	private async _challenge(message: Message) {
		if (this.container.econConfig.challenges.cooldown_end !== null) return;
		if (message.guild?.id !== '690282410675404854') return;
		if (message.channel.id !== '690282411795152946') return; // TODO: add this back in

		const { enabled, chance } = this.container.econConfig.challenges;

		if (!enabled) return;

		if (!probably(chance)) return;

		if (message.author.bot) return;

		await this.container.econManager.createChallenge(message);
		this.container.econConfig.challenges.cooldown_end = new Duration(this.container.econConfig.challenges.cooldown).fromNow.getTime();
	}

	// @ts-ignore reflection my beloved
	private async _antiPhish(message: Message) {
		const re = /[.]*(?:https?:\/\/(www\.)?)?(?<link>[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6})\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)/;

		if (!re.test(message.content)) return;

		const link = message.content.match(re)!.groups!.link.replaceAll(/http[s]?:\/\//g, '');

		if (!this.container.scamDomains.includes(link)) return;

		await message.delete();
		const embed = new MessageEmbed()
			.setColor('RED')
			.setTitle('Phishing Link Detected')
			.setDescription(`A phishing link was detected in your message and has been deleted. Please do not send phishing links in this server.`);

		await message.channel.send({ embeds: [embed] });
	}
}
