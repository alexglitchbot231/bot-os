import { InteractionHandler, InteractionHandlerTypes, Listener, ListenerOptions, PieceContext } from '@sapphire/framework';
import type { GuildMember, TextChannel } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { createDefaultEmbed } from '#lib/utils';
import { channelMention, roleMention, userMention } from '@discordjs/builders';

@ApplyOptions<InteractionHandler.Options>({ interactionHandlerType: InteractionHandlerTypes.SelectMenu })
export class MemberJoin extends Listener {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: 'guildMemberAdd'
		});
	}
	public async run(member: GuildMember) {
		const embed = createDefaultEmbed(member)
			.setColor('GREEN')
			.setAuthor({ name: 'New Member', iconURL: 'https://cdn.discordapp.com/emojis/875754472834469948.png' })
			.setDescription(
				`${userMention(member.id)} has joined the server! Make sure to read the ${channelMention(
					'1066394638937763873'
				)} and get some roles from ${channelMention('973934746994348082')}!`
			)
			.setFooter({ text: `We now have ${member.guild.memberCount} members!` });

		const c = (await member.guild!.channels.fetch('697877224505999410')) as TextChannel;
		await c.send({
			content: `${member} ${roleMention('1040244508954202123')}`,
			embeds: [embed],
			allowedMentions: { users: [member.id], roles: ['1040244508954202123'] }
		});
	}
}
