import { container, InteractionHandler, InteractionHandlerTypes, Listener, ListenerOptions, PieceContext } from '@sapphire/framework';
import type { SelectMenuInteraction, TextChannel } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import type { ChallengeCompletedPayload } from '#lib/interfaces';
import { createDefaultEmbed, loadEconomyConfig, saveEconomyConfig, sleep } from '#lib/utils';
import { Duration, Time } from '@sapphire/time-utilities';

export class UserEvent extends Listener<'challengeCompleted'> {
	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			event: 'challengeCompleted',
			emitter: container.emitter
		});
	}
	public async run({ response, type, user, newPoints, msg }: ChallengeCompletedPayload) {
		const embed = createDefaultEmbed(user!).setTitle('Challenge Completed').addFields(
			{
				name: 'Type',
				value: type!.name,
				inline: true
			},
			{
				name: 'Response',
				value: response!,
				inline: true
			},
			{
				name: 'Challenge Points',
				value: newPoints!.toLocaleString(),
				inline: true
			}
		);

		const c = await container.client.guilds.fetch('690282410675404854');
		const channel = (await c.channels.fetch('1058892032560595054')) as TextChannel;

		await channel.send({ embeds: [embed] });

		await sleep(Time.Second * 5);
		await msg!.delete();
		container.econConfig.challenges.cooldown_end = new Duration(container.econConfig.challenges.cooldown).fromNow.getTime();
		await saveEconomyConfig();
		await loadEconomyConfig(true);
	}
}
