import { loadDisabledModules, loadEconomyConfig } from '#lib/utils';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { PrismaClient } from '@prisma/client';
import { InteractionHandlerTypes, ListenerOptions, PieceContext } from '@sapphire/framework';
import { Listener, Store } from '@sapphire/framework';
import { blue, gray, green, yellow, red } from 'colorette';
import { version } from 'pjson';
import { AntiPhishService } from '#lib/AntiPhishService';
import { readFileSync } from 'fs';
import { join } from 'path';
import { rootDir } from '#lib/constants';
import { fetch, FetchResultTypes } from '@sapphire/fetch';
import { MathChallengeType, ScrambleChallengeType, WordChallengeType } from '#src/challenge-types';

const dev = process.env.NODE_ENV !== 'production';

export class UserEvent extends Listener {
	private readonly style = dev ? yellow : blue;

	public constructor(context: PieceContext, options?: ListenerOptions) {
		super(context, {
			...options,
			once: true
		});
	}

	public async run() {
		try {
			this.container.prisma = new PrismaClient();
			await this.container.prisma.$connect();
			this.container.dbConnected = true;
		} catch (error) {
			this.container.dbConnected = false;
			this.container.logger.warn('Could not connect to the database.', error);
		}
		await loadDisabledModules();

		const phishService = new AntiPhishService();
		await phishService.fetchUrls();
		await phishService.startWS();

		await this.printBanner();
		// this.styleCommands();
		await loadEconomyConfig();

		const words = await fetch('https://haste.badstagram.gay/raw/ofobohozip', FetchResultTypes.Text);

		this.container.allowedWords = words.split('\n');
	}

	/* 	private async printBanner() {
		const success = `${green('+')}`;
		const error = `${magentaBright('-')}`;
		const disabled = red('x');

		const { container } = this;

		try {
			container.prisma = new PrismaClient();
			await container.prisma.$connect();
			container.dbConnected = true;
		} catch (error) {
			container.dbConnected = false;
			container.logger.warn('Could not connect to the database.', error);
		}

		const llc = dev ? magentaBright : white;
		const blc = dev ? magenta : blue;

		const blank = llc('');

		// Offset Pad
		const pad = ' '.repeat(7);
		const connector = ' └── ';

		this.container.logger.info(
			String.raw`
[${success}] Discord
[${container.dbConnected ? success : error}] Database
[${container.scamDomainWsConnected ? success : error}] Scam Domain WebSocket
[${!container.lavalinkEnabled ? disabled : container.musicNode ? success : error}] LavaLink
${blank}${dev ? ` ${pad}${blc('<')}${llc('/')}${blc('>')} ${llc('DEVELOPMENT MODE')}` : ''}
		`.trim()
		);
	} */

	private async printBanner() {
		const commands = this.container.stores.get('commands').size.toLocaleString();
		const listeners = this.container.stores.get('listeners').size.toLocaleString();
		const precontitions = this.container.stores.get('preconditions').size.toLocaleString();
		const interactionHandler = this.container.stores.get('interaction-handlers').size.toLocaleString();
		const button = this.container.stores
			.get('interaction-handlers')
			.filter((c) => c.interactionHandlerType === InteractionHandlerTypes.Button)
			.size.toLocaleString();
		const select = this.container.stores
			.get('interaction-handlers')
			.filter((c) => c.interactionHandlerType === InteractionHandlerTypes.SelectMenu)
			.size.toLocaleString();
		const modal = this.container.stores
			.get('interaction-handlers')
			.filter((c) => c.interactionHandlerType === InteractionHandlerTypes.ModalSubmit)
			.size.toLocaleString();

		const tasks = this.container.stores.get('scheduled-tasks').size.toLocaleString();
		const scamDomain = this.container.scamDomains.length.toLocaleString();

		const raw = readFileSync(join(rootDir, 'art.txt'), 'utf-8');

		const art = raw
			.replace('{{command_count}}', green(commands))
			.replaceAll(/{{green}}/g, '\u001b[0;32m') // ew gross
			.replaceAll(/\|>/g, yellow('|>'))
			.replace('{{listener_count}}', green(listeners))
			.replace('{{precondition_count}}', green(precontitions))
			.replace('{{interaction_handler_count}}', green(interactionHandler))
			.replace('{{button_count}}', green(button))
			.replace('{{select_count}}', green(select))
			.replace('{{modal_count}}', green(modal))
			.replace('{{task_count}}', green(tasks))
			.replace('{{scam_domain_count}}', green(scamDomain))
			.replace('{{disabled_module_count}}', green(this.container.disabledModules.length.toLocaleString()));

		console.log(art);
	}
	private styleSubCommands(name: string, last: boolean) {
		return gray(`${last ? '└─' : '├─'} [${green('+')}] Loaded subcommand ${green(name)}.`);
	}

	private styleCommands() {
		const { client } = this.container;
		const commands = client.stores.get('commands').values();

		this.container.logger.info(red('Loading commands...'));

		for (const { name, options } of commands) {
			const opts = options as AgbSubCommandOptions;
			const { subcommands } = opts;
			const { logger } = this.container;

			logger.info(`[${green('+')}] Loaded ${green(name)}...`);

			if (subcommands) {
				const scs = subcommands?.map((o) => o.name);
				const last = scs.pop()!;
				for (const sc of scs) logger.info(this.styleSubCommands(sc, false));
				logger.info(this.styleSubCommands(last, true));
			}
		}
	}
}
