import { createDefaultEmbed, generateButtonCommandId, safeDM } from '#lib/utils';
import { time, TimestampStyles, userMention } from '@discordjs/builders';
import { ApplyOptions } from '@sapphire/decorators';
import type { PieceContext } from '@sapphire/framework';
import { ScheduledTask, ScheduledTaskOptions } from '@sapphire/plugin-scheduled-tasks';
import { Time } from '@sapphire/time-utilities';
import { MessageActionRow, MessageButton, MessageOptions, TextChannel } from 'discord.js';

@ApplyOptions<ScheduledTaskOptions>({
	name: 'reminder',
	interval: Time.Minute
})
export class ReminderTask extends ScheduledTask {
	public constructor(context: PieceContext, options: ScheduledTaskOptions) {
		super(context, options);
	}

	public async run(_payload: unknown) {
		const { prisma, client, logger } = this.container;

		const reminders = await prisma.reminders.findMany({
			where: {
				Active: true,
				EndAt: {
					lte: new Date()
				}
			}
		});

		for (const reminder of reminders) {
			const { ChannelId, Text, StartedAt, Id, UserId } = reminder;

			await prisma.reminders.update({
				where: {
					Id
				},
				data: { Active: false }
			});

			const user = await client.users.fetch(UserId);
			const embed = createDefaultEmbed(user)
				.setTitle('Reminder')
				.setColor('GREEN')
				.setDescription(`Hey ${user.username}, ${time(StartedAt!, TimestampStyles.RelativeTime)} ago, you wanted to be reminded ${Text}`);

			const opts: MessageOptions = {
				embeds: [embed],
				components: [
					new MessageActionRow().setComponents([
						new MessageButton()
							.setCustomId(generateButtonCommandId('reminder-snooze', Id.toString()))
							.setLabel('Snooze')
							.setStyle('PRIMARY')
					])
				]
			};
			const msg = await safeDM(user, opts);

			if (!msg.success) {
				logger.info(`Unable to DM ${user.tag} (${user.id})... Failing back to channel`);
				const c = (await client.channels.fetch(ChannelId)) as TextChannel;
				c.send({ content: userMention(UserId), ...opts });
			}

			logger.info(`Reminder ${Text} sent to ${user.tag} (${user.id})`);
		}
	}
}

declare module '@sapphire/framework' {
	interface ScheduledTasks {
		reminder: never;
	}
}
