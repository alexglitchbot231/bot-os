import { ApplyOptions } from '@sapphire/decorators';
import type { PieceContext } from '@sapphire/framework';
import { ScheduledTask, ScheduledTaskOptions } from '@sapphire/plugin-scheduled-tasks';
import { Time } from '@sapphire/time-utilities';
import type { TextChannel } from 'discord.js';

@ApplyOptions<ScheduledTaskOptions>({
	name: 'birthdaytask',
	interval: Time.Minute
})
export class BirthdayTaskTask extends ScheduledTask {
	public constructor(context: PieceContext, options: ScheduledTaskOptions) {
		super(context, options);
	}

	public async run(payload: unknown) {
		const bDays = await this.container.prisma.users.findMany({
			where: {
				BirthDate: {
					equals: new Date().getDate()
				},
				AND: {
					BirthMonth: {
						equals: new Date().getMonth() + 1
					}
				}
			},
			select: {
				Id: true
			}
		});

		for (const { Id } of bDays) {
			const member = await this.container.client.guilds.cache
				.get('690282410675404854')!
				.members.fetch(Id)
				.catch(() => null);

			if (!member) {
				continue;
			}

			const channel = (await this.container.client.channels.fetch('908830611911888927')) as TextChannel;

			await channel.send(`<:_:1054797474553925672> Happy birthday, ${member}!`);

			await member.roles.add('958128613247578112');
		}
	}
}

declare module '@sapphire/framework' {
	interface ScheduledTasks {
		birthdaytask: never;
	}
}
