import { pickRandom } from '#lib/utils';
import { userMention, bold } from '@discordjs/builders';
import { ApplyOptions } from '@sapphire/decorators';
import type { PieceContext } from '@sapphire/framework';
import { type ScheduledTaskOptions, ScheduledTask } from '@sapphire/plugin-scheduled-tasks';
import { Time } from '@sapphire/time-utilities';
import { MessageActionRow, MessageButton, MessageEmbed, TextChannel } from 'discord.js';

@ApplyOptions<ScheduledTaskOptions>({
	name: 'poll',
	interval: Time.Minute
})
export class PollTask extends ScheduledTask {
	public constructor(context: PieceContext, options: ScheduledTaskOptions) {
		super(context, options);
	}

	public async run(_payload: unknown) {
		const { prisma, client } = this.container;

		const giveaways = await prisma.poll.findMany({
			where: {
				EndAt: {
					lte: new Date()
				}
			}
		});

		for (const p of giveaways) {
			const { Question, Upvotes, Downvotes, Id, MessageId, Voters } = p;

			await prisma.giveaways.delete({
				where: {
					Id
				}
			});

			const g = await client.guilds.fetch('690282410675404854');
			const c = (await g.channels.fetch('697518288950788126')) as TextChannel;
			const m = await c.messages.fetch(MessageId!);

			m.edit({
				embeds: [
					new MessageEmbed(m.embeds[0]!)
						.setTitle('Poll Ended')
						.setDescription(m.embeds[0]!.description!.replace(/Ends/i, 'Ended'))
						.setColor('RED')
				],
				components: []
			});

			if (Voters.length == 0) {
				await m.reply({
					allowedMentions: {
						repliedUser: false
					},
					content: `No one voted :(`
				});

				return;
			}

			await m.reply({
				allowedMentions: {
					repliedUser: false
				},
				embeds: [
					new MessageEmbed()
						.setTitle(Question)
						.setDescription(`${bold('Upvotes:')} ${Upvotes}\n${bold('Downvotes:')} ${Downvotes}`)
						.setColor(Upvotes > Downvotes ? 'GREEN' : 'RED')
				]
			});
		}
	}
}

declare module '@sapphire/framework' {
	interface ScheduledTasks {
		poll: never;
	}
}
