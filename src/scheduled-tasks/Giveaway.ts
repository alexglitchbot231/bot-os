import { pickRandom } from '#lib/utils';
import { userMention, bold } from '@discordjs/builders';
import { ApplyOptions } from '@sapphire/decorators';
import type { PieceContext } from '@sapphire/framework';
import { type ScheduledTaskOptions, ScheduledTask } from '@sapphire/plugin-scheduled-tasks';
import { Time } from '@sapphire/time-utilities';
import { MessageActionRow, MessageButton, MessageEmbed, TextChannel } from 'discord.js';

@ApplyOptions<ScheduledTaskOptions>({
	name: 'giveaway',
	interval: Time.Minute
})
export class GiveawayTask extends ScheduledTask {
	public constructor(context: PieceContext, options: ScheduledTaskOptions) {
		super(context, options);
	}

	public async run(_payload: unknown) {
		const { prisma, client } = this.container;

		const giveaways = await prisma.giveaways.findMany({
			where: {
				EndAt: {
					lte: new Date()
				}
			}
		});

		for (const gw of giveaways) {
			const { Prize, WinnerCount, Id, MessageId, EnteredUsers } = gw;

			await prisma.giveaways.delete({
				where: {
					Id
				}
			});

			const g = await client.guilds.fetch('690282410675404854');
			const c = (await g.channels.fetch('705073217555136534')) as TextChannel;
			const m = await c.messages.fetch(MessageId!);

			m.edit({
				embeds: [
					new MessageEmbed(m.embeds[0]!)
						.setTitle('Giveaway Ended')
						.setDescription(m.embeds[0]!.description!.replace(/Ends/i, 'Ended'))
						.setColor('RED')
				],
				components: [
					new MessageActionRow().setComponents([
						new MessageButton().setStyle('SUCCESS').setCustomId('h').setLabel('Enter').setDisabled(true)
					])
				]
			});

			const winners = [];

			if (EnteredUsers.length == 0) {
				await m.reply({
					allowedMentions: {
						repliedUser: false
					},
					content: `No one won the giveaway :(`
				});

				return;
			}

			for (var i = 0; i < WinnerCount; i++) {
				winners.push(pickRandom(EnteredUsers));
			}

			await m.reply({
				allowedMentions: {
					users: winners,
					repliedUser: false
				},
				content: `Congratulations ${winners.map((u) => userMention(u))}! You won the ${bold(Prize)}`
			});
		}
	}
}
