import { loadEconomyConfig, saveEconomyConfig } from '#lib/utils';
import { ApplyOptions } from '@sapphire/decorators';
import type { PieceContext } from '@sapphire/framework';
import { type ScheduledTaskOptions, ScheduledTask } from '@sapphire/plugin-scheduled-tasks';
import { Time } from '@sapphire/time-utilities';

@ApplyOptions<ScheduledTaskOptions>({
	name: 'RemoveChallengeCooldown',
	interval: Time.Minute
})
export class RemoveChallengeCooldownTask extends ScheduledTask {
	public constructor(context: PieceContext, options: ScheduledTaskOptions) {
		super(context, options);
	}

	public async run(_payload: unknown) {
		const { cooldown_end } = this.container.econConfig.challenges;

		if (!cooldown_end) return;

		const now = new Date();
		const cooldown_end_date = new Date(cooldown_end);

		if (now > cooldown_end_date) {
			this.container.econConfig.challenges.cooldown_end = null;

			await saveEconomyConfig();
			await loadEconomyConfig(true);
		}
	}
}
