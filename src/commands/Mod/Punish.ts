import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { createDefaultEmbed } from '#lib/utils';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'mod',
	requireFlags: ['Staff'],
	subcommands: [
		{
			name: 'ban',
			chatInputRun: 'ban'
		},
		{
			name: 'kick',
			chatInputRun: 'kick'
		},
		{
			name: 'mute',
			chatInputRun: 'mute'
		},
		{
			name: 'warn',
			chatInputRun: 'warn'
		}
	]
})
export class PunishCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('punish')
					.setDescription('Punishment commands')
					.addSubcommand((b) =>
						b
							.setName('ban')
							.setDescription('Ban a user')
							.addUserOption((b) => b.setName('user').setDescription('The user to ban').setRequired(true))
							.addStringOption((b) => b.setName('reason').setDescription('The reason for the ban').setRequired(true))
							.addStringOption((b) => b.setName('evidence').setDescription('The evidence for the ban').setRequired(true))
					)
					.addSubcommand((b) =>
						b
							.setName('kick')
							.setDescription('kick a user')
							.addUserOption((b) => b.setName('user').setDescription('The user to kick').setRequired(true))
							.addStringOption((b) => b.setName('reason').setDescription('The reason for the kick').setRequired(true))
							.addStringOption((b) => b.setName('evidence').setDescription('The evidence for the kick').setRequired(true))
					)
					.addSubcommand((b) =>
						b
							.setName('warn')
							.setDescription('warn a user')
							.addUserOption((b) => b.setName('user').setDescription('The user to warn').setRequired(true))
							.addStringOption((b) => b.setName('reason').setDescription('The reason for the warn').setRequired(true))
							.addStringOption((b) => b.setName('evidence').setDescription('The evidence for the warn').setRequired(true))
					)
					.addSubcommand((b) =>
						b
							.setName('mute')
							.setDescription('mute a user')
							.addUserOption((b) => b.setName('user').setDescription('The user to mute').setRequired(true))
							.addIntegerOption((b) => b.setName('duration').setDescription('The duration of the mute in days').setRequired(true))
							.addStringOption((b) => b.setName('reason').setDescription('The reason for the mute').setRequired(true))
							.addStringOption((b) => b.setName('evidence').setDescription('The evidence for the mute').setRequired(true))
					),
			DefaultChatInputOptions
		);
	}

	public async ban(interaction: CommandInteraction): Promise<void> {}

	public async kick(interaction: CommandInteraction): Promise<void> {}

	public async mute(interaction: CommandInteraction): Promise<void> {}

	public async warn(interaction: CommandInteraction): Promise<void> {
		const user = interaction.options.getUser('user', true);
		const reason = interaction.options.getString('reason', true);
		const evidence = interaction.options.getString('evidence', true);

		await this.container.prisma.punishment.create({
			data: {
				Evidence: evidence,
				Reason: reason,
				Type: 'Warn',
				UserId: user.id,
				ModeratorId: interaction.user.id
			}
		});

		const embed = createDefaultEmbed(user).setDescription(`${user} has been warned`).setColor('YELLOW');

		await interaction.reply({ embeds: [embed] });
	}
}
