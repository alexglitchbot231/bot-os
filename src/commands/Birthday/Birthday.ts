import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { Timestamp } from '@sapphire/time-utilities';
import { createDefaultEmbed } from '#lib/utils';
import { time, TimestampStyles } from '@discordjs/builders';
import dayjs from 'dayjs';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'birthday',
	subcommands: [
		{
			name: 'set',
			chatInputRun: 'setBirthday'
		},
		{
			name: 'clear',
			chatInputRun: 'clearBirthday'
		},
		{
			name: 'list',
			chatInputRun: 'listBirthday'
		}
	]
})
export class BirthdayCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('birthday')
					.setDescription('Birthday commands')
					.addSubcommand((c) =>
						c //
							.setName('set')
							.setDescription('Set your birthday')
							.addIntegerOption((o) =>
								o.setName('date').setDescription('Your birth date').setRequired(true).setMinValue(1).setMaxValue(31)
							)
							.addIntegerOption((o) =>
								o.setName('month').setDescription('Your birth date').setRequired(true).addChoices(
									{
										name: 'January',
										value: 1
									},
									{
										name: 'February',
										value: 2
									},
									{
										name: 'March',
										value: 3
									},
									{
										name: 'April',
										value: 4
									},
									{
										name: 'May',
										value: 5
									},
									{
										name: 'June',

										value: 6
									},
									{
										name: 'July',
										value: 7
									},
									{
										name: 'August',
										value: 8
									},
									{
										name: 'September',
										value: 9
									},
									{
										name: 'October',
										value: 10
									},
									{
										name: 'November',
										value: 11
									},
									{
										name: 'December',
										value: 12
									}
								)
							)
					)
					.addSubcommand((c) =>
						c //
							.setName('clear')
							.setDescription('Clear your birthday')
					)
					.addSubcommand((c) =>
						c //
							.setName('list')
							.setDescription('List all birthdays set')
					),

			DefaultChatInputOptions
		);
	}

	public async setBirthday(interaction: CommandInteraction) {
		const date = interaction.options.getInteger('date', true);
		const month = interaction.options.getInteger('month', true);

		await this.container.prisma.users.update({
			where: {
				Id: interaction.user.id
			},
			data: {
				BirthMonth: month,
				BirthDate: date
			}
		});

		const monthMapping = new Map([
			[1, 'January'],
			[2, 'February'],

			[3, 'March'],
			[4, 'April'],
			[5, 'May'],
			[6, 'June'],
			[7, 'July'],
			[8, 'August'],
			[9, 'September'],
			[10, 'October'],
			[11, 'November'],
			[12, 'December']
		]);

		await interaction.reply({
			content: `Your birthday has been set to ${monthMapping.get(month)} ${this.humanizeDate(date.toString())}`,
			ephemeral: true
		});
	}

	public async clearBirthday(interaction: CommandInteraction) {
		await this.container.prisma.users.update({
			where: {
				Id: interaction.user.id
			},
			data: {
				BirthMonth: null,
				BirthDate: null
			}
		});

		return interaction.reply({
			content: 'Your birthday has been cleared',
			ephemeral: true
		});
	}

	public async listBirthday(interaction: CommandInteraction) {
		this.container.logger.debug('Fetching birthdays');
		const users = await this.container.prisma.users.findMany({
			where: {
				AND: {
					BirthMonth: {
						not: null
					},
					BirthDate: {
						not: null
					}
				}
			},
			select: {
				Id: true,
				BirthMonth: true,
				BirthDate: true
			}
		});

		this.container.logger.debug('Fetched birthdays');
		this.container.logger.debug(users);

		const monthMapping = new Map([
			[1, 'January'],
			[2, 'February'],
			[3, 'March'],
			[4, 'April'],
			[5, 'May'],
			[6, 'June'],
			[7, 'July'],
			[8, 'August'],
			[9, 'September'],
			[10, 'October'],
			[11, 'November'],
			[12, 'December']
		]);

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Birthdays')
			.setDescription(
				users
					.map((user) => {
						const date = dayjs().add(user.BirthDate!, 'day').add(user.BirthMonth!, 'month').toDate();

						if (date.getMonth() + 1 == 12 && date.getDate() > 25) {
							date.setFullYear(date.getFullYear() + 1);
						}

						return `${monthMapping.get(user.BirthMonth!)} ${this.humanizeDate(user.BirthDate!.toString())} (${time(
							date,
							TimestampStyles.RelativeTime
						)}): <@${user.Id}>`;
					})
					.join('\n')
			);

		await interaction.reply({
			embeds: [embed]
		});
	}

	private humanizeDate(date: string) {
		if (date == '1') return `${date}st`;
		if (date == '2') return `${date}nd`;
		if (date == '3') return `${date}rd`;
		return `${date}th`;
	}
}
