import { Awaitable, CommandInteraction, MessageActionRow, Modal, TextInputComponent } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';

import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'suggest'
})
export class SuggestCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand((builder) => builder.setName('suggest').setDescription('Suggest something'), DefaultChatInputOptions);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const row1 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent().setCustomId('suggestion').setLabel('What is your suggestion').setStyle('PARAGRAPH').setRequired(true)
		);

		const modal = new Modal()
			.setTitle('Suggestion')
			.setCustomId('agb.suggest')
			// @ts-expect-error TS is high
			.addComponents(row1);

		await interaction.showModal(modal);
	}
}
