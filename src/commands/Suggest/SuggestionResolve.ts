import { Awaitable, CommandInteraction, MessageActionRow, TextChannel } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbSubCommand } from '#lib/structures';
import type { AgbSubCommandOptions } from '#src/lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';
import { respondError } from '#lib/utils';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'suggest',
	requireFlags: ['SuggestionManager'],
	subcommands: [
		{
			name: 'accept',
			chatInputRun: 'accept'
		},
		{
			name: 'deny',
			chatInputRun: 'deny'
		},
		{
			name: 'consider',
			chatInputRun: 'consider'
		}
	]
})
export class SuggestionResolveCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('suggestion_resolve')
					.setDescription('Resolve a suggestion')
					.addSubcommand((subcommand) =>
						subcommand
							.setName('accept')
							.setDescription('Accept a suggestion')
							.addIntegerOption((option) => option.setName('id').setDescription('The ID of the suggestion').setRequired(true))
							.addStringOption((option) =>
								option.setName('reason').setDescription('The reason for accepting the suggestion').setRequired(false)
							)
					)
					.addSubcommand((subcommand) =>
						subcommand
							.setName('deny')
							.setDescription('Deny a suggestion')
							.addIntegerOption((option) => option.setName('id').setDescription('The ID of the suggestion').setRequired(true))
							.addStringOption((option) =>
								option.setName('reason').setDescription('The reason for accepting the suggestion').setRequired(false)
							)
					)
					.addSubcommand((subcommand) =>
						subcommand
							.setName('consider')
							.setDescription('Consider a suggestion')
							.addIntegerOption((option) => option.setName('id').setDescription('The ID of the suggestion').setRequired(true))
							.addStringOption((option) =>
								option.setName('reason').setDescription('The reason for accepting the suggestion').setRequired(false)
							)
					),
			DefaultChatInputOptions
		);
	}

	public async accept(interaction: CommandInteraction) {
		const id = interaction.options.getInteger('id', true);
		const reason = interaction.options.getString('reason', false) ?? 'No reason provided';

		const suggestion = await this.container.prisma.suggestion.findUnique({
			where: {
				Id: id
			}
		});

		if (!suggestion) {
			await respondError({ interaction, error: new Error('That suggestion does not exist') });
			return;
		}

		const c = (await interaction.guild!.channels.fetch('987387468212998204')) as TextChannel;
		const m = await c.messages.fetch(suggestion.MessageID!).catch(() => null);

		if (!m) {
			await respondError({ interaction, error: new Error('That suggestion does not exist') });
			return;
		}

		const oldComponents = m.components[0].components;

		await m.edit({
			embeds: [
				m.embeds[0].setFooter({
					text: `Accepted by ${interaction.user.tag} | ${reason}`,
					iconURL: interaction.user.displayAvatarURL({ dynamic: true })
				})
			],
			components: [new MessageActionRow().setComponents(...oldComponents.map((c) => c.setDisabled(true)))]
		});

		await this.container.prisma.suggestion.update({
			where: {
				Id: id
			},
			data: {
				Status: 'Accepted'
			}
		});

		return interaction.reply({
			content: 'Suggestion accepted',
			ephemeral: true
		});
	}

	public async deny(interaction: CommandInteraction) {
		// TODO: do stuff
		return interaction.reply('Hello world!');
	}

	public async consider(interaction: CommandInteraction) {
		// TODO: do stuff
		return interaction.reply('Hello world!');
	}
}
