import {
	ApplicationCommandOptionChoiceData,
	AutocompleteInteraction,
	Awaitable,
	CommandInteraction,
	GuildMember,
	MessageActionRow,
	MessageButton,
	TextChannel
} from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { ApplyOptions } from '@sapphire/decorators';
import { createDefaultEmbed, respondError, youtubeThumbnail } from '#lib/utils';
import dayjs from 'dayjs';
import { fetch } from '@sapphire/fetch';
import { AgbSubCommand } from '#lib/structures';
import type { AgbSubCommandOptions } from '#lib/interfaces';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'music',
	requireLavaLink: true,
	requireMissingFlags: ['MusicBanned'],
	subcommands: [
		{
			name: 'join',
			chatInputRun: 'join'
		},
		{
			name: 'leave',
			chatInputRun: 'leave'
		},
		{
			name: 'skip',
			chatInputRun: 'skip'
		},
		{
			name: 'now-playing',
			chatInputRun: 'nowPlaying'
		},
		{
			name: 'play',
			chatInputRun: 'play'
		},
		{
			name: 'queue',
			type: 'group',
			entries: [
				{
					name: 'remove',
					chatInputRun: 'queueRemove'
				},
				{
					name: 'shuffle',
					chatInputRun: 'queueShuffle'
				},
				{
					name: 'list',
					chatInputRun: 'queueList'
				}
			]
		}
	]
})
export class MusicCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('music')
					.setDescription('Music commands')
					.addSubcommand((b) => b.setName('join').setDescription('Join a voice channel'))
					.addSubcommand((b) => b.setName('leave').setDescription('Leave a voice channel'))
					.addSubcommand((b) =>
						b
							.setName('play')
							.setDescription('Play a song')
							.addStringOption((b) => b.setName('query').setDescription('The song to play').setRequired(true).setAutocomplete(true))
					)
					.addSubcommand((b) => b.setName('skip').setDescription('Skip the current song'))
					.addSubcommand((b) =>
						b
							.setName('volume')
							.setDescription('Set the volume of the player')
							.addNumberOption((b) =>
								b.setName('volume').setDescription('The volume to set').setRequired(true).setMinValue(1).setMaxValue(1000)
							)
					)
					.addSubcommand((b) => b.setName('now-playing').setDescription('Show the current song'))
					.addSubcommandGroup((b) =>
						b
							.setName('queue')
							.setDescription('Queue commands')
							.addSubcommand((b) => b.setName('clear').setDescription('Clear the queue'))
							.addSubcommand((b) =>
								b
									.setName('remove')
									.setDescription('Remove a song from the queue')
									.addIntegerOption((b) => b.setName('index').setDescription('The index of the song to remove').setRequired(true))
							)
							.addSubcommand((b) => b.setName('shuffle').setDescription('Shuffle the queue'))
							.addSubcommand((b) => b.setName('list').setDescription('List the queue'))
					),
			DefaultChatInputOptions
		);
	}

	// public async chatInputRun(interaction: CommandInteraction) {
	// 	const sc = interaction.options.getSubcommand(true).replaceAll(/-/g, '');
	// 	const g = interaction.options.getSubcommandGroup(false);

	// 	this.container.logger.debug(sc);
	// 	if (!g) Reflect.get(this, sc).call(this, interaction);
	// 	else Reflect.get(this, `${g.replaceAll(/_-/g, '')}_${sc}`).call(this, interaction);
	// }

	public async autocompleteRun(i: AutocompleteInteraction) {
		const { name, value } = i.options.getFocused(true);
		if (name !== 'query') return;
		if (!value) {
			await i.respond([
				{
					name: 'Auto complete from YouTube is here',
					value: '__null__'
				}
			]);
			return;
		}

		const result = (await fetch<any[]>(`https://www.google.com/complete/search?ds=yt&output=firefox&q=${value}`))[1] ?? [
			{
				name: 'No results found',
				value: '__null__'
			}
		];

		if (!result.length) {
			await i.respond([
				{
					name: 'No results found',
					value: '__null__'
				}
			]);
			return;
		}

		const res: ApplicationCommandOptionChoiceData[] = result.map((x: any) => ({
			name: x,
			value: x
		}));

		await i.respond(res);
	}

	// @ts-ignore
	private async join(interaction: CommandInteraction) {
		const node = this.container.musicNode!.getNode();
		if (!node) throw 'LavaLink Unavailable';

		const channel = (interaction.member! as GuildMember).voice.channel;
		if (!channel) {
			await respondError({ interaction, error: new Error('You must be in a voice channel to use this command.') });
			return;
		}

		const player = await node.joinChannel({
			guildId: interaction.guild!.id,
			channelId: channel!.id,
			shardId: 0
		});

		this.container.musicBoundTextChannel = interaction.channel as TextChannel;

		player.on('end', async (reason) => {
			if (reason.reason === 'FINISHED') {
				player.reset(); // fuck me this is a fucking stupid hack
				const track = this.container.musicQueue.dequeue();

				if (!track) return;

				const embed = createDefaultEmbed(track.requester)
					.setDescription(`Now playing: **${track.track.info.title}**`)
					.setFooter({ text: `Length: ${dayjs(track.track.info.length).format('mm:ss')}` });

				await this.container.musicBoundTextChannel!.send({ embeds: [embed] });

				player.playTrack({ track: track!.track.track });
			}
		});

		this.container.musicPlayer = player;

		await interaction.reply(`Joined ${channel}.`);
	}

	// @ts-ignore
	private async leave(interaction: CommandInteraction) {
		const node = this.container.musicNode!.getNode();
		if (!node) {
			await respondError({ interaction, error: new Error('LavaLink Unavailable.') });
			return;
		}

		const current = interaction.guild?.me?.voice.channel;

		if (!current) {
			await respondError({ interaction, error: new Error('I am not in a voice channel.') });
			return;
		}

		node.leaveChannel(interaction.guild?.id!);

		this.container.musicPlayer = undefined;
		this.container.musicQueue.clear();

		interaction.reply(`Left ${current}.`);
	}

	// @ts-ignore
	private async play(interaction: CommandInteraction) {
		const { musicPlayer, musicNode } = this.container;

		if (!musicPlayer) {
			await respondError({ interaction, error: new Error("I'm not connected to any voice channel") });
			return;
		}

		const query = interaction.options.getString('query');
		const res = await musicNode!.getNode()?.rest.resolve(`ytsearch:${query}`);

		if (!res || res.tracks.length === 0) {
			await respondError({ interaction, error: new Error('No tracks found.') });
			return;
		}

		const tracks = res.tracks;
		const { musicQueue } = this.container;
		let type: 'np' | 'queued' = 'np';
		if (musicPlayer.track === null) {
			musicPlayer.playTrack({
				track: tracks[0].track
			});
			type = 'np';
		} else {
			musicQueue.enqueue({
				requester: interaction.user,
				track: tracks[0]
			});
			type = 'queued';
		}

		const embed = createDefaultEmbed(interaction.user).setDescription(`${type === 'np' ? 'Now playing' : 'Queued'} ${tracks[0].info.title}`);

		interaction.reply({ embeds: [embed] });
	}

	// @ts-ignore
	private async nowPlaying(interaction: CommandInteraction) {
		const { musicPlayer } = this.container;
		const id = musicPlayer?.track!;
		const song = await musicPlayer?.node.rest.decode(id);

		if (!song) {
			await respondError({ interaction, error: new Error('No song playing.') });
			return;
		}

		// @ts-ignore
		const { title, length, identifier } = song;
		// this.container.logger.debug(info);
		const embed = createDefaultEmbed(interaction.user)
			.setTitle(title)
			.setThumbnail(youtubeThumbnail(identifier))
			.setDescription(`${dayjs(musicPlayer?.position ?? 0).format('mm:ss')} / ${dayjs(length).format('mm:ss')}`);

		await interaction.reply({
			embeds: [embed],
			components: [
				new MessageActionRow().addComponents([
					new MessageButton().setLabel('Open in YouTube').setStyle('LINK').setURL(`https://www.youtube.com/watch?v=${identifier}`)
				])
			]
		});
	}

	// @ts-ignore
	private async skip(interaction: CommandInteraction) {
		const { musicPlayer, musicQueue } = this.container;

		musicPlayer!.playTrack({
			track: musicQueue.dequeue()!.track.track,
			options: { noReplace: false }
		});

		interaction.reply('Skipped.');
	}

	// @ts-ignore
	private async queue_clear(interaction: CommandInteraction) {
		const { musicQueue } = this.container;

		musicQueue.clear();

		interaction.reply('Queue cleared.');
	}

	// @ts-ignore
	private async queueRemove(interaction: CommandInteraction) {
		const { musicQueue } = this.container;

		const tracks = musicQueue.items.map((x, i) => `${(i + 1).toLocaleString()}) ${x.track.info.title} - ${x.requester.tag}`);

		const embed = createDefaultEmbed(interaction.user).setTitle('Queue').setDescription(tracks.join('\n'));

		await interaction.reply({
			embeds: [embed]
		});
	}

	// @ts-ignore
	private async queueShuffle(interaction: CommandInteraction) {
		const { musicQueue } = this.container;

		musicQueue.shuffle();

		interaction.reply('Queue shuffled.');
	}

	// @ts-ignore
	private async queueList(interaction: CommandInteraction) {
		const { musicQueue } = this.container;

		const index = interaction.options.getInteger('index', true) - 1;

		if (index < 0 || index >= musicQueue.items.length) {
			await respondError({ interaction, error: new Error('Invalid index.') });
			return;
		}

		const removed = musicQueue.remove(index);

		interaction.reply(`Removed ${removed.track.info.title} from the queue.`);
	}
}
