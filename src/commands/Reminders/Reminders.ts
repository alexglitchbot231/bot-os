import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder, time, TimestampStyles } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { Duration } from '@sapphire/time-utilities';
import { createDefaultEmbed, respondError } from '#lib/utils';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';
import { Culture } from '@microsoft/recognizers-text-date-time';
import * as DateTimeRecognizers from '@microsoft/recognizers-text-date-time';
import dayjs from 'dayjs';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'reminder',
	subcommands: [
		{
			name: 'add',
			chatInputRun: 'add'
		},
		{
			name: 'list',
			chatInputRun: 'list'
		},
		{
			name: 'remove',
			chatInputRun: 'remove'
		},
		{
			name: 'clear',
			chatInputRun: 'clear'
		}
	]
})
export class RemindersCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('reminders')
					.setDescription('Reminders commands')
					.addSubcommand((b) =>
						b
							.setName('add')
							.setDescription('Add a reminder')
							.addStringOption((b) => b.setName('duration').setRequired(true).setDescription('The duration of the reminder'))
							.addStringOption((b) => b.setName('text').setRequired(true).setDescription('The text of the reminder'))
					)
					.addSubcommand((b) => b.setName('list').setDescription('List all reminders'))
					.addSubcommand((b) =>
						b
							.setName('remove')
							.setDescription('Remove a reminder')
							.addIntegerOption((b) => b.setName('id').setRequired(true).setDescription('The id of the reminder'))
					)
					.addSubcommand((b) => b.setName('clear').setDescription('Clear all reminders')),
			DefaultChatInputOptions
		);
	}

	// public async chatInputRun(interaction: CommandInteraction) {
	// 	const sc = interaction.options.getSubcommand(true).replaceAll(/_-/g, '');
	// 	Reflect.get(this, sc).call(this, interaction);
	// }

	public async add(interaction: CommandInteraction) {
		const durRaw = interaction.options.getString('duration', true);
		const text = interaction.options.getString('text', true);

		const dur = this.parseTime(durRaw);

		if (!dur || !dayjs(dur).isValid()) {
			respondError({ interaction, error: new Error('Invalid duration') });
			return;
		}

		const { prisma } = this.container;

		await prisma.reminders.create({
			data: {
				EndAt: dur,
				Text: text,
				UserId: interaction.user.id,
				ChannelId: interaction.channel!.id,
				StartedAt: new Date()
			}
		});

		const embed = createDefaultEmbed(interaction.user).setDescription(`You have added a reminder to ${text}`);

		await interaction.reply({ embeds: [embed] });
	}

	public async list(interaction: CommandInteraction) {
		const { prisma } = this.container;

		const reminders = await prisma.reminders.findMany({
			where: {
				UserId: interaction.user.id,
				Active: true
			},
			orderBy: {
				EndAt: 'asc'
			}
		});

		if (!reminders.length) {
			const embed = createDefaultEmbed(interaction.user).setDescription('You have no reminders');
			await interaction.reply({ embeds: [embed] });
			return;
		}

		const embed = createDefaultEmbed(interaction.user).setDescription(
			reminders.map((r) => `${r.Id}) ${time(r.EndAt, TimestampStyles.RelativeTime)} - ${r.Text}`).join('\n')
		);

		await interaction.reply({ embeds: [embed] });
	}

	public async remove(interaction: CommandInteraction) {
		const id = interaction.options.getInteger('id', true);

		await interaction.deferReply({ ephemeral: true });
		// const conf = await confirm({
		// 	interaction,
		// 	message: `Are you sure you want to remove reminder ${id}?`,
		// 	firstReply: true,
		// 	ephemeral: true
		// });

		// if (!conf) {
		// 	interaction.editReply({ content: 'Cancelled' });
		// 	return;
		// }

		const { prisma } = this.container;

		const reminder = await prisma.reminders.findFirst({
			where: {
				Id: id,
				UserId: interaction.user.id
			}
		});

		if (!reminder) {
			const embed = createDefaultEmbed(interaction.user).setDescription('You have no reminder with that id');
			await interaction.editReply({ embeds: [embed] });
			return;
		}

		await prisma.reminders.delete({ where: { Id: id } });

		const embed = createDefaultEmbed(interaction.user).setDescription(`You have removed reminder ${id}`);

		await interaction.editReply({ embeds: [embed] });
	}

	public async clear(interaction: CommandInteraction) {
		await interaction.deferReply({ ephemeral: true });
		// const conf = await confirm({
		// 	interaction,
		// 	message: `Are you sure you want to clear all reminders?`,
		// 	firstReply: true,
		// 	ephemeral: true
		// });

		// if (!conf) {
		// 	interaction.editReply({ content: 'Cancelled' });
		// 	return;
		// }

		const { prisma } = this.container;

		const reminders = await prisma.reminders.findMany({
			where: {
				UserId: interaction.user.id
			}
		});

		if (!reminders.length) {
			const embed = createDefaultEmbed(interaction.user).setDescription('You have no reminders');
			await interaction.editReply({ embeds: [embed] });
			return;
		}

		await prisma.reminders.deleteMany({
			where: {
				UserId: interaction.user.id
			}
		});

		const embed = createDefaultEmbed(interaction.user).setDescription(`You have cleared ${reminders.length} reminders`);

		await interaction.editReply({ embeds: [embed] });
	}

	private parseTime(time: string): Date | undefined {
		const model = new DateTimeRecognizers.DateTimeRecognizer(Culture.English).getDateTimeModel();
		const results = model.parse(time);

		const res = results[0]?.resolution?.values[0].value;

		if (res) return new Date(res);
		else {
			const dur = new Duration(time);
			if (dur.offset) return dur.fromNow;
		}

		return undefined;
	}
}
