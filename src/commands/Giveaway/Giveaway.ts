import { Awaitable, CommandInteraction, MessageActionRow, MessageButton, MessageEmbed, TextChannel } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { roleMention, SlashCommandBuilder, time, TimestampStyles } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { Duration } from '@sapphire/time-utilities';
import { generateButtonCommandId, respondError } from '#lib/utils';
import { stripIndents } from 'common-tags';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'giveaway'
})
export class GiveawayCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('giveaway')
					.setDescription('Giveaway commands')
					.addSubcommand((b) =>
						b
							.setName('start')
							.setDescription('Start a giveaway')
							.addStringOption((b) => b.setName('duration').setRequired(true).setDescription('The duration of the giveaway'))
							.addStringOption((b) => b.setName('prize').setRequired(true).setDescription('The prize of the giveaway'))
							.addIntegerOption((b) => b.setName('winner_count').setRequired(false).setDescription('How many winners should there be'))
					),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const sc = interaction.options.getSubcommand(true).replaceAll(/-/g, '');
		const g = interaction.options.getSubcommandGroup(false);
		if (!g) Reflect.get(this, sc).call(this, interaction);
		else Reflect.get(this, `${g.replaceAll(/-/g, '')}_${sc}`).call(this, interaction);
	}

	public async start(interaction: CommandInteraction) {
		const durationRaw = interaction.options.getString('duration', true);
		const prize = interaction.options.getString('prize', true);
		const winnerCount = interaction.options.getInteger('winner_count', false) ?? 1;

		const duration = new Duration(durationRaw);

		if (isNaN(duration.offset)) {
			respondError({ interaction, error: new Error('Invalid duration') });
			return;
		}

		await interaction.deferReply({ ephemeral: true });

		const { prisma } = this.container;

		const res = await prisma.giveaways.create({
			data: {
				EndAt: duration.fromNow,
				Prize: prize,
				HostId: interaction.user.id.toString(),
				ChannelId: interaction.channel!.id,
				WinnerCount: winnerCount,
				EnteredUsers: [] as string[],
				StartedAt: new Date()
			},
			select: {
				Id: true
			}
		});

		const { Id: id } = res;

		const c = (await interaction.guild?.channels.fetch('705073217555136534')) as TextChannel;

		const embed = new MessageEmbed()
			.setColor('RANDOM')
			.setTitle('Giveaway')
			.setFooter({ text: `ID: ${id}` }).setDescription(stripIndents`
			**Prize**: ${prize}
			**Winners**: ${winnerCount.toLocaleString()}
			**Ends**: ${time(duration.fromNow, TimestampStyles.RelativeTime)}
			**Started By**: ${interaction.member}
			`);

		const m = await c.send({
			content: roleMention('1005061142936309810'),
			embeds: [embed],
			components: [
				new MessageActionRow().setComponents([
					new MessageButton().setLabel('Enter').setCustomId(generateButtonCommandId('giveaway', 'enter', id.toString())).setStyle('SUCCESS')
				])
			]
		});

		await prisma.giveaways.update({
			where: {
				Id: res.Id
			},
			data: {
				MessageId: m.id
			}
		});

		interaction.editReply({
			content: 'Giveaway started!'
		});
	}
}
