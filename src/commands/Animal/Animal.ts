import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';
import { fetch } from '@sapphire/fetch';
import type { CatAPI } from '#lib/interfaces/CatApi';
import { createDefaultEmbed } from '#lib/utils';
import type { DogAPI } from '#src/lib/interfaces/DogApi';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'animal',
	subcommands: [
		{
			name: 'cat',
			chatInputRun: 'cat'
		},
		{
			name: 'dog',
			chatInputRun: 'dog'
		}
	]
})
export class AnimalCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('animal')
					.setDescription('Animal commands')
					.addSubcommand((b) => b.setName('cat').setDescription('Get a cat'))
					.addSubcommand((b) => b.setName('dog').setDescription('Get a dog')),
			DefaultChatInputOptions
		);
	}

	public async cat(interaction: CommandInteraction) {
		const data = await fetch<CatAPI[]>('https://api.thecatapi.com/v1/images/search');

		const { url } = data[0];

		const embed = createDefaultEmbed(interaction.user).setTitle('Cat').setImage(url).setFooter({ text: 'Powered by https://thecatapi.com' });

		await interaction.reply({ embeds: [embed] });
	}

	public async dog(interaction: CommandInteraction) {
		const { message } = await fetch<DogAPI>('https://dog.ceo/api/breeds/image/random');

		const embed = createDefaultEmbed(interaction.user).setTitle('Dog').setImage(message).setFooter({ text: 'Powered by https://dog.ceo' });

		await interaction.reply({ embeds: [embed] });
	}
}
