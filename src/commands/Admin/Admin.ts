import { AutocompleteInteraction, Awaitable, CommandInteraction, MessageActionRow, MessageButton, MessageEmbed, TextChannel } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { AGBUserFlagsOptions, AGBUserFlagsType, CommandModuleOptions, confDir, DefaultChatInputOptions, Emojis } from '#lib/constants';
import { ApplyOptions } from '@sapphire/decorators';
import { createDefaultEmbed, pickRandom, respondError } from '#lib/utils';
import { PaginatedMessage } from '@sapphire/discord.js-utilities';
import { codeBlock } from '@sapphire/utilities';
import { inspect } from 'util';
import dayjs from 'dayjs';
import type { Prisma, Users } from '@prisma/client';
import { UserFlagManager } from '#lib/UserFlagManager';
import * as TOML from '@iarna/toml';
import type { BotConfig } from '#lib/interfaces';
import { join } from 'path';
import { readFileSync } from 'fs';
import { AgbSubCommand } from '#lib/structures';
import type { AgbCommandOptions, AgbSubCommandOptions } from '#lib/interfaces';
import { ErrorHandler } from '#lib/ErrorHandler';

@ApplyOptions<AgbSubCommandOptions>({
	requireFlags: ['Developer'],
	module: 'admin',
	subcommands: [
		{
			name: 'user-flags',
			type: 'group',
			entries: [
				{
					name: 'set',
					chatInputRun: 'userflags_set'
				},
				{
					name: 'unset',
					chatInputRun: 'userflags_unset'
				},
				{
					name: 'show',
					chatInputRun: 'userflags_show'
				}
			]
		},
		{
			name: 'sql',
			chatInputRun: 'sql'
		},
		{
			name: 'sync-db',
			chatInputRun: 'syncdb'
		},
		{
			name: 'blacklist',
			chatInputRun: 'blacklist'
		},
		{
			name: 'unblacklist',
			chatInputRun: 'unblacklist'
		},
		{
			name: 'send-onboarding-message',
			chatInputRun: 'sendOnboardingMessage'
		},
		{
			name: 'remove_cooldown',
			chatInputRun: 'removeCooldown'
		},
		{
			name: 'reload-config',
			chatInputRun: 'reloadConfig'
		},
		{
			name: 'handlers',
			chatInputRun: 'handlers'
		},
		{
			name: 'error-test',
			chatInputRun: 'errorTest'
		},
		{
			name: 'spawn_challenge',
			chatInputRun: 'spawnChallenge'
		},
		{
			name: 'module',
			type: 'group',
			entries: [
				{
					name: 'list',
					chatInputRun: 'module_list'
				},
				{
					name: 'disable',
					chatInputRun: 'module_disable'
				},
				{
					name: 'enable',
					chatInputRun: 'module_enable'
				}
			]
		}
	]
})
export class AdminCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('admin')
					.setDescription('Admin commands')
					.addSubcommand((b) =>
						b
							.setName('sql')
							.setDescription('Execute SQL queries')
							.addStringOption((b) => b.setName('query').setRequired(true).setDescription('The query to execute'))
					)
					.addSubcommand((b) =>
						b
							.setName('spawn_challenge')
							.setDescription('Spawn a challenge')
							.addStringOption((b) =>
								b
									.setName('type')
									.setRequired(true)
									.setDescription('The type to spawn')
									.setRequired(false)
									.setChoices(
										...this.container.challengeTypes.map((type) => ({
											name: type.name,
											value: type.name
										}))
									)
							)
					)
					.addSubcommand((b) =>
						b
							.setName('remove_cooldown')
							.setDescription('Remove a cooldown')
							.addUserOption((b) => b.setName('user').setRequired(true).setDescription('The user to remove the cooldown from'))
							.addStringOption((b) =>
								b.setName('command').setRequired(true).setDescription('The command to remove the cooldown for').setAutocomplete(true)
							)
					)
					.addSubcommand((b) => b.setName('sync-db').setDescription('Sync the database'))
					.addSubcommand((b) => b.setName('send-onboarding-message').setDescription('Send the onboarding message'))
					.addSubcommand((b) => b.setName('reload-config').setDescription('Reload the config'))
					.addSubcommand((b) => b.setName('handlers').setDescription('handlers'))
					.addSubcommand((b) => b.setName('error-test').setDescription('Test the error handler'))
					.addSubcommand((b) =>
						b
							.setName('blacklist')
							.setDescription('Blacklist a user from the bot')
							.addUserOption((b) => b.setName('user').setRequired(true).setDescription('The user to blacklist'))
							.addStringOption((b) => b.setName('reason').setRequired(true).setDescription('The reason for blacklisting'))
					)
					.addSubcommand((b) =>
						b
							.setName('un-blacklist')
							.setDescription('Un-blacklist a user from the bot')
							.addUserOption((b) => b.setName('user').setRequired(true).setDescription('The user to un-blacklist'))
					)
					.addSubcommandGroup((b) => {
						return b
							.setName('user-flags')
							.setDescription('User flags commands')
							.addSubcommand((b) =>
								b
									.setName('set')
									.setDescription('Set a user flag')
									.addUserOption((b) => b.setName('user').setRequired(true).setDescription('The user to set the flag for'))
									.addStringOption((b) =>
										b
											.setName('flag')
											.setRequired(true)
											.setDescription('The flag to set')
											.setChoices(...AGBUserFlagsOptions)
									)
							)
							.addSubcommand((b) =>
								b
									.setName('unset')
									.setDescription('Unset a user flag')
									.addUserOption((b) => b.setName('user').setRequired(true).setDescription('The user to set the flag for'))
									.addStringOption((b) =>
										b
											.setName('flag')
											.setRequired(true)
											.setDescription('The flag to set')
											.setChoices(...AGBUserFlagsOptions)
									)
							)
							.addSubcommand((b) =>
								b
									.setName('show')
									.setDescription('Show a users flags')
									.addUserOption((b) => b.setName('user').setRequired(true).setDescription('The user to set the flag for'))
							);
					})
					.addSubcommandGroup((b) => {
						return b
							.setName('module')
							.setDescription('Module commands')
							.addSubcommand((b) => b.setName('list').setDescription('List the modules and their status'))
							.addSubcommand((b) =>
								b
									.setName('disable')
									.setDescription('Disable a module')
									.addStringOption((b) =>
										b
											.setName('module')
											.setRequired(true)
											.setDescription('The module to disable')
											.setChoices(...CommandModuleOptions)
									)
							)
							.addSubcommand((b) =>
								b
									.setName('enable')
									.setDescription('Enable a module')
									.addStringOption((b) =>
										b
											.setName('module')
											.setRequired(true)
											.setDescription('The module to disable')
											.setChoices(...CommandModuleOptions)
									)
							);
					}),
			DefaultChatInputOptions
		);
	}

	public async autocompleteRun(interaction: AutocompleteInteraction) {
		this.container.logger.debug("stacy's mom has got it going on");

		if (interaction.options.getSubcommand() === 'remove_cooldown' && interaction.options.getFocused(true).name === 'command') {
			this.container.logger.debug("She's all i want");

			const commands = this.container.stores.get('commands').filter((c) => (c.options as AgbCommandOptions).cooldownDelay !== undefined);
			await interaction.respond(commands.map((c) => ({ name: c.name, value: c.name })));
		}

		return [{ name: 'test', value: 'test' }];
	}

	public async sql(interaction: CommandInteraction) {
		const query = interaction.options.getString('query', true);
		await interaction.deferReply();
		const now = dayjs();
		const rows = await this.container.prisma.$queryRawUnsafe<any[]>(query);
		const elapsed = dayjs().diff(now).toLocaleString();

		if (rows.length === 0) {
			await interaction.editReply(`Query returned no results.`);
			return;
		}

		const paginatedMsg = new PaginatedMessage();

		rows.forEach((row) => {
			paginatedMsg.addPageEmbed(
				createDefaultEmbed(interaction.user)
					.setDescription(codeBlock('json', inspect(row, { depth: 1 })))
					.setFooter({ text: `⌚ ${elapsed}ms` })
			);
		});

		await paginatedMsg.run(interaction);
	}

	// @ts-ignore
	private async syncdb(interaction: CommandInteraction) {
		await interaction.deferReply({ ephemeral: true });
		const { prisma } = this.container;

		const arr: Prisma.Prisma__UsersClient<Users>[] = [];

		const members = await interaction.guild!.members.fetch();

		members
			.filter((m) => !m.user.bot)
			.forEach((m) => {
				arr.push(
					prisma.users.upsert({
						create: {
							Id: m.id
						},
						update: {},
						where: { Id: m.id }
					})
				);
			});

		try {
			await prisma.$transaction(arr);
			await interaction.editReply({ content: 'Done!' });
		} catch (e) {
			await interaction.editReply({ content: codeBlock('', e) });
		}
	}

	// @ts-ignore
	private async userflags_set(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user', true);
		const flag = interaction.options.getString('flag', true) as AGBUserFlagsType;
		await new UserFlagManager(user).setFlag(flag);

		await interaction.reply({ content: `Set flag ${flag} for ${user.tag}` });
	}

	// @ts-ignore
	private async userflags_unset(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user', true);
		const flag = interaction.options.getString('flag', true) as AGBUserFlagsType;
		await new UserFlagManager(user).unsetFlag(flag);

		await interaction.reply({ content: `Unset flag ${flag} for ${user.tag}` });
	}

	// @ts-ignore
	private async userflags_show(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user', true);
		const flags = await new UserFlagManager(user).getFlags();

		const embed = createDefaultEmbed(user)
			// .setTitle(`Flags for ${user.tag}`)
			.setDescription(flags.map((flag) => `• \`${flag}\``).join('\n'));

		await interaction.reply({ embeds: [embed] });
	}

	// @ts-ignore
	private async blacklist(interaction: CommandInteraction) {
		const { options } = interaction;
		const user = options.getUser('user', true);
		const reason = options.getString('reason', true);
		const fMgr = new UserFlagManager(user);
		await interaction.deferReply({ ephemeral: true });

		if (await fMgr.hasFlag('Blacklisted')) {
			await respondError({ interaction, error: new Error(`${user.tag} is already blacklisted.`) });
			return;
		}

		await this.container.prisma.users.update({
			where: { Id: user.id },
			data: {
				BlackistReason: reason
			}
		});
		await fMgr.setFlag('Blacklisted');
		await interaction.editReply({ content: `Blacklisted ${user.tag}` });
	}

	// @ts-ignore
	private async unblacklist(interaction: CommandInteraction) {
		const { options } = interaction;
		const user = options.getUser('user', true);
		const fMgr = new UserFlagManager(user);
		await interaction.deferReply({ ephemeral: true });

		if (!(await fMgr.hasFlag('Blacklisted'))) {
			await respondError({ interaction, error: new Error(`${user.tag} is not already blacklisted.`) });
			return;
		}

		await this.container.prisma.users.update({
			where: { Id: user.id },
			data: {
				BlackistReason: null
			}
		});
		await fMgr.unsetFlag('Blacklisted');
		await interaction.editReply({ content: `Un-blacklisted ${user.tag}` });
	}

	private async sendOnboardingMessage(interaction: CommandInteraction) {
		const row1 = [
			new MessageButton().setLabel('Introduce Yourself').setCustomId('agb.onboarding:introduce').setStyle('SUCCESS'),
			new MessageButton().setLabel('Send Feedback').setCustomId('agb.onboarding:feedback').setStyle('SUCCESS'),
			new MessageButton().setLabel('Select Roles').setCustomId('agb.roles:main').setStyle('SUCCESS'),
			new MessageButton().setLabel('Role Info').setCustomId('agb.onboarding:roleinfo').setStyle('SUCCESS'),
			new MessageButton().setLabel('Tone Indicators').setCustomId('agb.onboarding:tone').setStyle('SUCCESS')
		];
		const row2 = [new MessageButton().setLabel('Submit Bug Report').setCustomId('agb.onboarding:bug').setStyle('DANGER')];

		const embed = new MessageEmbed()
			.setTitle('Welcome to the server!')
			.setDescription(
				"Our interactive buttons below this message allow you to view our rules, customize your roles, introduce yourself and submit feedback to us! Enjoy your stay, and we're looking forward to seeing you in our chats."
			)
			.setColor('RANDOM');

		await interaction.reply({
			embeds: [embed],
			components: [new MessageActionRow().setComponents(row1), new MessageActionRow().setComponents(row2)]
		});
	}

	private async removeCooldown(interaction: CommandInteraction) {
		const { options } = interaction;
		const { prisma } = this.container;
		const user = options.getUser('user', true);
		const command = options.getString('command', true);

		const cooldown = await prisma.commandCooldown.findFirst({
			where: {
				UserId: user.id,
				CommandName: command
			}
		});

		if (!cooldown) {
			await interaction.reply({ content: `Cooldown for command \`${command}\` does not exist.` });
			return;
		}

		await prisma.commandCooldown.delete({
			where: {
				Id: cooldown.Id
			}
		});
		await interaction.reply({ content: `Removed cooldown for \`${command}\` for ${user.tag}` });
	}

	private async reloadConfig(interaction: CommandInteraction) {
		const raw = readFileSync(join(confDir, 'config.toml'), 'utf-8');
		this.container.botConfig = TOML.parse(raw) as any as BotConfig;

		await interaction.reply({ content: 'Reloaded config.', ephemeral: true });
	}

	private async handlers(interaction: CommandInteraction) {
		const handlers = this.container.client.stores
			.get('interaction-handlers')
			.map((h) => `${h.interactionHandlerType} - ${h.name}`)
			.join('\n');

		await interaction.reply({ content: handlers, ephemeral: true });
	}

	// @ts-ignore
	private async errorTest(interaction: CommandInteraction) {
		const err = new Error('This is a test error');
		await new ErrorHandler().handleCommandError(err, interaction);

		await respondError({ interaction, error: err });
	}

	private async spawnChallenge(interaction: CommandInteraction) {
		const typeName = interaction.options.getString('type', false);
		const { challengeTypes } = this.container;
		const chosenType = typeName ? challengeTypes.find((t) => t.name == typeName)! : pickRandom(challengeTypes);

		await interaction.reply({ content: `Spawning challenge of type \`${chosenType.name}\`...`, ephemeral: true });

		const res = await chosenType.run(interaction.channel as TextChannel);

		if (!res.completed) return;

		const { user, newPoints, response, type, msg } = res;

		this.container.emitter.emit('challengeCompleted', {
			type: type!,
			user: user!,
			response: response!,
			newPoints: newPoints!,
			msg: msg!
		});
	}

	private async module_list(interaction: CommandInteraction) {
		const enabled = Emojis.tick;
		const disabled = Emojis.cross;

		const modules = new Set([
			...this.container.client.stores
				.get('commands')
				.map((c) => c.options as AgbCommandOptions)
				.map((c) => c.module)
				.sort()
		]);

		const commands = [...modules].map((m) => (this.container.disabledModules.includes(m) ? `${disabled} ${m}` : `${enabled} ${m}`));

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Module List')
			.setDescription(commands.join('\n'))
			.setFooter({ text: `${modules.size - this.container.disabledModules.length}/${modules.size} modules enabled` });

		await interaction.reply({ embeds: [embed] });
	}

	private async module_enable(interaction: CommandInteraction) {
		const module = interaction.options.getString('module', true);

		if (!this.container.disabledModules.includes(module)) {
			await interaction.reply({ content: `Module \`${module}\` is already enabled.`, ephemeral: true });
			return;
		}

		this.container.disabledModules = this.container.disabledModules.filter((m) => m !== module);

		await this.container.prisma.botConfig.updateMany({
			data: {
				DisabledModules: this.container.disabledModules
			}
		});

		await interaction.reply({ content: `Enabled module \`${module}\``, ephemeral: true });
	}

	private async module_disable(interaction: CommandInteraction) {
		const module = interaction.options.getString('module', true);

		if (this.container.disabledModules.includes(module)) {
			await interaction.reply({ content: `Module \`${module}\` is already disabled.`, ephemeral: true });
			return;
		}

		this.container.disabledModules.push(module);

		await this.container.prisma.botConfig.updateMany({
			data: {
				DisabledModules: this.container.disabledModules
			}
		});

		await interaction.reply({ content: `Disabled module \`${module}\``, ephemeral: true });
	}
}
