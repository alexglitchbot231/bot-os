import { Awaitable, CommandInteraction, MessageActionRow, MessageSelectMenu, MessageSelectOptionData } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { BuyableFlagsOptions, DefaultChatInputOptions } from '#lib/constants';
import { RequireFlag } from '#lib/decorators';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'shop',
	subcommands: [
		{
			name: 'buy',
			chatInputRun: 'buy'
		},
		{
			name: 'create',
			type: 'group',
			entries: [
				{
					name: 'item',
					chatInputRun: 'create_item'
				},
				{
					name: 'flag',
					chatInputRun: 'create_flag'
				},
				{
					name: 'role',
					chatInputRun: 'create_role'
				},
				{
					name: 'multiplier',
					chatInputRun: 'create_multiplier'
				}
			]
		}
	]
})
export class ShopCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('shop')
					.setDescription('Shop commands')
					.addSubcommand((b) => b.setName('buy').setDescription('Buy something'))
					.addSubcommandGroup((b) =>
						b
							.setName('create')
							.setDescription('Create')
							.addSubcommand((b) =>
								b
									.setName('item')
									.setDescription('Create an item')
									.addStringOption((b) => b.setName('name').setDescription('Name of the item').setRequired(true))
									.addStringOption((b) => b.setName('description').setDescription('Description of the item').setRequired(true))
									.addIntegerOption((b) => b.setName('price').setDescription('Price of the item').setRequired(true))
							)
							.addSubcommand((b) =>
								b
									.setName('role')
									.setDescription('Create a buyable role')
									.addStringOption((b) => b.setName('name').setDescription('Name of the item').setRequired(true))
									.addStringOption((b) => b.setName('description').setDescription('Description of the item').setRequired(true))
									.addIntegerOption((b) => b.setName('price').setDescription('Price of the item').setRequired(true))
									.addRoleOption((b) => b.setName('role').setDescription('Role to buy').setRequired(true))
							)
							.addSubcommand((b) =>
								b
									.setName('flag')
									.setDescription('Create a buyable flag')
									.addStringOption((b) => b.setName('name').setDescription('Name of the item').setRequired(true))
									.addStringOption((b) => b.setName('description').setDescription('Description of the item').setRequired(true))
									.addIntegerOption((b) => b.setName('price').setDescription('Price of the item').setRequired(true))
									.addStringOption((b) =>
										b
											.setName('flag')
											.setDescription('Flag to buy')
											.setRequired(true)
											.setChoices(...BuyableFlagsOptions)
									)
							)
							.addSubcommand((b) =>
								b
									.setName('multiplier')
									.setDescription('Create a multiplier flag')
									.addIntegerOption((b) => b.setName('amount').setDescription('Name of the item').setRequired(true))
									.addIntegerOption((b) =>
										b.setName('hours').setDescription('How many hours the multiplier should last').setRequired(true)
									)
									.addIntegerOption((b) => b.setName('price').setDescription('Price of the item').setRequired(true))
							)
					),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const sc = interaction.options.getSubcommand(true).replaceAll(/_/g, '');
		const g = interaction.options.getSubcommandGroup(false);
		if (!g) Reflect.get(this, sc).call(this, interaction);
		else Reflect.get(this, `${g.replaceAll(/_/g, '')}_${sc}`).call(this, interaction);
	}

	public async buy(interaction: CommandInteraction) {
		const items = await this.container.prisma.shopItem.findMany({});

		const select = new MessageSelectMenu().setCustomId('agb.shopBuy');
		const selectOptions: MessageSelectOptionData[] = [];

		const emojiMapping: Record<string, string> = {
			Flag: '🚩',
			Role: '<:icons_roles:866605189029298197>',
			Item: '',
			Multiplier: '<:icons_x:1035163939790667887>'
		};

		items.forEach((item) => {
			selectOptions.push({
				label: `${item.Name} - ${item.Price}`,
				value: item.Id.toString(),
				description: item.Description,
				emoji: emojiMapping[item.Type]
			});
		});

		select.setOptions(selectOptions);

		await interaction.reply({
			content: 'Select an item to buy',
			components: [new MessageActionRow().setComponents(select)]
		});
	}

	@RequireFlag('EconomyManager')
	public async create_item(interaction: CommandInteraction) {
		const options = interaction.options;
		const name = options.getString('name', true);
		const description = options.getString('description', true);
		const price = options.getInteger('price', true);

		await this.container.prisma.shopItem.create({
			data: {
				Name: name,
				Description: description,
				Price: price
			}
		});

		await interaction.reply(`Created item ${name} with price ${price}`);
	}

	@RequireFlag('EconomyManager')
	public async create_role(interaction: CommandInteraction) {
		const options = interaction.options;
		const name = options.getString('name', true);
		const description = options.getString('description', true);
		const role = options.getRole('role', true);
		const price = options.getInteger('price', true);

		await this.container.prisma.shopItem.create({
			data: {
				Name: name,
				Description: description,
				Price: price,
				Role: role.id,
				Type: 'Role'
			}
		});

		await interaction.reply({
			content: `Created role ${role} with price ${price}`,
			allowedMentions: { parse: [] }
		});
	}

	@RequireFlag('EconomyManager')
	public async create_flag(interaction: CommandInteraction) {
		const options = interaction.options;
		const name = options.getString('name', true);
		const description = options.getString('description', true);
		const flag = options.getString('flag', true);
		const price = options.getInteger('price', true);

		await this.container.prisma.shopItem.create({
			data: {
				Name: name,
				Description: description,
				Price: price,
				Flag: flag,
				Type: 'Flag'
			}
		});

		await interaction.reply({
			content: `Created flag ${flag} with price ${price}`,
			allowedMentions: { parse: [] }
		});
	}

	@RequireFlag('EconomyManager')
	public async create_multiplier(interaction: CommandInteraction) {
		const options = interaction.options;
		const amount = options.getInteger('amount', true);
		const length = options.getInteger('hours', true);
		const price = options.getInteger('price', true);

		await this.container.prisma.shopItem.create({
			data: {
				Name: `${amount}x for ${length}`,
				Description: `Multiplies your earnings by ${amount}x for ${length} hours`,
				Multiplier: amount,
				MultiplierLength: length,
				Price: price,
				Type: 'Multiplier'
			}
		});

		await interaction.reply({
			content: `Created ${amount}x multiplier for ${length} with price ${price}`,
			allowedMentions: { parse: [] },
			ephemeral: true
		});
	}
}
