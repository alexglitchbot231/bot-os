import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { createDefaultEmbed } from '#lib/utils';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'config'
})
export class UserConfigCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('user_config')
					.setDescription('User Config')
					.addSubcommand((b) => b.setName('show').setDescription('Show user config')),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const sc = interaction.options.getSubcommand(true).replaceAll(/_/g, '');
		const g = interaction.options.getSubcommandGroup(false);
		if (!g) Reflect.get(this, sc).call(this, interaction);
		else Reflect.get(this, `${g.replaceAll(/_/g, '')}_${sc}`).call(this, interaction);
	}

	public async show(interaction: CommandInteraction) {
		let config = await this.container.prisma.users.findFirst({
			where: {
				Id: interaction.user.id
			},
			select: {
				HugsEnabled: true,
				Timezone: true,
				Location: true,
				PrivateLocation: true
			}
		});

		if (!config) {
			config = (await this.container.prisma.users.create({
				data: {
					Id: interaction.user.id
				},
				select: {
					HugsEnabled: true,
					Timezone: true,
					Location: true,
					PrivateLocation: true
				}
			}))!;
		}

		const parseBool = (foo: boolean, ifYes = 'Yes', ifNo = 'No') => (foo ? ifYes : ifNo);
		const embed = createDefaultEmbed(interaction.user).setDescription(`
		**Time Zone:** ${config.Timezone || 'Not set'}
		**Hugs Enabled:** ${parseBool(config.HugsEnabled)}
		**Location**: ${parseBool(config.PrivateLocation, '[Private]', config.Location || 'Not set')}
		`);

		await interaction.reply({ embeds: [embed] });
	}
}
