import {
	ApplicationCommandOptionChoiceData,
	AutocompleteInteraction,
	Awaitable,
	CommandInteraction,
	MessageActionRow,
	Modal,
	TextInputComponent
} from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { createDefaultEmbed } from '#lib/utils';
import { CommandCost } from '#lib/decorators';
import { inlineCodeBlock } from '@sapphire/utilities';
import { AgbSubCommand } from '#lib/structures';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'tag',
	subcommands: [
		{
			name: 'create',
			chatInputRun: 'create'
		},
		{
			name: 'show',
			chatInputRun: 'show'
		}
	]
})
export class TagCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('tag')
					.setDescription('Tag commands')
					.addSubcommand((b) => b.setName('create').setDescription('Create a tag'))
					.addSubcommand((b) =>
						b
							.setName('show')
							.setDescription('Show a tag')
							.addStringOption((b) => b.setName('title').setDescription('The tag title').setAutocomplete(true).setRequired(true))
					),
			DefaultChatInputOptions
		);
	}

	@CommandCost(1000)
	// @ts-ignore
	private async create(interaction: CommandInteraction) {
		const row1 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent().setCustomId('title').setLabel('Tag title').setStyle('SHORT').setRequired(true)
		);

		const row2 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent().setCustomId('content').setLabel('Tag content').setStyle('PARAGRAPH').setRequired(true)
		);
		const row3 = new MessageActionRow().addComponents(
			// @ts-expect-error TS is high
			new TextInputComponent().setCustomId('image').setLabel('Image URL').setStyle('SHORT').setRequired(false)
		);

		const modal = new Modal()
			.setTitle('Create a tag')
			.setCustomId('agb.tagCreate')
			// @ts-expect-error TS is high
			.addComponents(row1, row2, row3);

		await interaction.showModal(modal);
	}

	public async autocompleteRun(interaction: AutocompleteInteraction) {
		const opt = interaction.options.getFocused(true);
		if (opt.name !== 'title') return;

		const tags = await this.container.prisma.tags.findMany({
			where: {
				Title: {
					startsWith: <string>opt.value
				}
			}
		});

		const res: ApplicationCommandOptionChoiceData[] = tags.map((tag) => ({
			name: tag.Title,
			value: tag.Title
		}));

		await interaction.respond(res);
	}

	// @ts-ignore
	private async show(interaction: CommandInteraction) {
		const title = interaction.options.getString('title', true);
		const { prisma, client } = this.container;
		const tag = await prisma.tags.findFirst({
			where: {
				Title: title
			},
			select: {
				Content: true,
				Title: true,
				ownerId: true,
				UsageCount: true,
				ImageUrl: true
			}
		});

		if (!tag) {
			await interaction.reply({ content: `No tag found with the title ${inlineCodeBlock(title)}`, ephemeral: true });
			return;
		}

		await prisma.tags.update({
			where: {
				Title: title
			},
			data: {
				UsageCount: { increment: 1 }
			}
		});

		const { Content, Title, ownerId, UsageCount } = tag;

		const ownerObj = await client.users.fetch(ownerId);

		const embed = createDefaultEmbed(ownerObj)
			.setTitle(Title)
			.setDescription(Content)
			.setFooter({ text: `This tag has been used ${UsageCount + 1} times` });

		if (tag.ImageUrl) embed.setImage(tag.ImageUrl);

		await interaction.reply({ embeds: [embed] });
	}
}
