import type { Awaitable, CacheType, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { AgbCommand } from '#lib/structures';
import { createDefaultEmbed } from '#lib/utils';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	disabledReason: 'Economy has been deprecated.',
	module: 'economy'
})
export class BalanceCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand((builder) =>
			builder
				.setName('balance')
				.setDescription('Get your balance')
				.addUserOption((b) => b.setName('user').setDescription('The user').setRequired(false))
		);
	}

	public async chatInputRun(interaction: CommandInteraction<CacheType>) {
		const user = interaction.options.getUser('user', false) ?? interaction.user;

		const dbUser = await this.container.prisma.users.findFirst({
			where: { Id: user.id },
			select: { Money: true }
		});

		const embed = createDefaultEmbed(user).setDescription(
			`${user.id === interaction.id ? 'You' : user.username} ${user.id === interaction.id ? 'have' : 'has'} $${dbUser!.Money.toLocaleString()}`
		);

		await interaction.reply({ embeds: [embed] });
	}
}
