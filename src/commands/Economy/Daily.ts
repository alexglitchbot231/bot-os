import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { ApplyOptions } from '@sapphire/decorators';
import { Time } from '@sapphire/time-utilities';
import { randomInt } from 'crypto';
import { createDefaultEmbed } from '#lib/utils';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#lib/interfaces';

@ApplyOptions<AgbCommandOptions>({
	cooldownDelay: Time.Day,
	requireMissingFlags: ['EconBanned'],
	disabledReason: 'Economy has been deprecated.',
	module: 'economy'
})
export class DailyCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand((builder) => builder.setName('daily').setDescription('Claim your daily reward'), DefaultChatInputOptions);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const {
			ranges: {
				daily: { min, max }
			},
			global_multiplier: multi
		} = this.container.econConfig;
		const pMulti = await this.container.econManager.getMultiplierForUser(interaction.user);
		const reward = randomInt(min, max) * (multi + pMulti);

		const newbal = await this.container.econManager.addBalance(interaction.user, reward, 'Daily Reward');

		const embed = createDefaultEmbed(interaction.user)
			.setDescription(`You claimed your daily reward of $${reward.toLocaleString()}!`)
			.setFooter({ text: `Your new balance is $${newbal.toLocaleString()}` });

		interaction.reply({ embeds: [embed] });
	}
}
