import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder, userMention } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { createDefaultEmbed } from '#lib/utils';
import { ApplyOptions } from '@sapphire/decorators';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#lib/interfaces';

@ApplyOptions<AgbCommandOptions>({
	requireMissingFlags: ['EconBanned'],
	disabledReason: 'Economy has been deprecated.',
	module: 'economy'
})
export class LeaderboardCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('leaderboard')
					.setDescription('Get the guild leaderboard')
					.addStringOption((b) =>
						b.setName('type').setRequired(false).setDescription('The type of leaderboard to get').addChoices(
							{
								name: 'money',
								value: 'money'
							},
							{
								name: 'won_challenges',
								value: 'won_challenges'
							}
						)
					),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const type: 'money' | 'won_challenges' = (interaction.options.getString('type', false) as 'money' | 'won_challenges' | null) ?? 'money';

		const embed = createDefaultEmbed(interaction.user).setTitle(`${type === 'money' ? 'Money' : 'Won Challenges'} Leaderboard`);

		if (type === 'money') {
			const leaderboard = await this.container.econManager.getLeaderboard();
			const leaderboardEntries = leaderboard.members
				.map((entry, index) => {
					return `\`${index + 1}\` **${entry.money.toLocaleString()}** | ${userMention(entry.userId)}`;
				})
				.join('\n');

			embed.setDescription(leaderboardEntries);
		} else {
			const leaderboard = await this.container.econManager.getChallengeLeaderboard();
			const leaderboardEntries = leaderboard.members
				.map((entry, index) => {
					return `\`${index + 1}\` **${entry.wonChallenges.toLocaleString()}** | ${userMention(entry.userId)}`;
				})
				.join('\n');

			embed.setDescription(leaderboardEntries);
		}
		await interaction.reply({ embeds: [embed] });
	}
}
