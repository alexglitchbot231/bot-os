import { Awaitable, CacheType, CommandInteraction, Formatters } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';
import type { AgbSubCommandOptions } from '#src/lib/interfaces';
import { Emojis } from '#lib/constants';
import { createDefaultEmbed, loadEconomyConfig, respondError, saveEconomyConfig } from '#lib/utils';
import { stripIndent } from 'common-tags';
import dayjs from 'dayjs';
import { Duration } from '@sapphire/time-utilities';

@ApplyOptions<AgbSubCommandOptions>({
	requireFlags: ['EconomyManager'],
	module: 'economy',
	disabledReason: 'Economy has been deprecated.',
	subcommands: [
		{
			name: 'view',
			chatInputRun: 'view'
		},
		{
			name: 'global_multiplier',
			chatInputRun: 'globalMultiplier'
		},
		{
			name: 'challenges',
			type: 'group',
			entries: [
				{
					name: 'disable',
					chatInputRun: 'challengeDisable'
				},
				{
					name: 'enable',
					chatInputRun: 'challengeEnable'
				},
				{
					name: 'chance',
					chatInputRun: 'challengeChance'
				},
				{
					name: 'cooldown',
					chatInputRun: 'challengeCooldown'
				}
			]
		},
		{
			name: 'range',
			type: 'group',
			entries: [
				{
					name: 'daily',
					chatInputRun: 'rangeDaily'
				},
				{
					name: 'hourly',
					chatInputRun: 'rangeHourly'
				},
				{
					name: 'work',
					chatInputRun: 'rangeWork'
				}
			]
		}
	]
})
export class EconConfigCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand((builder) =>
			builder
				.setName('econ_config')
				.setDescription('Configure the economy')
				.addSubcommand((b) => b.setName('view').setDescription('View the current configuration'))
				.addSubcommand((b) =>
					b
						.setName('global_multiplier')
						.setDescription('Set the global multiplier')
						.addIntegerOption((b) => b.setName('multiplier').setDescription('The multiplier').setRequired(true).setMinValue(1))
				)
				.addSubcommandGroup((b) =>
					b
						.setName('challenges')
						.setDescription('Configure challenges')
						.addSubcommand((b) => b.setName('disable').setDescription('Disable challenges'))
						.addSubcommand((b) => b.setName('enable').setDescription('Enable challenges'))
						.addSubcommand((b) =>
							b
								.setName('cooldown')
								.setDescription('Change the cooldown of challenges')
								.addStringOption((b) => b.setName('cooldown').setDescription('The cooldown').setRequired(true))
						)
						.addSubcommand((b) =>
							b
								.setName('chance')
								.setDescription('Set the chance of a challenge occuring')
								.addIntegerOption((b) =>
									b
										.setName('chance')
										.setDescription('The percentage of a challenge occuring')
										.setRequired(true)
										.setMinValue(0)
										.setMaxValue(100)
								)
						)
				)
				.addSubcommandGroup((b) =>
					b
						.setName('range')
						.setDescription('Configure the range of rewards')
						.addSubcommand((b) =>
							b
								.setName('daily')
								.setDescription('Configure the range of the daily reward')
								.addIntegerOption((b) =>
									b.setName('min').setDescription('The minimum amount of money').setRequired(true).setMinValue(0)
								)
								.addIntegerOption((b) =>
									b.setName('max').setDescription('The maximum amount of money').setRequired(true).setMinValue(0)
								)
						)
						.addSubcommand((b) =>
							b
								.setName('hourly')
								.setDescription('Configure the range of the hourly reward')
								.addIntegerOption((b) =>
									b.setName('min').setDescription('The minimum amount of money').setRequired(true).setMinValue(0)
								)
								.addIntegerOption((b) =>
									b.setName('max').setDescription('The maximum amount of money').setRequired(true).setMinValue(0)
								)
						)
						.addSubcommand((b) =>
							b
								.setName('work')
								.setDescription('Configure the range of the work reward')
								.addIntegerOption((b) =>
									b.setName('min').setDescription('The minimum amount of money').setRequired(true).setMinValue(0)
								)
								.addIntegerOption((b) =>
									b.setName('max').setDescription('The maximum amount of money').setRequired(true).setMinValue(0)
								)
						)
				)
		);
	}

	public async view(interaction: CommandInteraction<CacheType>) {
		const { challenges, ranges, global_multiplier } = this.container.econConfig;

		const { connector, endConnector, blank } = Emojis;

		let challengeValue = `
		${connector} Enabled: ${challenges.enabled ? Emojis.tick : Emojis.cross}
		${connector} Cooldown: ${challenges.cooldown}
		${connector} On Cooldown: ${challenges.cooldown_end != null ? Emojis.tick : Emojis.cross}
	`;

		if (challenges.cooldown_end != null) {
			// const end = dayjs(challenges.cooldown_end).format('DD/MM/YYYY HH:mm:ss');
			challengeValue += `${blank} ${connector} Cooldown ends: ${Formatters.time(Math.round(challenges.cooldown_end / 1000), 'R')}\n`;
		}

		challengeValue += `${endConnector} Chance: ${challenges.chance}%`;

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Economy Configuration')
			.addField('Global Multiplier', global_multiplier.toLocaleString())
			.addField('Challenges', stripIndent(challengeValue))
			.addField(
				'Ranges',
				stripIndent`
                    ${connector} Daily
                    	${blank} ${connector} Max: $${ranges.daily.max}
                    	${blank} ${endConnector} Min: $${ranges.daily.min}
                    ${connector} Hourly
                    	${blank} ${connector} Max: $${ranges.hourly.max}
                    	${blank} ${endConnector} Min: $${ranges.hourly.min}
                    ${endConnector} Work
                    	${blank} ${connector} Max: $${ranges.work.max}
                    	${blank} ${endConnector} Min: $${ranges.work.min}
                `
			);

		await interaction.reply({ embeds: [embed] });
	}

	public async globalMultiplier(interaction: CommandInteraction<CacheType>) {
		const chance = interaction.options.getInteger('multiplier', true);
		this.container.econConfig.challenges.chance = chance;
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: `The global multiplier is now ${chance}x`, ephemeral: true });
	}

	public async challengeDisable(interaction: CommandInteraction<CacheType>) {
		this.container.econConfig.challenges.enabled = false;
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: 'Disabled challenges', ephemeral: true });
	}

	public async challengeEnable(interaction: CommandInteraction<CacheType>) {
		this.container.econConfig.challenges.enabled = true;
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: 'Enabled challenges', ephemeral: true });
	}

	public async challengeCooldown(interaction: CommandInteraction<CacheType>) {
		const cooldown = interaction.options.getString('cooldown', true);

		const dur = new Duration(cooldown).offset;

		if (isNaN(dur)) {
			await respondError({ interaction, error: new Error('Invalid duration') });
			return;
		}

		this.container.econConfig.challenges.cooldown = cooldown;
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: `The challenge cooldown is now ${cooldown}`, ephemeral: true });
	}

	public async challengeChance(interaction: CommandInteraction<CacheType>) {
		const chance = interaction.options.getInteger('chance', true);
		this.container.econConfig.challenges.chance = chance;
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: `The chance of a challenge spawning is now ${chance}%`, ephemeral: true });
	}

	public async rangeDaily(interaction: CommandInteraction<CacheType>) {
		const min = interaction.options.getInteger('min', true);
		const max = interaction.options.getInteger('max', true);

		this.container.econConfig.ranges.daily.min = min;
		this.container.econConfig.ranges.daily.max = max;

		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: `The range of </daily:990442676862218240> is now $${min} - $${max}`, ephemeral: true });
	}

	public async rangeHourly(interaction: CommandInteraction<CacheType>) {
		const min = interaction.options.getInteger('min', true);
		const max = interaction.options.getInteger('max', true);

		this.container.econConfig.ranges.hourly.min = min;
		this.container.econConfig.ranges.hourly.max = max;

		await saveEconomyConfig();
		await loadEconomyConfig(true);
		await interaction.reply({ content: `The range of </hourly:990442681475923968> is now $${min} - $${max}`, ephemeral: true });
	}

	public async rangeWork(interaction: CommandInteraction<CacheType>) {
		const min = interaction.options.getInteger('min', true);
		const max = interaction.options.getInteger('max', true);

		this.container.econConfig.ranges.work.min = min;
		this.container.econConfig.ranges.work.max = max;

		await saveEconomyConfig();
		await loadEconomyConfig(true);
		await interaction.reply({ content: `The range of </work:990442683954782228> is now $${min} - $${max}`, ephemeral: true });
	}
}
