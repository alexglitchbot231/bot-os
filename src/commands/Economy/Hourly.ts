import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { createDefaultEmbed } from '#lib/utils';
import { randomInt } from 'crypto';
import { ApplyOptions } from '@sapphire/decorators';
import { Time } from '@sapphire/time-utilities';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#lib/interfaces';

@ApplyOptions<AgbCommandOptions>({
	cooldownDelay: Time.Hour,
	requireMissingFlags: ['EconBanned'],
	disabledReason: 'Economy has been deprecated.',
	module: 'economy'
})
export class HourlyCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand((builder) => builder.setName('hourly').setDescription('Claim your hourly reward'), DefaultChatInputOptions);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const {
			ranges: {
				daily: { min, max }
			},
			global_multiplier: multi
		} = this.container.econConfig;

		const pMulti = await this.container.econManager.getMultiplierForUser(interaction.user);
		const reward = randomInt(min, max) * (multi + pMulti);

		const newBal = await this.container.econManager.addBalance(interaction.user, reward, 'Hourly Reward');

		const embed = createDefaultEmbed(interaction.user)
			.setDescription(`You claimed your hourly reward of $${reward.toLocaleString()}!`)
			.setFooter({ text: `Your new balance is $${newBal.toLocaleString()}` });

		interaction.reply({ embeds: [embed] });
	}
}
