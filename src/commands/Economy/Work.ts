import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions, Jobs } from '#lib/constants';
import { createDefaultEmbed, pickRandom } from '#lib/utils';
import { randomInt } from 'crypto';
import { ApplyOptions } from '@sapphire/decorators';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#lib/interfaces';

@ApplyOptions<AgbCommandOptions>({
	requireMissingFlags: ['EconBanned'],
	disabledReason: 'Economy has been deprecated.',
	module: 'economy'
})
export class WorkCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand((builder) => builder.setName('work').setDescription('Go to work for money'), DefaultChatInputOptions);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const {
			ranges: {
				daily: { min, max }
			},
			global_multiplier: multi
		} = this.container.econConfig;
		const pMulti = await this.container.econManager.getMultiplierForUser(interaction.user);
		const reward = randomInt(min, max) * (multi + pMulti);

		const job = pickRandom(Jobs);

		const newBal = await this.container.econManager.addBalance(interaction.user, reward, `Work (${job})`);

		const embed = createDefaultEmbed(interaction.user)
			.setDescription(`You worked as a **${job}** $${reward.toLocaleString()}!`)
			.setFooter({ text: `Your new balance is $${newBal.toLocaleString()}` });

		interaction.reply({ embeds: [embed] });
	}
}
