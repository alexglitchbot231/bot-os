import { Awaitable, CommandInteraction, MessageActionRow, Modal, TextInputComponent } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';
import { respondError } from '#lib/utils';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'apply',
	subcommands: [
		{
			name: 'irl_friend',
			chatInputRun: 'irlFriend'
		},
		{
			name: 'staff',
			chatInputRun: 'staff'
		}
	]
})
export class ApplyCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('apply')
					.setDescription('Apply for a role')
					.addSubcommand((s) => s.setName('irl_friend').setDescription('Apply for the IRL Friend role'))
					.addSubcommand((s) => s.setName('staff').setDescription('Apply for staff')),
			DefaultChatInputOptions
		);
	}

	public async irlFriend(interaction: CommandInteraction) {
		const row1 = new MessageActionRow().addComponents(
			// @ts-expect-error fuck TS
			new TextInputComponent()
				.setCustomId('name')
				.setLabel('What is your IRL name')
				.setPlaceholder("This won't be shared outside your application")
				.setStyle('SHORT')
				.setRequired(true)
		);

		const row2 = new MessageActionRow().addComponents(
			// @ts-expect-error fuck TS
			new TextInputComponent().setCustomId('how').setLabel('How do you know Alex?').setStyle('PARAGRAPH').setRequired(true)
		);

		const modal = new Modal()
			.setTitle('IRL Friend Application')
			.setCustomId('agb.apply:irl_friend')
			// @ts-expect-error fuck TS
			.addComponents(row1, row2);

		await interaction.showModal(modal);
	}

	public async staff(interaction: CommandInteraction) {
		await respondError({ interaction, error: new Error('This command is not yet implemented') });
	}
}
