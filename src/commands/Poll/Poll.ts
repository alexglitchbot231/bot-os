import { Awaitable, CommandInteraction, MessageActionRow, MessageButton, MessageEmbed, TextChannel } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { roleMention, SlashCommandBuilder, time, TimestampStyles } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { generateButtonCommandId, respondError } from '#lib/utils';
import { Duration } from '@sapphire/time-utilities';
import { stripIndents } from 'common-tags';
import { isNullish } from '@sapphire/utilities';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'poll',
	subcommands: [
		{
			name: 'create',
			chatInputRun: 'create'
		}
	]
})
export class PollCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('poll')
					.setDescription('Poll Commands')
					.addSubcommand((b) =>
						b
							.setName('create')
							.setDescription('Create a poll')
							.addStringOption((b) => b.setName('duration').setDescription('The duration of the poll').setRequired(true))
							.addStringOption((b) => b.setName('title').setDescription('The title of the poll').setRequired(true))
					),
			DefaultChatInputOptions
		);
	}

	// public async chatInputRun(interaction: CommandInteraction) {
	// 	const sc = interaction.options.getSubcommand(true).replaceAll(/_/g, '');
	// 	const g = interaction.options.getSubcommandGroup(false);
	// 	if (!g) Reflect.get(this, sc).call(this, interaction);
	// 	else Reflect.get(this, `${g.replaceAll(/_/g, '')}_sc`).call(this, interaction);
	// }

	public async create(interaction: CommandInteraction) {
		const durationRaw = interaction.options.getString('duration', true);
		const title = interaction.options.getString('title', true);

		const duration = new Duration(durationRaw);
		if (isNaN(duration.offset)) {
			respondError({ interaction, error: new Error('Invalid duration') });
			return;
		}

		await interaction.deferReply({ ephemeral: true });

		const { prisma } = this.container;

		const res = await prisma.poll.create({
			data: {
				EndAt: duration.fromNow,
				Question: title,
				CreatedAt: new Date(),
				CreatedBy: interaction.user.id
			},
			select: {
				Id: true
			}
		});

		const { Id: id } = res;

		const c = (await interaction.guild?.channels.fetch('697518288950788126')) as TextChannel;

		const embed = new MessageEmbed()
			.setColor('RANDOM')
			.setTitle('Poll')
			.setFooter({ text: `ID: ${id}` }).setDescription(stripIndents`
			**Question**: ${title}
			**Ends**: ${time(duration.fromNow, TimestampStyles.RelativeTime)}
			**Started By**: ${interaction.member}
			`);

		const m = await c.send({
			content: roleMention('1005958687321493535'),
			embeds: [embed],
			components: [
				new MessageActionRow().setComponents([
					new MessageButton()
						.setEmoji('⬆️')
						.setLabel('0')
						.setStyle('PRIMARY')
						.setCustomId(generateButtonCommandId('poll', 'vote', id.toString(), 'up')),
					new MessageButton()
						.setEmoji('⬇️')
						.setLabel('0')
						.setStyle('DANGER')
						.setCustomId(generateButtonCommandId('poll', 'vote', id.toString(), 'down'))
				])
			]
		});

		await prisma.poll.update({
			where: {
				Id: res.Id
			},
			data: {
				MessageId: m.id
			}
		});

		interaction.editReply({
			content: 'Poll started!'
		});
	}
}
