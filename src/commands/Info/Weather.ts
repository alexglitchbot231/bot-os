import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { getColors, getData, getFile, getIcons, getWeatherName, resolveData } from '#lib/weatherUtils';
import type { CurrentCondition, ResolvedConditions } from '#src/lib/interfaces/Weather';
import { Canvas } from 'canvas-constructor/napi-rs';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class WeatherCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('weather')
					.setDescription('Get the weather in a location')
					.addStringOption((option) => option.setName('location').setDescription('The location to get the weather for').setRequired(true))
					.addBooleanOption((option) => option.setName('hide').setDescription('Hide the location').setRequired(false)),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const location = interaction.options.getString('location', true);
		const hide = interaction.options.getBoolean('hide', false) ?? false;

		const data = await getData(location);

		const [current] = data.current_condition;
		const [nearestArea] = data.nearest_area;
		const resolved = resolveData(current);

		const place = `${nearestArea.region[0].value || nearestArea.areaName[0].value}, ${nearestArea.country[0].value}`;

		const attachment = await this.draw(current.weatherDesc[0].value, place, current, resolved);

		return interaction.reply({ files: [attachment], ephemeral: hide });
	}

	private async draw(desc: string, place: string, conditions: CurrentCondition, resolved: ResolvedConditions): Promise<Buffer> {
		const weatherName = getWeatherName(conditions.weatherCode);
		const { background, text, theme } = getColors(weatherName);
		const [conditionImage, icons] = await Promise.all([getFile(weatherName), getIcons(theme)]);

		const { width, height, cardWidth, cardHeight, margin, columns, rows } = WeatherCommand.resolveCoordinates();

		const imageSize = 128;
		const halfImageSize = imageSize / 2;

		const iconSize = 32;
		const halfIconSize = iconSize / 2;
		const iconMargin = iconSize + 10;

		return (
			new Canvas(width, height)
				.save()
				.setShadowColor('rgba(0,0,0,.7)')
				.setShadowBlur(margin)
				.setColor(background)
				.createRoundedPath(margin, margin, cardWidth, cardHeight, margin / 2)
				.fill()
				.restore()

				// Place
				.setTextFont('24px RobotoRegular')
				.setTextBaseline('middle')
				.setColor(text)
				.printResponsiveText(place, columns[0].left, rows[0].center, columns[2].right - columns[0].left)

				// Weather Icon (todo)
				.setTextFont('20px RobotoLight')
				.printImage(conditionImage, columns[0].center - halfImageSize, rows[2].center - halfImageSize)

				// Temperature
				.printImage(icons.temperature, columns[1].left, rows[2].center - halfIconSize)
				.printText(resolved.temperature, columns[1].left + iconMargin, rows[2].center)

				// Wind
				.save()
				.translate(columns[2].left + halfIconSize, rows[2].center)
				.rotate((Number(conditions.winddirDegree) * Math.PI) / 180 + Math.PI)
				.printImage(icons.pointer, -halfIconSize, -halfIconSize)
				.restore()
				.printText(resolved.windSpeed, columns[2].left + iconMargin, rows[2].center)

				.printImage(icons.precipitation, columns[1].left, rows[3].center - halfIconSize)
				.printText(resolved.precipitation, columns[1].left + iconMargin, rows[3].center)

				// Visibility
				.printImage(icons.visibility, columns[2].left, rows[3].center - halfIconSize)
				.printText(resolved.visibility, columns[2].left + iconMargin, rows[3].center)

				// Weather Name
				.printResponsiveText(desc, columns[1].left, rows[1].center, columns[2].right - columns[1].left)

				.pngAsync()
		);
	}

	private static resolveCoordinates(): Coordinates {
		const width = 540;
		const height = 260;
		const margin = 15;

		const cardWidth = width - margin * 2;
		const cardHeight = height - margin * 2;

		const contentWidth = cardWidth - margin * 2;
		const contentHeight = cardHeight - margin * 2;

		const contentMargin = margin * 2;

		const amountColumns = 3;
		const amountRows = 4;

		const columnWidth = contentWidth / amountColumns;
		const rowHeight = contentHeight / amountRows;

		const columns: Column[] = [];
		for (let x = 0; x < amountColumns; ++x) {
			const left = Math.ceil(x * columnWidth) + contentMargin;
			const center = Math.round((x + 0.5) * columnWidth) + contentMargin;
			const right = Math.floor((x + 1) * columnWidth) + contentMargin;
			columns.push({ left, center, right });
		}

		const rows: Row[] = [];
		for (let y = 0; y < amountRows; ++y) {
			const top = Math.ceil(y * rowHeight) + contentMargin;
			const center = Math.round((y + 0.5) * rowHeight) + contentMargin;
			const bottom = Math.floor((y + 1) * rowHeight) + contentMargin;
			rows.push({ top, center, bottom });
		}

		return { width, height, margin, cardWidth, cardHeight, contentWidth, contentHeight, columns, rows };
	}
}

interface Column {
	left: number;
	center: number;
	right: number;
}

interface Row {
	top: number;
	center: number;
	bottom: number;
}

interface Coordinates {
	width: number;
	height: number;
	margin: number;
	cardWidth: number;
	cardHeight: number;
	contentWidth: number;
	contentHeight: number;
	columns: Column[];
	rows: Row[];
}
