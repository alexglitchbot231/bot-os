import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { execSync } from 'child_process';
import { blue, red, green, cyan } from 'ansi-colors';
import { version, dependencies } from 'pjson';
import { codeBlock } from '@sapphire/utilities';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class StatsCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand((builder) => builder.setName('stats').setDescription('Bot stats'), DefaultChatInputOptions);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const deps = <any>dependencies;
		const parts = [
			`${green('--Versions--')}`,
			`${blue('AlexGlitchBot')} ${cyan('::')} ${red(`v${version} (${this._getCommitId()})`)}`,
			`${blue('Node')} ${cyan('::')} ${red(process.version)}`,
			`${blue('Sapphire')} ${cyan('::')} ${red(deps['@sapphire/framework'])}`,
			`${blue('Discord.js')} ${cyan('::')} ${red(`${require('discord.js').version}`)}`,
			`${blue('Lavalink')} ${cyan('::')} ${red(`${deps.shoukaku.replace('^', '')}`)}`,
			`${green('--Stats--')}`,
			`${blue('Uptime')} ${cyan('::')} ${red(this.container.startTime.fromNow(true))}`,
			`${blue('Memory')} ${cyan('::')} ${red(`${Math.round(process.memoryUsage().heapUsed / 1024 / 1024)}MB`)}`,
			`${blue('CPU')} ${cyan('::')} ${red(`${Math.round(process.cpuUsage().system / 1000 / 1000)}%`)}`,
			`${blue('Scam Domains')} ${cyan('::')} ${red(`${this.container.scamDomains.length.toLocaleString()}`)}`,
			`${green('--Cache--')}`,
			`${blue('Users')} ${cyan('::')} ${red(this.container.client.users.cache.size.toLocaleString())}`
		];

		interaction.reply(codeBlock('ansi', parts.join('\n')));
	}

	private _getCommitId(): string {
		return execSync('git log -n1 --format="%h"', {}).toString().trim();
	}
}
