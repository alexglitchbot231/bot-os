import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { inlineCodeBlock } from '@sapphire/utilities';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import dayjs from 'dayjs';
import { createDefaultEmbed } from '#lib/utils';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class PingCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand((builder) => builder.setName('ping').setDescription('Pong!'), DefaultChatInputOptions);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const now = dayjs();
		await interaction.reply('Pong!');

		const dbNow = dayjs();
		this.container.prisma.$queryRaw`SELECT 1`;

		const embed = createDefaultEmbed(interaction.user).setDescription(
			[
				`${inlineCodeBlock('REST')}: ${dayjs().diff(now, 'ms')}ms`,
				`${inlineCodeBlock('WebSocket')}: ${this.container.client.ws.ping}ms`,
				`${inlineCodeBlock('Database')}: ${dayjs().diff(dbNow, 'ms')}ms`
			].join('\n')
		);

		interaction.editReply({ embeds: [embed], content: null });
	}
}
