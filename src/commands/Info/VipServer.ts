import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#src/lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info',
	requireFlags: ['IrlFriend']
})
export class VipServerCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand(
			(builder) => builder.setName('vip_server').setDescription('Get a link to the vip server'),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		// TODO: do stuff
		return interaction.reply({
			content: 'https://discord.gg/hvQtFV3',
			ephemeral: true
		});
	}
}
