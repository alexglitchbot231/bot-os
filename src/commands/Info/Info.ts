import { Awaitable, CommandInteraction, ContextMenuInteraction, Guild, GuildMember, MessageEmbed, User } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { ContextMenuCommandBuilder, roleMention } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { createDefaultEmbed } from '#lib/utils';
import dayjs from 'dayjs';
import { AgbCommand } from '#lib/structures';
import { ApplicationCommandType } from 'discord-api-types/v9';
import { stripIndents } from 'common-tags';
import { fetch } from '@sapphire/fetch';

import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class InfoCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const ctxBuilder = new ContextMenuCommandBuilder().setName('User Info').setType(ApplicationCommandType.User);

		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('info')
					.setDescription('User Info')
					.addUserOption((b) => b.setName('user').setRequired(false).setDescription('The user to get info on')),
			DefaultChatInputOptions
		);
		registry.registerContextMenuCommand(ctxBuilder, DefaultChatInputOptions);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const user = (interaction.options.getUser('user', false) ?? interaction.member) as User | GuildMember;
		let member: GuildMember | undefined;
		let embed: MessageEmbed;
		if (user instanceof User) {
			member = await this._getMember(interaction.guild!, user);
			if (!member) {
				embed = await this._userInfo(user);
			} else {
				embed = await this._memberInfo(member);
			}
		} else {
			member = user;
			embed = await this._memberInfo(member);
		}

		await interaction.reply({ embeds: [embed!] });
	}

	public async contextMenuRun(interaction: ContextMenuInteraction) {
		const member = await interaction.guild?.members.fetch(interaction.targetId);
		const embed = await this._memberInfo(member!);

		await interaction.reply({ embeds: [embed!] });
	}

	private async _getMember(g: Guild, user: User): Promise<GuildMember | undefined> {
		try {
			return await g.members.fetch(user);
		} catch (e) {
			return undefined;
		}
	}

	private async _userInfo(user: User) {
		const creation = dayjs(user.createdAt);
		const pronouns = await this._getPronouns(user.id);
		return createDefaultEmbed(user)
			.setAuthor({
				name: `${user.tag} ${pronouns !== 'Unspecified' ? `[${pronouns}]` : ''}`,
				iconURL: user.displayAvatarURL({ dynamic: true })
			})
			.addField('User ID', user.id)
			.addField('Creation Date', `${creation.format('DD/MM/YYYY')} (${creation.fromNow()})`)
			.setFooter({ text: pronouns !== 'Unspecified' ? 'Pronouns provided by pronoundb.org' : '' });
	}

	private async _memberInfo(member: GuildMember) {
		const { user } = member;

		const roles = member.roles.cache
			.sort((a, b) => b.comparePositionTo(a))
			.filter((r) => r.id !== member.guild.id)
			.map((r) => roleMention(r.id))
			.join(' - ');
		const embed = await this._userInfo(user);

		const { Money: money, WonChallenges: wonChallenges } = (await this.container.prisma.users.findFirst({
			where: {
				Id: user.id
			}
		}))!;

		const mlb = await this.container.econManager.getLeaderboard();
		const clb = await this.container.econManager.getChallengeLeaderboard();

		const cp = clb.members.filter((x) => x.userId === user.id)[0];
		const mp = mlb.members.filter((x) => x.userId === user.id)[0];

		return embed
			.addField('Join Date', `${dayjs(member.joinedAt).format('DD/MM/YYYY')} (${dayjs(member.joinedAt).fromNow()})`)
			.addField(
				'Economy Stats',
				stripIndents`
			- Money: $${money.toLocaleString()} ${mp?.placement ? `**[#${mp.placement}]**` : ''}
			- Won Challenges: $${wonChallenges.toLocaleString()} ${cp?.placement ? `**[#${cp.placement}]**` : ''}
			`
			)
			.addField('Roles', roles);
	}

	private async _getPronouns(id: string) {
		const url = new URL('https://pronoundb.org/api/v1/lookup');
		url.searchParams.set('platform', 'discord');
		url.searchParams.set('id', id);

		const { pronouns } = await fetch<{ pronouns: string }>(url.toString());

		return {
			unspecified: 'Unspecified',
			hh: 'he/him',
			hi: 'he/it',
			hs: 'he/she',
			ht: 'he/they',
			ih: 'it/him',
			ii: 'it/its',
			is: 'it/she',
			it: 'it/they',
			shh: 'she/he',
			sh: 'she/her',
			si: 'she/it',
			st: 'she/they',
			th: 'they/he',
			ti: 'they/it',
			ts: 'they/she',
			tt: 'they/them',
			any: 'Any pronouns',
			other: 'Other pronouns',
			ask: 'Ask me my pronouns',
			avoid: 'Avoid pronouns, use my name'
		}[pronouns];
	}
}
