import {
	Awaitable,
	CommandInteraction,
	GuildMember,
	InteractionReplyOptions,
	MessageActionRow,
	MessageButton,
	MessageOptions,
	User
} from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { createDefaultEmbed, generateButtonCommandId, userToMember } from '#lib/utils';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class AvatarCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('avatar')
					.setDescription('Get a users avatar')
					.addUserOption((option) => option.setName('user').setDescription('The user to get the avatar of').setRequired(false)),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user', false) ?? interaction.user;
		const member = await userToMember(user, interaction.guild!);
		member ? await this._memberAvatar(interaction, member) : await this._userAvatar(interaction, user);
	}

	private async _memberAvatar(i: CommandInteraction, member: GuildMember) {
		const guildPfp = member.avatarURL({ dynamic: true, size: 2048 });
		const globalPfp =
			member.user.avatarURL({ dynamic: true, size: 2048 }) ??
			`https://cdn.discordapp.com/embed/avatars/${parseInt(member.user.discriminator) % 5}.png`;

		const embed = createDefaultEmbed(member)
			.setTitle(`${member.user.tag}'s ${guildPfp ? 'Server' : 'Global'} avatar`)
			.setImage(guildPfp ?? globalPfp);

		const opts: InteractionReplyOptions = { embeds: [embed] };

		if (guildPfp) {
			opts.components = [
				new MessageActionRow().addComponents([
					new MessageButton()
						.setCustomId(generateButtonCommandId('avatar', 'global', member.id))
						.setLabel('Show Global')
						.setStyle('PRIMARY')
				])
			];
		}

		await i.reply(opts);
	}

	private async _userAvatar(i: CommandInteraction, user: User) {
		const embed = createDefaultEmbed(user)
			.setTitle(`${user.tag}'s avatar`)
			.setImage(user.avatarURL({ dynamic: true, size: 2048 })!);

		await i.reply({ embeds: [embed] });
	}
}
