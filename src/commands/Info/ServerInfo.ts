import { Awaitable, CommandInteraction, MessageEmbed } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder, time, userMention } from '@discordjs/builders';
import { DefaultChatInputOptions, Emojis, zws } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { stripIndent, stripIndents } from 'common-tags';
import { parseGuildFeatures } from '#lib/parsers';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class ServerInfoCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand(
			(builder) => builder.setName('server_info').setDescription('Get information about the server'),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const { guild } = interaction;

		const members = await guild!.members.fetch();
		const botCount = members.filter((m) => m.user.bot).size;
		const humanCount = members.size - botCount;

		const allChannels = await guild!.channels.fetch();
		const categoryCount = allChannels.filter((c) => c.type === 'GUILD_CATEGORY').size;
		const stageCount = allChannels.filter((c) => c.type === 'GUILD_STAGE_VOICE').size;
		const textCount = allChannels.filter((c) => c.type === 'GUILD_TEXT').size;
		const voiceCount = allChannels.filter((c) => c.type === 'GUILD_VOICE').size;

		const emojisCount = await guild!.emojis.fetch().then((c) => c.size);
		const stickerCount = await guild!.stickers.fetch().then((c) => c.size);

		const { connector, endConnector, channels, emojis } = Emojis;
		const { category, stage, text, voice } = channels;
		const { emoji, sticker } = emojis;

		const embed = new MessageEmbed()
			.setColor('RANDOM')
			.setAuthor({ name: guild!.name, iconURL: guild!.iconURL({ dynamic: true }) ?? undefined })
			.addField('ID', guild!.id, true)
			.addField('Owner', userMention(await guild!.fetchOwner().then((o) => o.id)), true)
			.addField('Created', time(guild!.createdAt, 'R'), true)
			.addField(`${guild!.roles.cache.size} Roles`, zws)
			.addField(
				`${members.size} members`,
				stripIndents`
			${connector} ${Emojis.bot} Bots: ${botCount}
			${endConnector} ${Emojis.bot} Humans: ${humanCount}
			`,
				false
			)
			.addField(
				`${allChannels.size} Channels`,
				stripIndents`
			${connector} ${category} Categories: ${categoryCount}
			${connector} ${text} Text Channels: ${textCount}
			${connector} ${voice} Voice Channels: ${voiceCount}
			${endConnector} ${stage} Stage Channels: ${stageCount}
			`
			)
			.addField(
				`${emojisCount + stickerCount} Emojis`,
				stripIndent`
			${connector} ${emoji} Emojis: ${emojisCount}
			${endConnector} ${sticker} Sticker: ${stickerCount}
			`
			)
			.addField(`${guild!.features.length} Features`, parseGuildFeatures(guild!).join('\n'));
		await interaction.reply({ embeds: [embed] });
	}
}
