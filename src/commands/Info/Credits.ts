import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { createDefaultEmbed } from '#lib/utils';

import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class CreditsCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand(
			(builder) => builder.setName('credits').setDescription('Give credits to various resources used by the bot'),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		type hyperlink = `[${string}](${string})`;
		const creds: Record<hyperlink, string> = {
			'[discord.js](https://discord.js.org)': 'The Discord Framework',
			'[Sapphire](https://www.sapphirejs.dev/)': 'The Command Framework',
			'[Icons](https://discord.gg/aPvvhefmt3)': 'Provides the icons used in the bot',
			'[Skyra Wardrobe](https://github.com/skyra-project/wardrobe)': 'Provides the icons in the weather command',
			'[Contabo](https://contabo.com)': 'Hosting Provider'
		};

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Credits')
			.setDescription(
				Object.keys(creds)
					.map((key) => `${key}: ${creds[key as hyperlink]}`)
					.join('\n')
			);

		await interaction.reply({
			embeds: [embed],
			ephemeral: true
		});
	}
}
