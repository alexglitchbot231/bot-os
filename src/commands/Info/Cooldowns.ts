import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { createDefaultEmbed } from '#lib/utils';
import { SlashCommandBuilder } from '@discordjs/builders';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DurationFormatter } from '@sapphire/time-utilities';
import dayjs from 'dayjs';
import type { Awaitable, CommandInteraction } from 'discord.js';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'info'
})
export class CooldownsCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();

		registry.registerChatInputCommand(
			(builder) => builder.setName('cooldowns').setDescription('View your current cooldowns'),
			DefaultChatInputOptions
		);
	}
	public async chatInputRun(interaction: CommandInteraction) {
		const commandsOnCooldown = await this.container.prisma.commandCooldown.findMany({
			where: {
				UserId: interaction.user.id
			}
		});

		const embed = createDefaultEmbed(interaction.user);

		if (commandsOnCooldown.length === 0) {
			embed.setDescription('None of your commands are on cooldown');
			await interaction.reply({
				embeds: [embed],
				ephemeral: true
			});
		}

		embed
			.setTitle('Cooldowns')
			.setDescription(
				commandsOnCooldown
					.map(
						(command) =>
							`</${command.CommandName}:${command.CommandId}>: ${new DurationFormatter().format(
								dayjs(command.CooldownEnd).diff(dayjs())
							)}`
					)
					.join('\n')
			);

		await interaction.reply({
			embeds: [embed]
		});
	}
}
