import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbSubCommand } from '#lib/structures';
import type { AgbSubCommandOptions } from '#src/lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

import { createDefaultEmbed, getJoke } from '#lib/utils';
import { spoiler } from '@discordjs/builders';
import { fetch } from '@sapphire/fetch';
import type { Flags } from '#lib/interfaces/Joke';
import type { DadJoke } from '#lib/interfaces/DadJoke';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'fun',
	subcommands: [
		{
			name: 'normal',
			chatInputRun: 'defaultJoke'
		},
		{
			name: 'dad',
			chatInputRun: 'dadJoke'
		}
	]
})
export class JokeCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('joke')
					.setDescription('Get a random joke')
					.addSubcommand((b) => b.setName('normal').setDescription('Get a random joke'))
					.addSubcommand((b) => b.setName('dad').setDescription('Get a random dad joke')),
			DefaultChatInputOptions
		);
	}

	async defaultJoke(interaction: CommandInteraction) {
		await interaction.deferReply();
		const joke = await getJoke('any');
		const flags = this.parseFlags(joke.flags);

		const embed = createDefaultEmbed(interaction.user) //
			.setTitle(joke.setup)
			.setDescription(spoiler(joke.delivery))
			.setFooter({ text: `Category: ${joke.category} | Flags: ${flags?.join(', ') ?? 'none'}` });

		await interaction.editReply({ embeds: [embed] });
	}

	async dadJoke(interaction: CommandInteraction) {
		await interaction.deferReply();
		const joke = await fetch<DadJoke>('https://icanhazdadjoke.com/', {
			headers: {
				Accept: 'application/json',
				'User-Agent': 'AlexGlitchBot (contact@badstagram.dev)'
			}
		});

		const embed = createDefaultEmbed(interaction.user) //
			.setTitle('Dad Joke')
			.setDescription(spoiler(joke.joke));

		await interaction.editReply({ embeds: [embed] });
	}

	parseFlags(flags: Flags) {
		const f = [];

		if (flags.explicit) f.push('explicit');
		if (flags.nsfw) f.push('nsfw');
		if (flags.political) f.push('political');
		if (flags.racist) f.push('racist');
		if (flags.religious) f.push('religious');
		if (flags.sexist) f.push('sexist');

		return f.length == 0 ? undefined : f;
	}
}
