import { Awaitable, CommandInteraction, MessageActionRow, Modal, TextInputComponent } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { ApplyOptions } from '@sapphire/decorators';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#src/lib/interfaces';

@ApplyOptions<AgbCommandOptions>({
	requireMissingFlags: ['ConfessionBanned'],
	module: 'fun'
})
export class ConfessCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('confess')
					.setDescription('Sumbit an anonymous confession'),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const modal = new Modal()
			.setTitle('Confess')
			.setCustomId('agb.confess')
			.setComponents(
				// @ts-expect-error TS is high
				new MessageActionRow().addComponents(
					// @ts-expect-error TS is high
					new TextInputComponent()
						.setLabel('Confession')
						.setCustomId('confession')
						.setPlaceholder('Confess here')
						.setStyle('PARAGRAPH')
						.setRequired(true)
				)
			);

		await interaction.showModal(modal);
	}
}
