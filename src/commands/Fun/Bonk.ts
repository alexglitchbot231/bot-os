import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#src/lib/interfaces';
import { createDefaultEmbed, pickRandom, randomGif } from '#lib/utils';
import { ApplyOptions } from '@sapphire/decorators';
import { Time } from '@sapphire/time-utilities';

@ApplyOptions<AgbCommandOptions>({
	cooldownDelay: Time.Minute * 5,
	module: 'fun'
})
export class BonkCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('bonk')
					.setDescription('bonk a member')
					.addUserOption((option) => option.setName('member').setDescription('The member to bonk').setRequired(true)),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const member = interaction.options.getMember('member', true);

		const gif = pickRandom(await randomGif('bonk', 10));

		const embed = createDefaultEmbed(interaction.user).setDescription(`${interaction.user} bonked ${member}`).setImage(gif.media_formats.gif.url);

		interaction.reply({ embeds: [embed] });
	}
}
