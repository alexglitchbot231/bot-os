import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder, userMention } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { createDefaultEmbed, pickRandom, randomGif } from '#lib/utils';
import { ApplyOptions } from '@sapphire/decorators';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#src/lib/interfaces';

@ApplyOptions<AgbCommandOptions>({
	preconditions: ['CheckHugsEnabled'],
	module: 'fun'
})
export class HugCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		const builder = new SlashCommandBuilder();
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('hug')
					.setDescription('Hug a user')
					.addUserOption((b) => b.setName('user').setRequired(true).setDescription('The user to hug')),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user', true);

		const gifs = await randomGif('hug', 8);
		const gif = pickRandom(gifs).media_formats.gif.url;

		const embed = createDefaultEmbed(interaction.user)
			.setDescription(`${userMention(interaction.user.id)} gave ${userMention(user.id)} a big hug!`)
			.setImage(gif);

		await interaction.reply({ embeds: [embed] });
	}
}
