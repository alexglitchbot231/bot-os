import type { Awaitable, CommandInteraction, ContextMenuInteraction, MessageContextMenuInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { ApplicationCommandType } from 'discord-api-types/v9';
import type { TextChannel } from 'discord.js';
import type { User } from 'discord.js';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

@ApplyOptions<AgbCommandOptions>({
	module: 'fun'
})
export class MockCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('mock')
					.setDescription('MoCk A UsEr')
					.addUserOption((b) => b.setName('user').setDescription('The user to MoCk').setRequired(true))
					.addStringOption((b) => b.setName('text').setDescription('The text to MoCk the user with').setRequired(true)),
			DefaultChatInputOptions
		);

		registry.registerContextMenuCommand((builder) =>
			builder //
				.setName('MoCk ThIs MeSsAgE')
				.setType(ApplicationCommandType.Message)
		);
	}

	private _mock(text: string): string {
		let mocked = '';
		for (let i = 0; i < text.length; i++) {
			mocked += i % 2 == 0 ? text.charAt(i).toUpperCase() : text.charAt(i);
		}

		return mocked;
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user', true);
		const text = interaction.options.getString('text', true).toLowerCase();

		let mocked = this._mock(text);

		const wh = await (interaction.channel as TextChannel).createWebhook(user.username);

		await wh.send({ content: mocked, avatarURL: user.avatarURL({ dynamic: true }) ?? undefined });

		await interaction.reply({ content: 'MoCkEd', ephemeral: true });

		await wh.delete();
	}

	public async contextMenuRun(interaction: ContextMenuInteraction) {
		if (!interaction.isMessageContextMenu()) {
			await interaction.reply({ content: 'o-o you found a super rare bug', ephemeral: true });
			return;
		}

		const text = (interaction as MessageContextMenuInteraction).targetMessage.content;
		const user = (interaction as MessageContextMenuInteraction).targetMessage.author as User;

		if (text.length == 0) {
			await interaction.reply({ content: 'That message has no content to MoCk', ephemeral: true });
			return;
		}

		let mocked = this._mock(text);

		const wh = await (interaction.channel as TextChannel).createWebhook(user.username);

		await wh.send({ content: mocked, avatarURL: user.avatarURL({ dynamic: true }) ?? undefined });

		await interaction.reply({ content: 'MoCkEd', ephemeral: true });

		await wh.delete();
	}
}
