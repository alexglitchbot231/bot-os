import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import type { AgbCommandOptions } from '#src/lib/interfaces';
import { createDefaultEmbed, respondError } from '#lib/utils';
import { ApplyOptions } from '@sapphire/decorators';
import { PaginatedMessage } from '@sapphire/discord.js-utilities';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { Time } from '@sapphire/time-utilities';
import type { Awaitable, CommandInteraction } from 'discord.js';
import { Configuration, OpenAIApi } from 'openai';
import dayjs from 'dayjs';

@ApplyOptions<AgbCommandOptions>({
	cooldownDelay: Time.Minute * 5,
	module: 'fun'
})
export class DalleECommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('dall-e')
					.setDescription('Generate an image from a prompt')
					.addStringOption((option) => option.setName('prompt').setDescription('The prompt').setRequired(true))
					.addIntegerOption((option) =>
						option.setName('count').setDescription('How many images to generate').setMinValue(1).setMaxValue(10).setRequired(false)
					),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const openai = new OpenAIApi(new Configuration({ apiKey: this.container.botConfig.secrets.openai }));

		const prompt = interaction.options.getString('prompt', true);
		const n = interaction.options.getInteger('count', false) ?? 1;

		await interaction.reply('Generating image... This may take a while.');

		const now = dayjs();
		const response = await openai
			.createImage({
				prompt,
				n,
				size: '1024x1024',
				response_format: 'url',
				user: interaction.user.id
			})
			.catch((e) => {
				throw new Error(`OpenAI returned an error: ${e.response.data.error.message}`);
			});

		const elapsed = dayjs().diff(now, 'seconds').toLocaleString();

		const { data } = response.data;

		if (data.length === 0) {
			await respondError({ interaction, error: new Error('Dall-e returned no results') });
			return;
		}

		if (n === 1) {
			const embed = createDefaultEmbed(interaction.user)
				.setTitle('Dall-E')
				.setDescription(`Prompt: ${prompt}`)
				.setImage(data[0].url!)
				.setFooter({ text: `Took ${elapsed}s` });
			await interaction.editReply({ embeds: [embed] });
			return;
		}

		const pMsg = new PaginatedMessage();

		for (const { url } of data) {
			const embed = createDefaultEmbed(interaction.user)
				.setTitle('Dall-E')
				.setDescription(`Prompt: ${prompt}`)
				.setImage(url!)
				.setFooter({ text: `Took ${elapsed}s` });

			pMsg.addPageEmbed(embed);
		}

		await pMsg.run(interaction);
	}
}
