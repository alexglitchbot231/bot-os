import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { SlashCommandBuilder } from '@discordjs/builders';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { randomInt } from 'crypto';
import type { AgbCommandOptions } from '#src/lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';
import { Time } from '@sapphire/time-utilities';

@ApplyOptions<AgbCommandOptions>({
	cooldownDelay: Time.Minute * 5,
	module: 'fun'
})
export class DiceCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder
					.setName('dice')
					.setDescription('Roll a dice')
					.addIntegerOption((b) => b.setName('sides').setRequired(false).setDescription('The number of sides on the dice'))
					.addIntegerOption((b) => b.setName('count').setRequired(false).setDescription('The number of dice to roll')),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const { options } = interaction;
		const sides = options.getInteger('sides', false) ?? 6;
		const count = options.getInteger('count', false) ?? 1;
		const rolls: number[] = [];

		for (let i = 0; i < count; i++) {
			rolls.push(randomInt(sides));
		}
		await interaction.reply(
			`You rolled ${rolls.map((x) => x.toLocaleString()).join(', ')} ${count > 1 ? `(${rolls.reduce((a, b) => a + b).toLocaleString()})` : ''}`
		);
	}
}
