import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import { AgbCommand } from '#lib/structures';
import { fetch, FetchResultTypes } from '@sapphire/fetch';
import { respondError } from '#lib/utils';
import type { AgbCommandOptions } from '#lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';

const BASE_URL = 'http://api.wolframalpha.com/v1';
const bannedRegexes = [/\bip\b/i, /location/i, /geoip/i, /where am i/i];
@ApplyOptions<AgbCommandOptions>({
	module: 'fun'
})
export class WolframCommand extends AgbCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('wolfram')
					.setDescription('Ask Wolfram|Alpha a questio')
					.addStringOption((b) => b.setName('question').setDescription('The question to ask').setRequired(true))
					.addBooleanOption((b) => b.setName('graphical').setDescription('If the output should be graphical').setRequired(false)),
			DefaultChatInputOptions
		);
	}

	public async chatInputRun(interaction: CommandInteraction) {
		const question = interaction.options.getString('question', true);
		const graphical = interaction.options.getBoolean('graphical', false) ?? false;

		this.container.logger.info({ question, graphical });

		if (bannedRegexes.some((r) => r.test(question))) {
			interaction.reply({ content: 'That is forbidden knowledge 👀', ephemeral: true });
			return;
		}

		if (graphical) {
			this.graphical(interaction, question);
		} else {
			this.textual(interaction, question);
		}
	}
	public async textual(interaction: CommandInteraction, question: string) {
		const url = new URL(`${BASE_URL}/result`);
		url.searchParams.append('appid', this.container.botConfig.secrets.wolfram);
		url.searchParams.append('i', question);

		let response: Response;
		try {
			response = await fetch(url.toString(), FetchResultTypes.Result);
			await interaction.reply(await response.text());
		} catch (e) {
			respondError({ interaction, error: new Error("Wolfram|Alpha didn't understand your input. Try using graphical mode") });
		}
	}

	public async graphical(interaction: CommandInteraction, question: string) {
		const url = new URL(`${BASE_URL}/simple`);
		url.searchParams.append('appid', this.container.botConfig.secrets.wolfram);
		url.searchParams.append('i', question);
		url.searchParams.append('units', 'metric');
		url.searchParams.append('background', '36393E');
		url.searchParams.append('foreground', 'white');

		await interaction.deferReply();
		const response = await fetch(url.toString(), FetchResultTypes.Result);

		if (!response.ok) {
			await respondError({ interaction, error: new Error(await response.text()) });
			return;
		}

		await interaction.editReply({
			files: [
				{
					attachment: Buffer.from(await response.arrayBuffer()),
					name: 'wolfram.gif'
				}
			]
		});
	}
}
