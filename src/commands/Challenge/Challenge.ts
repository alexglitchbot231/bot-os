import type { Awaitable, CommandInteraction } from 'discord.js';
import type { ApplicationCommandRegistry } from '@sapphire/framework';
import { DefaultChatInputOptions } from '#lib/constants';
import type { AgbSubCommandOptions } from '#lib/interfaces';
import { AgbSubCommand } from '#lib/structures';
import { ApplyOptions } from '@sapphire/decorators';
import { createDefaultEmbed, loadEconomyConfig, pluralise, respondError, saveEconomyConfig } from '#lib/utils';
import { userMention } from '@discordjs/builders';
import { chunk } from '@sapphire/utilities';
import { PaginatedMessage } from '@sapphire/discord.js-utilities';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'challenge',
	subcommands: [
		{
			name: 'points',
			type: 'group',
			entries: [
				{
					name: 'leaderboard',
					chatInputRun: 'leaderboard'
				},
				{
					name: 'balance',
					chatInputRun: 'balance'
				}
			]
		},
		{
			name: 'range',
			type: 'group',
			entries: [
				{
					name: 'show',
					chatInputRun: 'rangeShow'
				},
				{
					name: 'min',
					chatInputRun: 'rangeMin'
				},
				{
					name: 'max',
					chatInputRun: 'rangeMax'
				}
			]
		}
	]
})
export class ChallengeCommand extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName('challenge')
					.setDescription('Challenge commands')
					.addSubcommandGroup((g) =>
						g
							.setName('points')
							.setDescription('Points commands')
							.addSubcommand((c) => c.setName('leaderboard').setDescription('Show the points leaderboard'))
							.addSubcommand((c) =>
								c
									.setName('balance')
									.setDescription('Show your points balance')
									.addUserOption((o) => o.setName('user').setDescription('The user to show the balance of').setRequired(false))
							)
					)
					.addSubcommandGroup((g) =>
						g
							.setName('range')
							.setDescription('Range commands')
							.addSubcommand((c) => c.setName('leaderboard').setDescription('Show the points leaderboard'))
							.addSubcommand((c) => c.setName('show').setDescription('Show the current range'))
							.addSubcommand((c) =>
								c
									.setName('min')
									.setDescription('Set the minimum')
									.addIntegerOption((o) => o.setName('min').setDescription('The minimum').setRequired(true))
							)
							.addSubcommand((c) =>
								c
									.setName('max')
									.setDescription('Set the maximum')
									.addIntegerOption((o) => o.setName('max').setDescription('The maximum').setRequired(true))
							)
					),
			DefaultChatInputOptions
		);
	}

	public async leaderboard(interaction: CommandInteraction) {
		const lb = await this.container.prisma.users.findMany({
			select: {
				Id: true,
				ChallengePoints: true
			},
			orderBy: {
				ChallengePoints: 'desc'
			},
			where: {
				ChallengePoints: {
					gt: 0
				}
			}
		});

		const chunks = chunk(lb, 10);

		const pMsg = new PaginatedMessage();

		const mapping: Record<number, string> = {
			1: '🥇',
			2: '🥈',
			3: '🥉'
		};

		for (const chunk of chunks) {
			const embed = createDefaultEmbed(interaction.user)
				.setTitle('Points Leaderboard')
				.setDescription(
					chunk
						.map(
							(user, index) =>
								`${mapping[index + 1] ?? `#${index + 1}.`} ${userMention(user.Id)}: ${user.ChallengePoints} ${pluralise(
									user.ChallengePoints,
									'point'
								)}`
						)
						.join('\n')
				);

			pMsg.addPageEmbed(embed);
		}

		pMsg.run(interaction);
	}

	public async balance(interaction: CommandInteraction) {
		const user = interaction.options.getUser('user') ?? interaction.user;

		const { Id: id, ChallengePoints: challengePoints } = (await this.container.prisma.users.findUnique({
			where: {
				Id: user.id
			},
			select: {
				ChallengePoints: true,
				Id: true
			}
		}))!;

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Points Balance')
			.setDescription(
				`${user.id === interaction.user.id ? 'You have' : `${userMention(id)} has`} ${challengePoints} ${pluralise(challengePoints, 'point')}`
			);

		interaction.reply({ embeds: [embed] });
	}

	public async rangeShow(interaction: CommandInteraction) {
		const { min, max } = this.container.econConfig.ranges.challenges;

		const embed = createDefaultEmbed(interaction.user)
			.setTitle('Challenge Range')
			.addFields([
				{
					name: 'Minimum',
					value: min.toString(),
					inline: true
				},
				{
					name: 'Maximum',
					value: max.toString(),
					inline: true
				}
			]);

		interaction.reply({ embeds: [embed] });
	}

	public async rangeMin(interaction: CommandInteraction) {
		const min = interaction.options.getInteger('min', true);

		if (min < 0) {
			await respondError({
				interaction,
				error: new Error('The minimum cannot be less than 0')
			});
			return;
		}

		this.container.econConfig.ranges.challenges.min = min;
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: 'The minimum has been updated', ephemeral: true });
	}

	public async rangeMax(interaction: CommandInteraction) {
		const max = interaction.options.getInteger('max', true);
		const { min } = this.container.econConfig.ranges.challenges;

		if (max < 0) {
			await respondError({
				interaction,
				error: new Error('The maximum cannot be less than 0')
			});
			return;
		}

		if (max < min) {
			await respondError({
				interaction,
				error: new Error('The maximum cannot be less than the minimum')
			});
			return;
		}

		this.container.econConfig.ranges.challenges.max = max;
		await saveEconomyConfig();
		await loadEconomyConfig(true);

		await interaction.reply({ content: 'The maximum has been updated', ephemeral: true });
	}
}
