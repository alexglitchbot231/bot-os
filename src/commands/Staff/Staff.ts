import { AgbSubCommand } from '#lib/structures';
import type { AgbSubCommandOptions } from '#src/lib/interfaces';
import { ApplyOptions } from '@sapphire/decorators';
import { ApplicationCommandRegistry, container } from '@sapphire/framework';
import { Awaitable, ColorResolvable, CommandInteraction, Formatters, GuildMember, MessageActionRow, MessageButton, MessageEmbed } from 'discord.js';
import { UserFlagManager } from '#lib/UserFlagManager';
import { userToMember, respondError, sendPunishEmbed, safeDM, createDefaultEmbed } from '#lib/utils';
import { Duration } from '@sapphire/time-utilities';
import { stripIndents } from 'common-tags';
import type { PunishmentType } from '@prisma/client';
import { PaginatedMessage, PaginatedMessagePage } from '@sapphire/discord.js-utilities';

@ApplyOptions<AgbSubCommandOptions>({
	module: 'staff',
	name: 'staff',
	description: 'Staff commands',
	requireFlags: ['Staff'],
	subcommands: [
		{
			name: 'lockdown',
			chatInputRun: 'lockdown'
		},
		{
			name: 'history',
			chatInputRun: 'history'
		},
		{
			name: 'punish',
			type: 'group',
			entries: [
				{
					name: 'ban',
					chatInputRun: 'ban'
				},
				{
					name: 'kick',
					chatInputRun: 'kick'
				},
				{
					name: 'warn',
					chatInputRun: 'warn'
				},
				{
					name: 'timeout',
					chatInputRun: 'timeout'
				}
			]
		},
		{
			name: 'appeal',
			type: 'group',
			entries: [
				{
					name: 'approve',
					chatInputRun: 'banAppealApprove'
				},
				{
					name: 'deny',
					chatInputRun: 'banAppealDeny'
				}
			]
		}
	]
})
export class Staff extends AgbSubCommand {
	registerApplicationCommands(registry: ApplicationCommandRegistry): Awaitable<void> {
		registry.registerChatInputCommand((builder) =>
			builder //
				.setName(this.name)
				.setDescription(this.description)
				.addSubcommand((b) =>
					b
						.setName('lockdown')
						.setDescription('Lockdown a channel')
						.addStringOption((o) => o.setName('reason').setDescription('The reason for the lockdown').setRequired(true))
						.addChannelOption((o) => o.setName('channel').setDescription('The channel to lockdown').setRequired(false))
				)
				.addSubcommand((b) =>
					b
						.setName('history')
						.setDescription('View a users punishment history')
						.addUserOption((o) => o.setName('user').setDescription('The user').setRequired(true))
				)
				.addSubcommandGroup((b) =>
					b
						.setName('punish')
						.setDescription('Punish a user')
						.addSubcommand((b) =>
							b
								.setName('ban')
								.setDescription('Ban a user')
								.addUserOption((o) => o.setName('user').setDescription('The user').setRequired(true))
								.addStringOption((o) => o.setName('reason').setDescription('The reason').setRequired(true))
						)
						.addSubcommand((b) =>
							b
								.setName('warn')
								.setDescription('Warn a user')
								.addUserOption((o) => o.setName('user').setDescription('The user').setRequired(true))
								.addStringOption((o) => o.setName('reason').setDescription('The reason').setRequired(true))
						)
						.addSubcommand((b) =>
							b
								.setName('timeout')
								.setDescription('Timeout a user')
								.addUserOption((o) => o.setName('user').setDescription('The user').setRequired(true))
								.addStringOption((o) => o.setName('duration').setDescription('The duration').setRequired(true))
								.addStringOption((o) => o.setName('reason').setDescription('The reason').setRequired(true))
						)
						.addSubcommand((b) =>
							b
								.setName('kick')
								.setDescription('Kick a user')
								.addUserOption((o) => o.setName('user').setDescription('The user').setRequired(true))
								.addStringOption((o) => o.setName('reason').setDescription('The reason').setRequired(true))
						)
				)
				.addSubcommandGroup((b) =>
					b
						.setName('appeal')
						.setDescription('Ban appeal commands')
						.addSubcommand((b) =>
							b
								.setName('approve')
								.setDescription('Approve a ban appeal')
								.addIntegerOption((o) => o.setName('ban_id').setDescription('The ban ID').setRequired(true))
								.addStringOption((o) =>
									o.setName('comments').setDescription('Any comments to pass onto the appealing user').setRequired(false)
								)
						)
						.addSubcommand((b) =>
							b
								.setName('deny')
								.setDescription('Deny a ban appeal')
								.addIntegerOption((o) => o.setName('ban_id').setDescription('The ban ID').setRequired(true))
								.addStringOption((o) =>
									o.setName('comments').setDescription('Any comments to pass onto the appealing user').setRequired(false)
								)
						)
				)
		);
	}

	async ban(i: CommandInteraction) {
		container.logger.debug('gay');
		const user = i.options.getUser('user', true);
		const reason = i.options.getString('reason', true);

		const member = await userToMember(user, i.guild!);

		if (!member) {
			await respondError({
				interaction: i,
				error: new Error('Due to ~~technical limitations~~ me being lazy, you can only ban members of this server.')
			});

			return;
		}

		const unBannableRoles = [
			'747573502105616384', //  *
			'90285905176428604', // leader
			'98509631877283850', // acting leader
			'24496283891294238', // dev
			'93134233572737054', // admin
			'38482067427295282', // sr mod
			'90291479599513962', // mod
			'34509461422735390', // stream mod
			'90301424843620392' // staff
		];

		if (
			member.roles.cache.some((r) => unBannableRoles.includes(r.id)) ||
			member.permissions.has(['KICK_MEMBERS', 'BAN_MEMBERS', 'ADMINISTRATOR']) ||
			!member.bannable ||
			(await new UserFlagManager(user).getFlags()).some((f) => ['Staff', 'Developer'].includes(f))
		) {
			await respondError({
				interaction: i,
				error: new Error('You cannot ban this user.')
			});

			return;
		}

		const { Id: id } = await container.prisma.punishment.create({
			data: {
				Type: 'Ban',
				Reason: reason,
				User: {
					connect: {
						Id: user.id
					}
				},
				ModeratorId: i.user.id,
				Evidence: ''
			},
			select: {
				Id: true
			}
		});

		await sendPunishEmbed({
			guild: i.guild!,
			user: user,
			reason,
			type: 'ban',
			id,
			moderator: i.member! as GuildMember
		});

		await safeDM(user, {
			embeds: [
				{
					title: 'Punishment Received',
					description: `You have been banned from ${i.guild!.name} for ${reason}`,
					color: 'RED'
				}
			],
			components: [
				new MessageActionRow().addComponents(new MessageButton().setLabel('Appeal').setStyle('LINK').setURL('https://discord.gg/PVPc7zfaBF'))
			]
		});

		await member.ban({ reason });

		await i.reply(`Banned ${user.tag}`);
	}

	async warn(i: CommandInteraction) {
		const user = i.options.getUser('user', true);
		const reason = i.options.getString('reason', true);

		const { Id: id } = await container.prisma.punishment.create({
			data: {
				Type: 'Warn',
				Reason: reason,
				User: {
					connect: {
						Id: user.id
					}
				},
				ModeratorId: i.user.id,
				Evidence: ''
			},
			select: {
				Id: true
			}
		});

		await sendPunishEmbed({
			guild: i.guild!,
			user: user,
			reason,
			type: 'warn',
			id,
			moderator: i.member! as GuildMember
		});

		await safeDM(user, {
			embeds: [
				{
					title: 'Punishment Received',
					description: `You have been warned in ${i.guild!.name} for ${reason}`,
					color: 'YELLOW'
				}
			],
			components: [
				new MessageActionRow()
					.addComponents
					// new MessageButton().setLabel('Appeal').setCustomId(generateButtonCommandId('appeal', id.toString())).setStyle('PRIMARY')
					()
			]
		});

		await i.reply(`Warned ${user.tag}`);
	}

	async kick(i: CommandInteraction) {
		const user = i.options.getUser('user', true);
		const reason = i.options.getString('reason', true);

		const member = await userToMember(user, i.guild!);

		if (!member) {
			await respondError({
				interaction: i,
				error: new Error('Due to ~~technical limitations~~ me being lazy, you can only kick members of this server.')
			});

			return;
		}

		const unKickableRoles = [
			'747573502105616384', //  *
			'90285905176428604', // leader
			'98509631877283850', // acting leader
			'24496283891294238', // dev
			'93134233572737054', // admin
			'38482067427295282', // sr mod
			'90291479599513962', // mod
			'34509461422735390', // stream mod
			'90301424843620392' // staff
		];

		if (
			member.roles.cache.some((r) => unKickableRoles.includes(r.id)) ||
			member.permissions.has(['KICK_MEMBERS', 'BAN_MEMBERS', 'ADMINISTRATOR']) ||
			!member.kickable ||
			(await new UserFlagManager(user).getFlags()).some((f) => ['Staff', 'Developer'].includes(f))
		) {
			await respondError({
				interaction: i,
				error: new Error('You cannot kick this user.')
			});

			return;
		}

		const { Id: id } = await container.prisma.punishment.create({
			data: {
				Type: 'Kick',
				Reason: reason,
				User: {
					connect: {
						Id: user.id
					}
				},
				ModeratorId: i.user.id,
				Evidence: ''
			},
			select: {
				Id: true
			}
		});

		await sendPunishEmbed({
			guild: i.guild!,
			user: user,
			reason,
			type: 'kick',
			id,
			moderator: i.member! as GuildMember
		});

		await safeDM(user, {
			embeds: [
				{
					title: 'Punishment Received',
					description: `You have been kicked from ${i.guild!.name} for ${reason}`,
					color: 'ORANGE'
				}
			],
			components: [
				new MessageActionRow()
					.addComponents
					// new MessageButton().setLabel('Appeal').setCustomId(generateButtonCommandId('appeal', id.toString())).setStyle('PRIMARY')
					()
			]
		});
	}

	async timeout(i: CommandInteraction) {
		const user = i.options.getUser('user', true);
		const reason = i.options.getString('reason', true);
		const durationRaw = i.options.getString('duration', true);

		const dur = new Duration(durationRaw);

		if (isNaN(dur.offset)) {
			await respondError({ interaction: i, error: new Error('Invalid duration') });
			return;
		}

		const member = await userToMember(user, i.guild!);

		if (!member) {
			await respondError({
				interaction: i,
				error: new Error('You can only mute members of this server.')
			});

			return;
		}

		const unMuteableRoles = [
			'747573502105616384', //  *
			'90285905176428604', // leader
			'98509631877283850', // acting leader
			'24496283891294238', // dev
			'93134233572737054', // admin
			'38482067427295282', // sr mod
			'90291479599513962', // mod
			'34509461422735390', // stream mod
			'90301424843620392' // staff
		];

		if (
			member.roles.cache.some((r) => unMuteableRoles.includes(r.id)) ||
			member.permissions.has(['KICK_MEMBERS', 'BAN_MEMBERS', 'ADMINISTRATOR']) ||
			!member.kickable ||
			(await new UserFlagManager(user).getFlags()).some((f) => ['Staff', 'Developer'].includes(f))
		) {
			await respondError({
				interaction: i,
				error: new Error('You cannot mute this user.')
			});

			return;
		}

		const { Id: id } = await container.prisma.punishment.create({
			data: {
				Type: 'Mute',
				Reason: reason,
				User: {
					connect: {
						Id: user.id
					}
				},
				ModeratorId: i.user.id,
				Evidence: '',
				Expire: dur.fromNow
			},
			select: {
				Id: true
			}
		});

		await member.disableCommunicationUntil(dur.fromNow);
		await sendPunishEmbed({
			guild: i.guild!,
			user: user,
			reason,
			type: 'timeout',
			id,
			moderator: i.member! as GuildMember
		});

		await safeDM(user, {
			embeds: [
				{
					title: 'Punishment Received',
					description: `You have been muted from ${i.guild!.name} for ${reason}`,
					color: 'ORANGE'
				}
			]
		});

		await i.reply(`Muted ${user.tag} for until ${Formatters.time(dur.fromNow, 'F')}`);
	}

	async banAppealApprove(i: CommandInteraction) {
		const id = i.options.getInteger('ban_id', true);
		const comments = i.options.getString('comments', false) ?? 'None';

		const punishment = await container.prisma.punishment.findUnique({
			where: {
				Id: id
			}
		});

		if (!punishment) {
			await respondError({
				interaction: i,
				error: new Error('Invalid punishment ID.')
			});

			return;
		}

		if (!punishment.Active) {
			await respondError({
				interaction: i,
				error: new Error('This punishment is not active.')
			});

			return;
		}

		const user = await container.client.users.fetch(punishment.UserId);

		if (!user) {
			await respondError({
				interaction: i,
				error: new Error('Invalid user.')
			});

			return;
		}

		await container.prisma.punishment.update({
			where: {
				Id: id
			},
			data: {
				Active: false
			}
		});

		await i.reply({
			content: `Approved appeal for ${user.tag}`,
			ephemeral: true
		});

		await safeDM(user, {
			embeds: [
				{
					title: 'Appeal Approved',
					description: `Your appeal for ${punishment.Reason} has been approved by ${i.user.tag} with the following comments: ${comments}`,
					color: 'GREEN',
					footer: {
						text: `You are receiving this message because you have appealed a ban in ${i.guild!.name}`
					}
				}
			],
			components: [
				new MessageActionRow().addComponents(
					new MessageButton().setLabel('Rejoin Server').setURL('https://discord.gg/F3dMf7DqFY').setStyle('LINK')
				)
			]
		});
	}

	async banAppealDeny(i: CommandInteraction) {
		const id = i.options.getInteger('ban_id', true);
		const comments = i.options.getString('comments', false) ?? 'None';

		const punishment = await container.prisma.punishment.findUnique({
			where: {
				Id: id
			}
		});

		if (!punishment) {
			await respondError({
				interaction: i,
				error: new Error('Invalid punishment ID.')
			});

			return;
		}

		const user = await container.client.users.fetch(punishment.UserId);

		await i.reply({
			content: `Denied appeal for ${user.tag}`,
			ephemeral: true
		});

		if (punishment.Type === 'Ban') {
			await i.guild!.members.unban(user, `Appeal denied by ${i.user.tag} with comments: ${comments}`);
		}

		await safeDM(user, {
			embeds: [
				{
					title: 'Appeal Approved',
					description: `Your appeal for ${punishment.Reason} has been denied with the following comments: ${comments}`,
					color: 'RED',
					footer: {
						text: `You are receiving this message because you have appealed a ban in ${i.guild!.name}`
					}
				}
			]
		});
	}

	async history(i: CommandInteraction) {
		const user = i.options.getUser('user', true);

		const punishments = await container.prisma.punishment.findMany({
			where: {
				UserId: user.id
			},
			orderBy: {
				Expire: 'desc'
			}
		});

		if (punishments.length === 0) {
			const embed = createDefaultEmbed(user)
				.setTitle(`Punishment History for ${user.tag}`)
				.setDescription('This user has no punishments.')
				.setColor('GREEN');

			await i.reply({
				embeds: [embed]
			});
			return;
		}

		const colorMapping: Record<PunishmentType, ColorResolvable> = {
			Kick: 'ORANGE',
			Warn: 'YELLOW',
			Mute: 'ORANGE',
			Ban: 'RED'
		};

		const pMsg = new PaginatedMessage();
		for (const punishment of punishments) {
			const moderator = await container.client.users.fetch(punishment.ModeratorId);

			const embed = createDefaultEmbed(user)
				.setTitle(`Punishment #${punishment.Id} - ${punishment.Type}`)
				.setDescription(
					stripIndents`**Type:** ${punishment.Type}
				**Reason:** ${punishment.Reason}
				**Moderator:** ${moderator.tag}
				**Active:** ${punishment.Active ? 'Yes' : 'No'}`
				)
				.setColor(colorMapping[punishment.Type]);
			pMsg.addPageEmbed(embed);
		}

		await pMsg.run(i);
	}
}
